package laboral._transition;

import laboral.domain.address.Address;
import laboral.domain.address.Province;
import laboral.domain.address.StreetType;
import laboral.domain.address.persistence.dao.AddressDAO;
import laboral.domain.client.persistence.dao.ClientDAO;
import laboral.domain.contract.work_contract.persistence.dao.WorkContractDAO;
import laboral.domain.contract_type.persistence.dao.WorkContractTypeDAO;
import laboral.domain.employee.persistence.dao.EmployeeDAO;
import laboral.domain.employer.persistence.dao.EmployerDAO;
import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person.controller.PersonController;
import laboral.domain.person.persistence.dao.PersonDAO;
import laboral.domain.person_type.PersonType;
import laboral.domain.traceability_contract_documentation.persistence.dao.TraceabilityContractDocumentationDAO;
import laboral.domain.variation_type.persistence.dao.VariationTypeDAO;

import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;

public class GenerateTestData {

    public static void generateData(){

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

        PersonController personController = new PersonController();

        PersonDAO personDAO = PersonDAO.PersonDAOFactory.getInstance();
        AddressDAO addressDAO = AddressDAO.AddressDAOFactory.getInstance();
        ClientDAO clientDAO = ClientDAO.ClientDAOFactory.getInstance();
        EmployerDAO employerDAO = EmployerDAO.EmployerDAOFactory.getInstance();
        EmployeeDAO employeeDAO = EmployeeDAO.EmployeeDAOFactory.getInstance();
        WorkContractDAO workContractDAO = WorkContractDAO.ContractDAOFactory.getInstance();
        WorkContractTypeDAO workContractTypeDAO = WorkContractTypeDAO.WorkContractTypeDAOFactory.getInstance();
        TraceabilityContractDocumentationDAO traceabilityContractDocumentationDAO = TraceabilityContractDocumentationDAO.TraceabilityContractDocumentationDAOFactory.getInstance();
        VariationTypeDAO variationTypeDAO = VariationTypeDAO.VariationTypeDAOFactory.getInstance();

        Address address = new Address();
        address.setStreetType(StreetType.PGIND);
        address.setStreetName("Castiñeiras");
        address.setStreetExtended("Parcela 9");
        address.setMunicipality("Bueu");
        address.setPostalCode("36938");
        address.setProvince(Province.PONTEVEDRA);

        Set<Address> addressSet = new HashSet<>();
        addressSet.add(address);

        PersonCreationRequest personCreationRequest = PersonCreationRequest.PersonCreationRequestBuilder.create()
                .withLegalName("Comercial GUIBU Alimentación S. L.")
                .withPersonType(PersonType.LEGAL_PERSON)
                .withNieNif(new NieNif("B94092525"))
                .withAddresses(addressSet)
                .build();

//        Integer personId = personController.personCreator(personRequest);

//        ClientDBO clientDBO = new ClientDBO();
//        clientDBO.setPersonDBO(personDAO.findById(personId));

//        WorkCenterDBO workCenterDBO = new WorkCenterDBO();
//        workCenterDBO.setAddressDBO(addressDAO.findById(1));
//        Set<WorkCenterDBO> workCentersVO = new HashSet<>();
//        workCentersVO.add(workCenterDBO);
//
//        clientDBO.setWorkCenters(workCentersVO);
//        Integer clientId = clientDAO.create(clientDBO);

        EmployerDBO employerDBO = new EmployerDBO();
//        employerDBO.setClientDBO(clientDAO.findById(clientId));

        Integer employerId = employerDAO.create(employerDBO);

//        PersonRequest personRequestOther = PersonRequest.create()
//                .withId(15)
//                .withPersonType(PersonType.NATURAL_PERSON)
//                .withName("David")
//                .withFirstSurname("Guimerans")
//                .withSecondSurname("Budiño")
//                .withNieNif(new NieNif("78735057P"))
//                .withSocialSecurityAffiliationNumber("361010058275")
//                .withStudy(StudyLevelType.ESO_STUDIES)
//                .withNationality("Española")
//                .withCivilStatus(CivilStatusType.MARRIED)
//                .build();
//
//        personId = personController.personCreator(personRequestOther);

//        EmployerDBO employerDBORead = employerDAO.findById(1);
//
//        EmployeeDBO employeeDBO = new EmployeeDBO();
//        employeeDBO.setPersonDBO(personDAO.findById(15));
//        employeeDAO.create(employeeDBO);
//
//        ContractDBO contractDBO = new ContractDBO();
//        contractDBO.setEmployerDBO(employerDAO.findById(1));
//        contractDBO.setEmployeeDBO(employeeDAO.findById(1));
//        contractDBO.setStartDate(Date.valueOf(LocalDate.of(2019,5,1)));
//        contractDBO.setExpectedEndDate(Date.valueOf(LocalDate.of(2019,12,31)));
//        contractDBO.setContractType(contractTypeDAO.findById(60).getId());
//        contractDBO.setVariationType(2);
//
//        LocalDate clientNotificationDate = LocalDate.now();
//        LocalTime clientNotificationTime = LocalTime.now();
//        LocalDateTime ldt = LocalDateTime.of(clientNotificationDate,clientNotificationTime);
//        contractDBO.setClientNotification(Timestamp.valueOf(ldt));
//
//        TraceabilityContractDocumentationDBO traceabilityDBO = new TraceabilityContractDocumentationDBO();
//        traceabilityDBO.setContract(contractDBO);
//        traceabilityDBO.setStartDate(contractDBO.getStartDate());
//        traceabilityDBO.setExpectedEndDate(contractDBO.getExpectedEndDate());
//        traceabilityDBO.setVariationType(contractDBO.getVariationType());
//        traceabilityContractDocumentationDAO.create(traceabilityDBO);
//        contractDBO.setTraceability(traceabilityDBO);
//
//        Integer contractId = contractDAO.create(contractDBO);
//
//
//
//        ContractDBO contractDBORead = contractDAO.findById(contractId);
//
//        System.out.println("Client: " + contractDBORead.getEmployerDBO().getClientDBO().getPersonDBO().toAlphabeticalName() + " es una " + contractDBORead.getEmployerDBO().getClientDBO().getPersonDBO().getPersonType().getPersonTypeDescription().toLowerCase());
//        for(AddressDBO addressDBORead : contractDBORead.getEmployerDBO().getClientDBO().getPersonDBO().getAddresses()) {
//            System.out.println("Fiscal address: " + addressDBORead.toString());
//        }
//        for(WorkCenterDBO workCenterDBORead : contractDBORead.getEmployerDBO().getClientDBO().getWorkCenters()){
//            System.out.println("Work center: " + workCenterDBORead.getAddressDBO().toString());
//        }
//
//        System.out.println("Employee: " + contractDBORead.getEmployeeDBO().getPersonDBO().toNaturalName()
//                + " ::: Contract variation: " + variationTypeDAO.findById(contractDBO.getVariationType()).getVariationDescription() +
//                " ::: Contract type: " + contractTypeDAO.findById(contractDBORead.getContractType()).getColloquial()
//                + ", " + contractTypeDAO.findById(contractDBORead.getContractType()).getContractDescription() + ", desde " + contractDBORead.getStartDate().toLocalDate().format(dateFormatter));
//
//        System.out.println("Traceability: " + contractDBORead.getTraceability().getStartDate());
    }
}
