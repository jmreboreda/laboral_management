package laboral.domain.traceability_contract_documentation.persistence.dao;


import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.traceability_contract_documentation.persistence.dbo.TraceabilityContractDocumentationDBO;
import laboral.domain.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import javax.persistence.TypedQuery;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

public class TraceabilityContractDocumentationDAO implements GenericDAO<TraceabilityContractDocumentationDBO, Integer> {

    private SessionFactory sessionFactory;
    private Session session;

    public TraceabilityContractDocumentationDAO() {
    }

     public static class TraceabilityContractDocumentationDAOFactory {

        private static TraceabilityContractDocumentationDAO traceabilityContractDocumentationDAO;

        public static TraceabilityContractDocumentationDAO getInstance() {
            if(traceabilityContractDocumentationDAO == null) {
                traceabilityContractDocumentationDAO = new TraceabilityContractDocumentationDAO(HibernateUtil.retrieveGlobalSession());
            }
            return traceabilityContractDocumentationDAO;
        }
    }

    public TraceabilityContractDocumentationDAO(Session session) {
        this.session = session;
    }

    public Integer create(TraceabilityContractDocumentationDBO traceabilityContractDocumentationDBO) {

        try {
            session.beginTransaction();
            session.saveOrUpdate(traceabilityContractDocumentationDBO);
            session.getTransaction().commit();
        }
        catch (Exception e){
            System.err.println("No se han podido guardar los datos de trazabilidad para el contrato: " + e.getMessage());
        }

        return traceabilityContractDocumentationDBO.getId();
    }

    @Override
    public TraceabilityContractDocumentationDBO findById(Integer traceabilityContractDocumentationId) {
        TypedQuery<TraceabilityContractDocumentationDBO> query = session.createNamedQuery(TraceabilityContractDocumentationDBO.FIND_TRACEABILITY_BY_ID, TraceabilityContractDocumentationDBO.class);
        query.setParameter("traceabilityContractDocumentationId", traceabilityContractDocumentationId);

        return query.getSingleResult();
    }

    @Override
    public Integer update(TraceabilityContractDocumentationDBO entity) {

        return null;
    }

    @Override
    public Boolean delete(TraceabilityContractDocumentationDBO traceabilityContractDocumentationDBO) {

        return null;
    }

    @Override
    public List<TraceabilityContractDocumentationDBO> findAll() {
        return null;
    }

    public Integer updateTraceabilityRecord(TraceabilityContractDocumentationDBO traceabilityContractDocumentationDBO){
        TraceabilityContractDocumentationDBO traceabilityContractDocumentationReadVO = null;
        try {
            session.beginTransaction();
            traceabilityContractDocumentationReadVO = session.get(TraceabilityContractDocumentationDBO.class, traceabilityContractDocumentationDBO.getId());
            traceabilityContractDocumentationReadVO.setId(traceabilityContractDocumentationDBO.getId());
            traceabilityContractDocumentationReadVO.setVariationType(traceabilityContractDocumentationDBO.getVariationType());
            traceabilityContractDocumentationReadVO.setStartDate(traceabilityContractDocumentationDBO.getStartDate());
            traceabilityContractDocumentationReadVO.setExpectedEndDate(traceabilityContractDocumentationDBO.getExpectedEndDate());
            traceabilityContractDocumentationReadVO.setIDCReceptionDate(traceabilityContractDocumentationDBO.getIDCReceptionDate());
            traceabilityContractDocumentationReadVO.setDateDeliveryContractDocumentationToClient(traceabilityContractDocumentationDBO.getDateDeliveryContractDocumentationToClient());
            traceabilityContractDocumentationReadVO.setContractEndNoticeReceptionDate(traceabilityContractDocumentationDBO.getContractEndNoticeReceptionDate());
            session.update(traceabilityContractDocumentationReadVO);
            session.getTransaction().commit();
        }
        catch (Exception e){
            System.err.println("No se ha podido actualizar los datos de la trazabilidad del contrato en \"traceability_contract_documentation\": " + e.getMessage());
        }

        return traceabilityContractDocumentationDBO.getId();
    }

    public List<TraceabilityContractDocumentationDBO> findAllTraceabilityContractData(){
        TypedQuery<TraceabilityContractDocumentationDBO> query = session.createNamedQuery(TraceabilityContractDocumentationDBO.FIND_ALL_RECORD, TraceabilityContractDocumentationDBO.class);

        return query.getResultList();
    }

    public List<TraceabilityContractDocumentationDBO> findAllTraceabilityRecordByContractNumber(Integer contractNumber){
        TypedQuery<TraceabilityContractDocumentationDBO> query = session.createNamedQuery(TraceabilityContractDocumentationDBO.FIND_ALL_RECORD_BY_CONTRACT_NUMBER, TraceabilityContractDocumentationDBO.class);
        query.setParameter("contractNumber", contractNumber);

        return query.getResultList();
    }

    public List<TraceabilityContractDocumentationDBO> findTraceabilityForAllContractWithPendingIDC(){
        TypedQuery<TraceabilityContractDocumentationDBO> query = session.createNamedQuery(TraceabilityContractDocumentationDBO.FIND_ALL_CONTRACT_WITH_PENDING_IDC, TraceabilityContractDocumentationDBO.class);

        return query.getResultList();
    }

    public List<TraceabilityContractDocumentationDBO> findTraceabilityForAllContractWithPendingContractEndNotice(){
        TypedQuery<TraceabilityContractDocumentationDBO> query = session.createNamedQuery(TraceabilityContractDocumentationDBO.FIND_ALL_CONTRACT_WITH_PENDING_CONTRACT_END_NOTICE, TraceabilityContractDocumentationDBO.class);

        return query.getResultList();
    }

    public List<TraceabilityContractDocumentationDBO> findTraceabilityForAllContractWithWorkingDayScheduleWithEndDate(){
        TypedQuery<TraceabilityContractDocumentationDBO> query = session.createNamedQuery(TraceabilityContractDocumentationDBO.FIND_ALL_CONTRACT_WITH_WORKING_DAY_SCHEDULE_WITH_END_DATE, TraceabilityContractDocumentationDBO.class);

        return query.getResultList();
    }

    public List<TraceabilityContractDocumentationDBO> findTraceabilityForAllContractWithPendingLaborDocumentation(){
        TypedQuery<TraceabilityContractDocumentationDBO> query = session.createNamedQuery(TraceabilityContractDocumentationDBO.FIND_ALL_CONTRACT_WITH_PENDING_CONTRACT_DOCUMENTATION_TO_CLIENT, TraceabilityContractDocumentationDBO.class);

        return query.getResultList();
    }
}
