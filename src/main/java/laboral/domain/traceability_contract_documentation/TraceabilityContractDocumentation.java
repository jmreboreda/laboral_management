package laboral.domain.traceability_contract_documentation;

import laboral.domain.variation_type.VariationType;

import java.time.LocalDate;

public class TraceabilityContractDocumentation {

    private Integer id;
    private Integer contractNumber;
    private VariationType variationType;
    private LocalDate startDate;
    private LocalDate expectedEndDate;
    private LocalDate idcReceptionDate;
    private LocalDate dateDeliveryContractDocumentationToClient;
    private LocalDate contractEndNoticeReceptionDate;

    public TraceabilityContractDocumentation() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(Integer contractNumber) {
        this.contractNumber = contractNumber;
    }

    public VariationType getVariationType() {
        return variationType;
    }

    public void setVariationType(VariationType variationType) {
        this.variationType = variationType;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(LocalDate expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public LocalDate getIdcReceptionDate() {
        return idcReceptionDate;
    }

    public void setIdcReceptionDate(LocalDate idcReceptionDate) {
        this.idcReceptionDate = idcReceptionDate;
    }

    public LocalDate getDateDeliveryContractDocumentationToClient() {
        return dateDeliveryContractDocumentationToClient;
    }

    public void setDateDeliveryContractDocumentationToClient(LocalDate dateDeliveryContractDocumentationToClient) {
        this.dateDeliveryContractDocumentationToClient = dateDeliveryContractDocumentationToClient;
    }

    public LocalDate getContractEndNoticeReceptionDate() {
        return contractEndNoticeReceptionDate;
    }

    public void setContractEndNoticeReceptionDate(LocalDate contractEndNoticeReceptionDate) {
        this.contractEndNoticeReceptionDate = contractEndNoticeReceptionDate;
    }
}
