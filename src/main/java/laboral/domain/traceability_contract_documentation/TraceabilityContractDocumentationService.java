package laboral.domain.traceability_contract_documentation;

import laboral._transition.contract_variation_gmoldes.TraceabilityContractDocumentationDBOGmoldes;
import laboral.domain.traceability_contract_documentation.persistence.dao.TraceabilityContractDocumentationDAO;
import laboral.domain.traceability_contract_documentation.persistence.dbo.TraceabilityContractDocumentationDBO;

import java.sql.Date;
import java.time.LocalDate;

public class TraceabilityContractDocumentationService {

    private TraceabilityContractDocumentationService() {
    }

    public static class TraceabilityContractDocumentationServiceFactory {

        private static TraceabilityContractDocumentationService traceabilityContractDocumentationService;

        public static TraceabilityContractDocumentationService getInstance() {
            if(traceabilityContractDocumentationService == null) {
                traceabilityContractDocumentationService = new TraceabilityContractDocumentationService();
            }
            return traceabilityContractDocumentationService;
        }
    }

    private TraceabilityContractDocumentationDAO traceabilityContractDocumentationDAO = TraceabilityContractDocumentationDAO.TraceabilityContractDocumentationDAOFactory.getInstance();

    public TraceabilityContractDocumentationDBO findById(Integer traceabilityContractDocumentationId){

        return traceabilityContractDocumentationDAO.findById(traceabilityContractDocumentationId);
    }
}
