package laboral.domain.quote_account_code.mapper;

import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.quote_account_code.persistence.dbo.QuoteAccountCodeDBO;

public class MapperQuoteAccountCodeDBOToQuoteAccountCode implements GenericMapper<QuoteAccountCodeDBO, QuoteAccountCode> {

    @Override
    public QuoteAccountCode map(QuoteAccountCodeDBO quoteAccountCodeDBO) {

        if(quoteAccountCodeDBO == null){
            return null;
        }

        final QuoteAccountCode quoteAccountCode = new QuoteAccountCode();

        quoteAccountCode.setId(quoteAccountCodeDBO.getId());
        quoteAccountCode.setRegime(quoteAccountCodeDBO.getQuoteAccountCodeRegimeEnum());
        quoteAccountCode.setProvince(quoteAccountCodeDBO.getProvince());
        quoteAccountCode.setQuoteAccountCodeNumber(quoteAccountCodeDBO.getQuoteAccountNumber());
        quoteAccountCode.setControlDigit(quoteAccountCodeDBO.getControlDigit());
        quoteAccountCode.setActivityEpigraphNumber(quoteAccountCodeDBO.getActivityEpigraphNumber());
        quoteAccountCode.setActivityDescription(quoteAccountCodeDBO.getActivityDescription());

        return quoteAccountCode;
    }
}
