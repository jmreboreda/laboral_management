package laboral.domain.quote_account_code;

import laboral.domain.quote_account_code.persistence.dao.QuoteAccountCodeDAO;
import laboral.domain.quote_account_code.persistence.dbo.QuoteAccountCodeDBO;

public class QuoteAccountCodeService {

    QuoteAccountCodeDAO quoteAccountCodeDAO = QuoteAccountCodeDAO.QuoteAccountCodeDAOFactory.getInstance();

    public QuoteAccountCodeDBO findById(Integer quoteAccountCodeId){

        return quoteAccountCodeDAO.findById(quoteAccountCodeId);
    }

    public QuoteAccountCodeDBO findByQuoteAccountCode(String quoteAccountCode){

        return quoteAccountCodeDAO.findByQuoteAccountCode(quoteAccountCode);


    }
}
