package laboral.domain.quote_account_code.persistence.dao;


import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.quote_account_code.persistence.dbo.QuoteAccountCodeDBO;
import laboral.domain.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;

public class QuoteAccountCodeDAO implements GenericDAO<QuoteAccountCodeDBO, Integer> {

    private SessionFactory sessionFactory;
    private Session session;

    public QuoteAccountCodeDAO() {
    }

    public static class QuoteAccountCodeDAOFactory {

        private static QuoteAccountCodeDAO quoteAccountCodeDAO;

        public static QuoteAccountCodeDAO getInstance() {
            if(quoteAccountCodeDAO == null) {
                quoteAccountCodeDAO = new QuoteAccountCodeDAO(HibernateUtil.retrieveGlobalSession());
            }
            return quoteAccountCodeDAO;
        }
    }

    public QuoteAccountCodeDAO(Session session) {
        this.session = session;
    }

    @Override
    public Integer create(QuoteAccountCodeDBO quoteAccountCodeDBO) {
        try {
            session.beginTransaction();
            session.saveOrUpdate(quoteAccountCodeDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }
        return quoteAccountCodeDBO.getId();    }

    @Override
    public QuoteAccountCodeDBO findById(Integer quoteAccountCodeId) {

        try {
            TypedQuery<QuoteAccountCodeDBO> query = session.createNamedQuery(QuoteAccountCodeDBO.FIND_QUOTE_ACCOUNT_CODE_BY_ID, QuoteAccountCodeDBO.class);
            query.setParameter("quoteAccountCodeId", quoteAccountCodeId);

            return query.getSingleResult();

        }
        catch(NoResultException e){

            return null;
        }

    }

    @Override
    public Integer update(QuoteAccountCodeDBO quoteAccountCodeDBO) {

        return null;
    }

    @Override
    public Boolean delete(QuoteAccountCodeDBO quoteAccountCodeDBO) {

        return null;
    }

    @Override
    public List<QuoteAccountCodeDBO> findAll() {
        return null;
    }

    public QuoteAccountCodeDBO findByQuoteAccountCode(String quoteAccountCode){
        try {
            TypedQuery<QuoteAccountCodeDBO> query = session.createNamedQuery(QuoteAccountCodeDBO.FIND_QUOTE_ACCOUNT_CODE_BY_QUOTE_ACCOUNT_CODE_NUMBER, QuoteAccountCodeDBO.class);
            query.setParameter("quoteAccountCode", quoteAccountCode);

            return query.getSingleResult();

        }
        catch(NoResultException e){

            return null;
        }

    }

}
