package laboral.domain.work_center;

import laboral.domain.address.Address;
import laboral.domain.address.StreetType;

public class WorkCenter{

    private Integer id;
    private Address address;

    public WorkCenter(Integer id, Address address) {
        this.id = id;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String toString(){
        String space = " ";

        if(getAddress().getStreetType() == null || getAddress().getStreetType() == StreetType.UNKN){

            return "";
        }

        return getAddress().getStreetType().getStreetTypeDescription().concat(" ").concat(getAddress().getStreetName()).concat(", ").concat(getAddress().getStreetExtended()
                .concat(space.repeat(4)).concat(getAddress().getPostalCode()).concat(space.repeat(4)).concat(getAddress().getLocation())
                .concat(space.repeat(4)).concat(getAddress().getProvince().getName()));
    }
}
