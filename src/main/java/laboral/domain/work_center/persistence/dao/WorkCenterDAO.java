package laboral.domain.work_center.persistence.dao;

import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.utilities.HibernateUtil;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.TypedQuery;
import java.util.List;

public class WorkCenterDAO implements GenericDAO<WorkCenterDBO, Integer> {

    private SessionFactory sessionFactory;
    private Session session;

    public WorkCenterDAO() {
    }

    public static class WorkCenterDAOFactory {

        private static WorkCenterDAO workCenterDAO;

        public static WorkCenterDAO getInstance() {
            if(workCenterDAO == null) {
                workCenterDAO = new WorkCenterDAO(HibernateUtil.retrieveGlobalSession());
            }
            return workCenterDAO;
        }
    }

    public WorkCenterDAO(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    public Integer create(WorkCenterDBO workCenterDBO) {

        try{
        session.beginTransaction();
        session.saveOrUpdate(workCenterDBO);
        session.getTransaction().commit();
    }
        catch (org.hibernate.exception.ConstraintViolationException cve){

    }
            return workCenterDBO.getId();
}

    @Override
    public WorkCenterDBO findById(Integer workCenterId) {
        TypedQuery<WorkCenterDBO> query = session.createNamedQuery(WorkCenterDBO.FIND_WORK_CENTER_BY_ID, WorkCenterDBO.class);
        query.setParameter("workCenterId", workCenterId);

        return query.getSingleResult();
    }

    @Override
    public Integer update(WorkCenterDBO workCenterDBO) {
        try {
            session.beginTransaction();
            session.merge(workCenterDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }

        return workCenterDBO.getId();
    }

    @Override
    public Boolean delete(WorkCenterDBO workCenterDBO) {
        try {
            session.beginTransaction();
            session.delete(workCenterDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }

        return Boolean.TRUE;
    }

    public List<WorkCenterDBO> findByClientId(Integer clientId){
        TypedQuery<WorkCenterDBO> query = session.createNamedQuery(WorkCenterDBO.FIND_WORK_CENTER_BY_CLIENT_ID, WorkCenterDBO.class);
        query.setParameter("clientId", clientId);

        return query.getResultList();
    }

    @Override
    public List<WorkCenterDBO> findAll() {
        return null;
    }

    public WorkCenterDBO findWorkCenterByAddressId(Integer addressId, Integer clientId){
        TypedQuery<WorkCenterDBO> query = session.createNamedQuery(WorkCenterDBO.FIND_WORK_CENTER_BY_ADDRESS_ID, WorkCenterDBO.class);
        query.setParameter("addressId", addressId);
        query.setParameter("clientId", clientId);


        return query.getSingleResult();    }
}
