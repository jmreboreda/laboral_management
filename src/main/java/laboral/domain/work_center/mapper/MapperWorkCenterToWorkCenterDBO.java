package laboral.domain.work_center.mapper;

import laboral.domain.address.AddressService;
import laboral.domain.address.controller.AddressController;
import laboral.domain.address.mapper.MapperAddressToAddressDBO;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.work_center.WorkCenter;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;

public class MapperWorkCenterToWorkCenterDBO implements GenericMapper<WorkCenter, WorkCenterDBO> {

    AddressService addressService = AddressService.AddressServiceFactory.getInstance();

    @Override
    public WorkCenterDBO map(WorkCenter workCenter) {

        AddressDBO addressDBO = new AddressDBO();

        if(workCenter.getAddress().getId() != null) {
            addressDBO = addressService.findAddressDBOById(workCenter.getAddress().getId());
        }else{
            addressDBO.setId(workCenter.getAddress().getId());
        }
        addressDBO.setMunicipality(workCenter.getAddress().getMunicipality());
        addressDBO.setPostalCode(workCenter.getAddress().getPostalCode());
        addressDBO.setProvince(workCenter.getAddress().getProvince());
        addressDBO.setStreetExtended(workCenter.getAddress().getStreetExtended());
        addressDBO.setStreetName(workCenter.getAddress().getStreetName());
        addressDBO.setStreetType(workCenter.getAddress().getStreetType());
        addressDBO.setLocation(workCenter.getAddress().getLocation());
        addressDBO.setDefaultAddress(workCenter.getAddress().getDefaultAddress());

        WorkCenterDBO workCenterDBO = new WorkCenterDBO();
        workCenterDBO.setId(workCenter.getId());
        workCenterDBO.setContracts(null);
        workCenterDBO.setAddressDBO(addressDBO);

        return workCenterDBO;
    }
}
