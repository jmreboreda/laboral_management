package laboral.domain.work_center;

import laboral.domain.work_center.controller.WorkCenterController;
import laboral.domain.work_center.persistence.dao.WorkCenterDAO;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;

import java.util.List;

public class WorkCenterService {

    private WorkCenterService() {
    }

    public static class WorkCenterServiceFactory {

        private static WorkCenterService workCenterService;

        public static WorkCenterService getInstance() {
            if(workCenterService == null) {
                workCenterService = new WorkCenterService();
            }
            return workCenterService;
        }
    }

    private WorkCenterController workCenterController = new WorkCenterController();
    private WorkCenterDAO workContractDAO = WorkCenterDAO.WorkCenterDAOFactory.getInstance();

    WorkCenterDAO workCenterDAO = WorkCenterDAO.WorkCenterDAOFactory.getInstance();

    public WorkCenterDBO findWorkCenterById(Integer workCenterId){

        return workCenterDAO.findById(workCenterId);
    }

    public WorkCenterDBO findWorkCenterByAddressId(Integer addressId, Integer clientId){

        return workCenterDAO.findWorkCenterByAddressId(addressId, clientId);
    }

    public List<WorkCenterDBO> findByClientId(Integer clientId){

        return workCenterDAO.findByClientId(clientId);
    }
}
