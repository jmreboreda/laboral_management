package laboral.domain.contract;

import java.time.Duration;

public enum WorkDayTypeEnum {

    FULL_TIME("A tiempo completo", Duration.ofHours(40)),
    PARTIAL_TIME("A tiempo parcial", Duration.ofHours(0));

    private String workDayTypeDescription;
    private Duration weeklyWorkHours;

    WorkDayTypeEnum(String workDayTypeDescription, Duration weeklyWorkHours) {
        this.workDayTypeDescription = workDayTypeDescription;
    }

    public String getWorkDayTypeDescription() {
        return workDayTypeDescription;
    }

    public Duration getWeeklyWorkHours() {
        return weeklyWorkHours;
    }
}
