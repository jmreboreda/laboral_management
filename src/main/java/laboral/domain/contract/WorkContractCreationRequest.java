package laboral.domain.contract;

import laboral.domain.contract_json.ContractSchedule;
import laboral.domain.contract_type.WorkContractType;
import laboral.domain.employee.Employee;
import laboral.domain.employer.Employer;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.utilities.utilities.Utilities;
import laboral.domain.work_center.WorkCenter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class WorkContractCreationRequest{

    private Employer employer;
    private Employee employee;
    private WorkContractType workContractType;
    private Integer contractNumber;
    private Integer variationType;
    private LocalDate startDate;
    private LocalDate expectedEndDate;
    private LocalDate modificationDate;
    private LocalDate endDate;
    private ContractSchedule contractSchedule;
    private String laborCategory;
    private QuoteAccountCode quoteAccountCode;
    private String officialIdentificationNumber;
    private String publicNotes;
    private String privateNotes;
    private LocalDateTime clientNotification;
    private WorkCenter workCenter;

    public WorkContractCreationRequest(Employer employer,
                                       Employee employee,
                                       WorkContractType workContractType,
                                       Integer contractNumber,
                                       Integer variationType,
                                       LocalDate startDate,
                                       LocalDate expectedEndDate,
                                       LocalDate modificationDate,
                                       LocalDate endDate,
                                       ContractSchedule contractSchedule,
                                       String laborCategory,
                                       QuoteAccountCode quoteAccountCode,
                                       String officialIdentificationNumber,
                                       String publicNotes,
                                       String privateNotes,
                                       LocalDateTime clientNotification,
                                       WorkCenter workCenter){
        this.employer = employer;
        this.employee = employee;
        this.workContractType = workContractType;
        this.contractNumber = contractNumber;
        this.variationType = variationType;
        this.startDate = startDate;
        this.expectedEndDate = expectedEndDate;
        this.modificationDate = modificationDate;
        this.endDate = endDate;
        this.contractSchedule = contractSchedule;
        this.laborCategory = laborCategory;
        this.quoteAccountCode = quoteAccountCode;
        this.officialIdentificationNumber = officialIdentificationNumber;
        this.publicNotes = publicNotes;
        this.privateNotes = privateNotes;
        this.clientNotification = clientNotification;
        this.workCenter = workCenter;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public WorkContractType getContractType() {
        return workContractType;
    }

    public void setContractType(WorkContractType workContractType) {
        this.workContractType = workContractType;
    }

    public Integer getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(Integer contractNumber) {
        this.contractNumber = contractNumber;
    }

    public Integer getVariationType() {
        return variationType;
    }

    public void setVariationType(Integer variationType) {
        this.variationType = variationType;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(LocalDate expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(LocalDate modificationDate) {
        this.modificationDate = modificationDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public ContractSchedule getContractSchedule() {
        return contractSchedule;
    }

    public void setContractSchedule(ContractSchedule contractSchedule) {
        this.contractSchedule = contractSchedule;
    }

    public String getLaborCategory() {
        return laborCategory;
    }

    public void setLaborCategory(String laborCategory) {
        this.laborCategory = laborCategory;
    }

    public QuoteAccountCode getQuoteAccountCode() {
        return quoteAccountCode;
    }

    public void setQuoteAccountCode(QuoteAccountCode quoteAccountCode) {
        this.quoteAccountCode = quoteAccountCode;
    }

    public String getOfficialIdentificationNumber() {
        return officialIdentificationNumber;
    }

    public void setOfficialIdentificationNumber(String officialIdentificationNumber) {
        this.officialIdentificationNumber = officialIdentificationNumber;
    }

    public String getPublicNotes() {
        return publicNotes;
    }

    public void setPublicNotes(String publicNotes) {
        this.publicNotes = publicNotes;
    }

    public String getPrivateNotes() {
        return privateNotes;
    }

    public void setPrivateNotes(String privateNotes) {
        this.privateNotes = privateNotes;
    }

    public LocalDateTime getClientNotification() {
        return clientNotification;
    }

    public void setClientNotification(LocalDateTime clientNotification) {
        this.clientNotification = clientNotification;
    }

    public WorkCenter getWorkCenter() {
        return workCenter;
    }

    public void setWorkCenter(WorkCenter workCenter) {
        this.workCenter = workCenter;
    }

    public String toFileName(){

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd_MM_yyyy");

        LocalDate fileNameDate = startDate != null ? startDate : endDate;

        return Utilities.replaceWithUnderscore(getEmployer().toAlphabeticalName())
                + "_" +
                Utilities.replaceWithUnderscore(getContractType().getColloquial().toLowerCase())
                + "_" +
                fileNameDate.format(dateFormatter)
                + "_" +
                Utilities.replaceWithUnderscore(getEmployee().toAlphabeticalName());
    }

    public static WorkContractCreationRequestBuilder create() {
        return new WorkContractCreationRequestBuilder();
    }

    public static class WorkContractCreationRequestBuilder {

        private Employer employer;
        private Employee employee;
        private WorkContractType workContractType;
        private Integer contractNumber;
        private Integer variationType;
        private LocalDate startDate;
        private LocalDate expectedEndDate;
        private LocalDate modificationDate;
        private LocalDate endDate;
        private ContractSchedule contractSchedule;
        private String laborCategory;
        private QuoteAccountCode quoteAccountCode;
        private String officialIdentificationNumber;
        private String publicNotes;
        private String privateNotes;
        private LocalDateTime clientNotification;
        private WorkCenter workCenter;

        public WorkContractCreationRequestBuilder withEmployer(Employer employer) {
            this.employer = employer;
            return this;
        }

        public WorkContractCreationRequestBuilder withEmployee(Employee employee) {
            this.employee = employee;
            return this;
        }

        public WorkContractCreationRequestBuilder withContractType(WorkContractType workContractType) {
            this.workContractType = workContractType;
            return this;
        }

        public WorkContractCreationRequestBuilder withContractNumber(Integer workContractNumber) {
            this.contractNumber = workContractNumber;
            return this;
        }

        public WorkContractCreationRequestBuilder withVariationType(Integer variationType) {
            this.variationType = variationType;
            return this;
        }

        public WorkContractCreationRequestBuilder withStartDate(LocalDate startDate) {
            this.startDate = startDate;
            return this;
        }

        public WorkContractCreationRequestBuilder withExpectedEndDate(LocalDate expectedEndDate) {
            this.expectedEndDate = expectedEndDate;
            return this;
        }

        public WorkContractCreationRequestBuilder withModificationDate(LocalDate modificationDate) {
            this.modificationDate = modificationDate;
            return this;
        }

        public WorkContractCreationRequestBuilder withEndDate(LocalDate endDate) {
            this.endDate = endDate;
            return this;
        }

        public WorkContractCreationRequestBuilder withContractSchedule(ContractSchedule contractSchedule) {
            this.contractSchedule = contractSchedule;
            return this;
        }

        public WorkContractCreationRequestBuilder withLaborCategory(String laborCategory) {
            this.laborCategory = laborCategory;
            return this;
        }

        public WorkContractCreationRequestBuilder withQuoteAccountCode(QuoteAccountCode quoteAccountCode) {
            this.quoteAccountCode = quoteAccountCode;
            return this;
        }

        public WorkContractCreationRequestBuilder withIdentificationContractNumberINEM(String officialIdentificationNumber) {
            this.officialIdentificationNumber = officialIdentificationNumber;
            return this;
        }

        public WorkContractCreationRequestBuilder withPublicNotes(String publicNotes) {
            this.publicNotes = publicNotes;
            return this;
        }

        public WorkContractCreationRequestBuilder withPrivateNotes(String privateNotes) {
            this.privateNotes = privateNotes;
            return this;
        }

        public WorkContractCreationRequestBuilder withClientNotification(LocalDateTime clientNotification) {
            this.clientNotification = clientNotification;
            return this;
        }

        public WorkContractCreationRequestBuilder withWorkCenter(WorkCenter workCenter) {
            this.workCenter = workCenter;
            return this;
        }

        public WorkContractCreationRequest build() {
            return new WorkContractCreationRequest(this.employer, this.employee, this.workContractType, this.contractNumber, this.variationType, this.startDate,
                    this.expectedEndDate, this.modificationDate, this.endDate, this.contractSchedule, this.laborCategory, this.quoteAccountCode,
                    this.officialIdentificationNumber, this.publicNotes, this.privateNotes, this.clientNotification, this.workCenter);
        }
    }
}
