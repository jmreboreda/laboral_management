package laboral.domain.contract.work_contract.persistence.dbo;

import com.vladmihalcea.hibernate.type.array.IntArrayType;
import com.vladmihalcea.hibernate.type.array.StringArrayType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeBinaryType;
import com.vladmihalcea.hibernate.type.json.JsonNodeStringType;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import laboral.domain.contract_json.ContractSchedule;
import laboral.domain.employee.persistence.dbo.EmployeeDBO;
import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.quote_account_code.persistence.dbo.QuoteAccountCodeDBO;
import laboral.domain.traceability_contract_documentation.persistence.dbo.TraceabilityContractDocumentationDBO;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import static org.hibernate.annotations.CascadeType.DELETE;

@NamedQueries(value = {
        @NamedQuery(
                name = WorkContractDBO.FIND_ALL_CONTRACTS_ORDERED_BY_CONTRACT_NUMBER,
                query = "select p from WorkContractDBO p order by p.id"
        ),
        @NamedQuery(
                name = WorkContractDBO.FIND_CONTRACT_BY_ID,
                query = "select p from WorkContractDBO p where p.id = :contractId"
        ),
        @NamedQuery(
                name = WorkContractDBO.FIND_CONTRACT_BY_QUOTE_ACCOUNT_CODE_ID,
                query = "select p from WorkContractDBO p where p.quoteAccountCode.id = :quoteAccountCodeId"
        ),
        @NamedQuery(
                name = WorkContractDBO.FIND_ALL_CONTRACTS_BY_CLIENT_ID,
                query = "select p from WorkContractDBO p where p.employerDBO = :code"
        ),
        @NamedQuery(
                name = WorkContractDBO.FIND_ACTIVE_CONTRACT_AT_DATE,
                query = "select p from WorkContractDBO p where p.endingDate is null or endingDate > :date"
        ),
        @NamedQuery(
                name = WorkContractDBO.FIND_INITIAL_CONTRACT_BY_CONTRACT_NUMBER,
                query = "select p from WorkContractDBO p where p.variationTypeCode < 110 and p.endingDate is null and p.contractNumber = :contractNumber"
        ),
        @NamedQuery(
                name = WorkContractDBO.FIND_ALL_BY_WORK_CONTRACT_NUMBER,
                query = "select p from WorkContractDBO p where p.contractNumber = :contractNumber"
        ),
        @NamedQuery(
                name = WorkContractDBO.FIND_LAST_CONTRACT_NUMBER,
                query = "select MAX(contractNumber) from WorkContractDBO"
        ),
        @NamedQuery(
                name = WorkContractDBO.FIND_CONTRACT_VARIATION_BY_VARIOUS_PARAMETERS,
                query = "select p from WorkContractDBO p where p.contractNumber = :contractNumber and p.variationTypeCode = :variationTypeCode and startDate = :startDate"
        )
})

@Entity
@Table(name = "contract")
@TypeDefs({
        @TypeDef(name = "string-array", typeClass = StringArrayType.class),
        @TypeDef(name = "int-array", typeClass = IntArrayType.class),
        @TypeDef(name = "json", typeClass = JsonStringType.class),
        @TypeDef(name = "jsonb", typeClass = JsonBinaryType.class),
        @TypeDef(name = "jsonb-node", typeClass = JsonNodeBinaryType.class),
        @TypeDef(name = "json-node", typeClass = JsonNodeStringType.class),
})

public class WorkContractDBO implements Serializable {
    public static final String FIND_ALL_CONTRACTS_ORDERED_BY_CONTRACT_NUMBER = "WorkContractDBO.FIND_ALL_CONTRACTS_ORDERED_BY_CONTRACT_NUMBER";
    public static final String FIND_CONTRACT_BY_ID = "WorkContractDBO.FIND_CONTRACT_BY_ID";
    public static final String FIND_CONTRACT_BY_QUOTE_ACCOUNT_CODE_ID = "WorkContractDBO.FIND_CONTRACT_BY_QUOTE_ACCOUNT_CODE_ID";
    public static final String FIND_ALL_CONTRACTS_BY_CLIENT_ID = "WorkContractDBO.FIND_ALL_CONTRACTS_BY_CLIENT_ID";
    public static final String FIND_ACTIVE_CONTRACT_AT_DATE = "WorkContractDBO.FIND_ACTIVE_CONTRACT_AT_DATE";
    public static final String FIND_INITIAL_CONTRACT_BY_CONTRACT_NUMBER = "WorkContractDBO.FIND_CONTRACT_BY_CONTRACT_NUMBER";
    public static final String FIND_LAST_CONTRACT_NUMBER = "WorkContractDBO.FIND_LAST_CONTRACT_NUMBER";
    public static final String FIND_ALL_BY_WORK_CONTRACT_NUMBER = "WorkContractDBO.FIND_ALL_BY_WORK_CONTRACT_NUMBER";
    public static final String FIND_CONTRACT_VARIATION_BY_VARIOUS_PARAMETERS ="WorkContractDBO.FIND_CONTRACT_VARIATION_BY_VARIOUS_PARAMETERS";


    @Id
    @SequenceGenerator(name = "contract_id_seq", sequenceName = "contract_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contract_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;
    private Integer contractNumber;
    @OneToOne(fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "employerId")
    private EmployerDBO employerDBO;
    @OneToOne(fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "employeeId")
    private EmployeeDBO employee;
    private Integer contractTypeCode;
    private Integer variationTypeCode;
    private Date startDate;
    private Date expectedEndDate;
    private Date modificationDate;
    private Date endingDate;
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private ContractSchedule contractSchedule;
    private String laborCategory;
    @OneToOne(fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "quoteAccountCodeId")
    private QuoteAccountCodeDBO quoteAccountCode;
    private String officialIdentificationNumber;
    private String publicNotes;
    private String privateNotes;
    private Timestamp clientNotification;
    @OneToOne(fetch = FetchType.LAZY)
    @Cascade({org.hibernate.annotations.CascadeType.SAVE_UPDATE, DELETE})
    @JoinColumn(name = "workCenterId")
    private WorkCenterDBO workCenter;
    @OneToOne(fetch = FetchType.LAZY, cascade= CascadeType.ALL)
    @JoinColumn(name = "traceabilityId")
    private TraceabilityContractDocumentationDBO traceability;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(Integer contractNumber) {
        this.contractNumber = contractNumber;
    }

    public EmployerDBO getEmployerDBO() {
        return employerDBO;
    }

    public void setEmployerDBO(EmployerDBO employerDBO) {
        this.employerDBO = employerDBO;
    }

    public EmployeeDBO getEmployee() {
        return employee;
    }

    public void setEmployee(EmployeeDBO employee) {
        this.employee = employee;
    }

    public Integer getContractTypeCode() {
        return contractTypeCode;
    }

    public void setContractTypeCode(Integer contractTypeCode) {
        this.contractTypeCode = contractTypeCode;
    }

    public Integer getVariationTypeCode() {
        return variationTypeCode;
    }

    public void setVariationTypeCode(Integer variationTypeCode) {
        this.variationTypeCode = variationTypeCode;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(Date expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Date getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(Date endingDate) {
        this.endingDate = endingDate;
    }

    public ContractSchedule getContractSchedule() {
        return contractSchedule;
    }

    public void setContractSchedule(ContractSchedule contractSchedule) {
        this.contractSchedule = contractSchedule;
    }

    public String getLaborCategory() {
        return laborCategory;
    }

    public void setLaborCategory(String laborCategory) {
        this.laborCategory = laborCategory;
    }

    public QuoteAccountCodeDBO getQuoteAccountCode() {
        return quoteAccountCode;
    }

    public void setQuoteAccountCode(QuoteAccountCodeDBO quoteAccountCode) {
        this.quoteAccountCode = quoteAccountCode;
    }

    public String getOfficialIdentificationNumber() {
        return officialIdentificationNumber;
    }

    public void setOfficialIdentificationNumber(String officialIdentificationNumber) {
        this.officialIdentificationNumber = officialIdentificationNumber;
    }

    public String getPublicNotes() {
        return publicNotes;
    }

    public void setPublicNotes(String publicNotes) {
        this.publicNotes = publicNotes;
    }

    public String getPrivateNotes() {
        return privateNotes;
    }

    public void setPrivateNotes(String privateNotes) {
        this.privateNotes = privateNotes;
    }

    public Timestamp getClientNotification() {
        return clientNotification;
    }

    public void setClientNotification(Timestamp clientNotification) {
        this.clientNotification = clientNotification;
    }

    public WorkCenterDBO getWorkCenter() {
        return workCenter;
    }

    public void setWorkCenter(WorkCenterDBO workCenter) {
        this.workCenter = workCenter;
    }


    public TraceabilityContractDocumentationDBO getTraceability() {
        return traceability;
    }

    public void setTraceability(TraceabilityContractDocumentationDBO traceability) {
        this.traceability = traceability;
    }


    public static final class WorkContractDBOBuilder {
        private Integer id;
        private Integer contractNumber;
        private EmployerDBO employerDBO;
        private EmployeeDBO employee;
        private Integer contractTypeCode;
        private Integer variationTypeCode;
        private Date startDate;
        private Date expectedEndDate;
        private Date modificationDate;
        private Date endingDate;
        private ContractSchedule contractSchedule;
        private String laborCategory;
        private QuoteAccountCodeDBO quoteAccountCode;
        private String officialIdentificationNumber;
        private String publicNotes;
        private String privateNotes;
        private Timestamp clientNotification;
        private WorkCenterDBO workCenter;
        private TraceabilityContractDocumentationDBO traceability;

        private WorkContractDBOBuilder() {
        }

        public static WorkContractDBOBuilder create() {
            return new WorkContractDBOBuilder();
        }

        public WorkContractDBOBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public WorkContractDBOBuilder withContractNumber(Integer contractNumber) {
            this.contractNumber = contractNumber;
            return this;
        }

        public WorkContractDBOBuilder withEmployerDBO(EmployerDBO employerDBO) {
            this.employerDBO = employerDBO;
            return this;
        }

        public WorkContractDBOBuilder withEmployee(EmployeeDBO employee) {
            this.employee = employee;
            return this;
        }

        public WorkContractDBOBuilder withContractTypeCode(Integer contractTypeCode) {
            this.contractTypeCode = contractTypeCode;
            return this;
        }

        public WorkContractDBOBuilder withVariationTypeCode(Integer variationType) {
            this.variationTypeCode = variationType;
            return this;
        }

        public WorkContractDBOBuilder withStartDate(Date startDate) {
            this.startDate = startDate;
            return this;
        }

        public WorkContractDBOBuilder withExpectedEndDate(Date expectedEndDate) {
            this.expectedEndDate = expectedEndDate;
            return this;
        }

        public WorkContractDBOBuilder withModificationDate(Date modificationDate) {
            this.modificationDate = modificationDate;
            return this;
        }

        public WorkContractDBOBuilder withEndingDate(Date endingDate) {
            this.endingDate = endingDate;
            return this;
        }

        public WorkContractDBOBuilder withContractSchedule(ContractSchedule contractSchedule) {
            this.contractSchedule = contractSchedule;
            return this;
        }

        public WorkContractDBOBuilder withLaborCategory(String laborCategory) {
            this.laborCategory = laborCategory;
            return this;
        }

        public WorkContractDBOBuilder withQuoteAccountCode(QuoteAccountCodeDBO quoteAccountCode) {
            this.quoteAccountCode = quoteAccountCode;
            return this;
        }

        public WorkContractDBOBuilder withOfficialIdentificationNumber(String officialIdentificationNumber) {
            this.officialIdentificationNumber = officialIdentificationNumber;
            return this;
        }

        public WorkContractDBOBuilder withPublicNotes(String publicNotes) {
            this.publicNotes = publicNotes;
            return this;
        }

        public WorkContractDBOBuilder withPrivateNotes(String privateNotes) {
            this.privateNotes = privateNotes;
            return this;
        }

        public WorkContractDBOBuilder withClientNotification(Timestamp clientNotification) {
            this.clientNotification = clientNotification;
            return this;
        }

        public WorkContractDBOBuilder withWorkCenter(WorkCenterDBO workCenter) {
            this.workCenter = workCenter;
            return this;
        }

        public WorkContractDBOBuilder withTraceability(TraceabilityContractDocumentationDBO traceability) {
            this.traceability = traceability;
            return this;
        }

        public WorkContractDBO build() {
            WorkContractDBO workContractDBO = new WorkContractDBO();
            workContractDBO.setId(id);
            workContractDBO.setContractNumber(contractNumber);
            workContractDBO.setEmployerDBO(employerDBO);
            workContractDBO.setEmployee(employee);
            workContractDBO.setContractTypeCode(contractTypeCode);
            workContractDBO.setVariationTypeCode(variationTypeCode);
            workContractDBO.setStartDate(startDate);
            workContractDBO.setExpectedEndDate(expectedEndDate);
            workContractDBO.setModificationDate(modificationDate);
            workContractDBO.setEndingDate(endingDate);
            workContractDBO.setContractSchedule(contractSchedule);
            workContractDBO.setLaborCategory(laborCategory);
            workContractDBO.setQuoteAccountCode(quoteAccountCode);
            workContractDBO.setOfficialIdentificationNumber(officialIdentificationNumber);
            workContractDBO.setPublicNotes(publicNotes);
            workContractDBO.setPrivateNotes(privateNotes);
            workContractDBO.setClientNotification(clientNotification);
            workContractDBO.setWorkCenter(workCenter);
            workContractDBO.setTraceability(traceability);
            return workContractDBO;
        }
    }
}
