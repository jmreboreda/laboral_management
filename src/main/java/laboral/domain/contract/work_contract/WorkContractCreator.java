package laboral.domain.contract.work_contract;

import laboral.component.work_contract.creation.controllers.WorkContractCreationMainController;
import laboral.component.work_contract.variation.controller.WorkContractVariationMainController;
import laboral.domain.contract.WorkContract;
import laboral.domain.contract.WorkContractCreationRequest;
import laboral.domain.contract.WorkContractScheduleRetrieve;
import laboral.domain.contract.work_contract.controller.WorkContractController;
import laboral.domain.contract_json.ContractSchedule;
import laboral.domain.utilities.utilities.Utilities;
import laboral.domain.variation_type.VariationType;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;

public class WorkContractCreator {

    private WorkContractCreationMainController workContractCreationMainController;
    private WorkContractVariationMainController workContractVariationMainController;

    private final WorkContractController workContractController = new WorkContractController();

    public WorkContractCreator() {
    }

    public Integer persistNewWorkContract(WorkContractCreationMainController workContractCreationMainController) {
        this.workContractCreationMainController = workContractCreationMainController;
        WorkContractCreationRequest workContractCreationRequest = newWorkContractCreationRequestGenerator(null);

        return workContractController.createWorkContract(workContractCreationRequest);
    }

    public WorkContractCreationRequest newWorkContractCreationRequestGenerator(Integer contractNumber){

        VariationType variationType = new VariationType();
        if (workContractCreationMainController.getWorkContractData().getContractTypeSelector().getSelectionModel().getSelectedItem().getAdminPartnerSimilar()) {
            variationType.setVariationCode(101);
        } else if (workContractCreationMainController.getWorkContractData().getContractTypeSelector().getSelectionModel().getSelectedItem().getSurrogate()) {
            variationType.setVariationCode(109);
        } else if (workContractCreationMainController.getWorkContractData().getContractTypeSelector().getSelectionModel().getSelectedItem()
                .getColloquial().contains(WorkContractCreationMainController.getEmployeeOfNewClient())) {
            variationType.setVariationCode(108);
        } else {
            variationType.setVariationCode(100);
        }

        LocalDate notificationDate = workContractCreationMainController.getWorkContractData().getNotificationDate().getValue();
        LocalTime notificationTime = Utilities.convertStringToLocalTime(workContractCreationMainController.getWorkContractData().getNotificationHour().getText());
        LocalDate startDate = workContractCreationMainController.getWorkContractData().getDateFrom().getValue();
        LocalDate expectedEndDate = workContractCreationMainController.getWorkContractData().getDateTo() == null ? null : workContractCreationMainController.getWorkContractData().getDateTo().getValue();

        WorkContractScheduleRetrieve workContractScheduleRetrieve = new WorkContractScheduleRetrieve();
        ContractSchedule completeSchedule = workContractScheduleRetrieve.retrieveWorkContractSchedule(this.workContractCreationMainController.getWorkContractSchedule());

        if(contractNumber == null) {
            contractNumber = workContractController.findLastContractNumber() + 1;
        }

       return WorkContractCreationRequest.create()
                .withContractNumber(contractNumber)
                .withEmployer(workContractCreationMainController.getWorkContractParts().getEmployerSelector().getSelectionModel().getSelectedItem())
                .withEmployee(workContractCreationMainController.getWorkContractParts().getEmployeeSelector().getSelectionModel().getSelectedItem())
                .withContractType(workContractCreationMainController.getWorkContractData().getContractTypeSelector().getSelectionModel().getSelectedItem())
                .withWorkCenter(workContractCreationMainController.getWorkContractParts().getWorkCenterSelector().getSelectionModel().getSelectedItem())
                .withVariationType(variationType.getVariationCode())
                .withStartDate(startDate)
                .withExpectedEndDate(expectedEndDate)
                .withModificationDate(null)
                .withEndDate(null)
                .withContractSchedule(completeSchedule)
                .withLaborCategory(workContractCreationMainController.getWorkContractData().getLaborCategory().getText())
                .withQuoteAccountCode(workContractCreationMainController.getWorkContractParts().getQuoteAccountCodeSelector().getSelectionModel().getSelectedItem())
                .withIdentificationContractNumberINEM(null)
                .withPublicNotes(workContractCreationMainController.getWorkContractPublicNotes().getPublicNotes().getText())
                .withPrivateNotes(workContractCreationMainController.getWorkContractPrivateNotes().getPrivateNotes().getText())
                .withClientNotification(LocalDateTime.of(notificationDate, notificationTime))
                .build();
    }

    public Integer persistWorkContractExtinction(WorkContractVariationMainController workContractVariationMainController) {
        this.workContractVariationMainController = workContractVariationMainController;

        Integer variationTypeCode = workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtinction().getExtinctionCauseSelector()
                .getSelectionModel().getSelectedItem().getVariationCode();

        LocalDate notificationDate = workContractVariationMainController.getDateHourNotification().getNotificationDate().getValue();
        LocalTime notificationTime = Utilities.convertStringToLocalTime(workContractVariationMainController.getDateHourNotification().getNotificationHour().getText());
        LocalDate startDate = workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtinction().getDateFrom().getValue();

        WorkContract workContractToVariation = retrieveWorkContractToVariation();
        Integer workContractNumber = workContractVariationMainController.getWorkContractVariationSelector().getWorkContractSelector().getSelectionModel().getSelectedItem().getContractNumber();

        WorkContractCreationRequest workContractCreationRequest = WorkContractCreationRequest.create()
                .withContractNumber(workContractNumber)
                .withEmployer(workContractVariationMainController.getWorkContractVariationSelector().getEmployerSelector().getSelectionModel().getSelectedItem())
                .withEmployee(workContractVariationMainController.getWorkContractVariationSelector().getEmployeeSelector().getSelectionModel().getSelectedItem())
                .withContractType(workContractVariationMainController.getWorkContractVariationSelector().getWorkContractSelector().getSelectionModel().getSelectedItem().getContractType())
                .withWorkCenter(workContractToVariation.getWorkCenter())
                .withVariationType(variationTypeCode)
                .withStartDate(startDate)
                .withExpectedEndDate(null)
                .withModificationDate(null)
                .withEndDate(null)
                .withContractSchedule(new ContractSchedule(new HashMap<>()))
                .withLaborCategory(workContractToVariation.getLaborCategory())
                .withQuoteAccountCode(workContractToVariation.getQuoteAccountCode())
                .withIdentificationContractNumberINEM(workContractToVariation.getOfficialIdentificationNumber())
                .withPublicNotes(workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtinction().getPublicNotes().getText())
                .withPrivateNotes(workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtinction().getPrivateNotes().getText())
                .withClientNotification(LocalDateTime.of(notificationDate, notificationTime))
                .build();

        Integer workContractExtinctionId = workContractController.createWorkContract(workContractCreationRequest);

        Date endingDate = Date.valueOf(workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtinction().getDateFrom().getValue());
        workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtinction().updateEndingDateInInitialWorkContract(workContractNumber, endingDate);
        workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtinction().updateModificationDateInAllWorkContractVariation(workContractNumber, endingDate);

        return workContractExtinctionId;
    }



//    private void updateEndingDateInInitialWorkContract(Integer contractNumber, Date endingDate){
//        WorkContractService workContractService = new WorkContractService();
//        WorkContractDBO workContractDBO = workContractService.findInitialWorkContractByWorkContractNumber(contractNumber);
//        workContractDBO.setModificationDate(endingDate);
//        workContractDBO.setEndingDate(endingDate);
//        workContractService.update(workContractDBO);
//    }

//    private void updateModificationDateInAllWorkContractVariation(Integer contractNumber, Date endingDate){
//        WorkContractService workContractService = new WorkContractService();
//        VariationTypeService variationTypeService = new VariationTypeService();
//        List<WorkContractDBO> workContractDBOList = workContractService.findAllWorkContractVariationByContractNumber(contractNumber);
//        for(WorkContractDBO workContractDBO : workContractDBOList){
//            VariationTypeDBO variationTypeDBO = variationTypeService.findByVariationTypeCode(workContractDBO.getVariationType());
//            if(workContractDBO.getModificationDate() == null && !variationTypeDBO.getExtinction()){
//                workContractDBO.setModificationDate(endingDate);
//                workContractService.update(workContractDBO);
//            }
//        }
//    }

    public Integer persistWorkContractExtension(WorkContractVariationMainController workContractVariationMainController) {
        this.workContractVariationMainController = workContractVariationMainController;

        Integer variationTypeCode = 220;

        LocalDate notificationDate = workContractVariationMainController.getDateHourNotification().getNotificationDate().getValue();
        LocalTime notificationTime = Utilities.convertStringToLocalTime(workContractVariationMainController.getDateHourNotification().getNotificationHour().getText());
        LocalDate startDate = workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtension().getDateFrom().getValue();
        LocalDate expectedEndDate = workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtension().getDateTo().getValue();

        WorkContract workContractToVariation = retrieveWorkContractToVariation();
        Integer workContractNumber = workContractVariationMainController.getWorkContractVariationSelector().getWorkContractSelector().getSelectionModel().getSelectedItem().getContractNumber();

        ContractSchedule contractSchedule = workContractToVariation.getContractSchedule();

        WorkContractCreationRequest workContractCreationRequest = WorkContractCreationRequest.create()
                .withContractNumber(workContractNumber)
                .withEmployer(workContractVariationMainController.getWorkContractVariationSelector().getEmployerSelector().getSelectionModel().getSelectedItem())
                .withEmployee(workContractVariationMainController.getWorkContractVariationSelector().getEmployeeSelector().getSelectionModel().getSelectedItem())
                .withContractType(workContractToVariation.getContractType())
                .withWorkCenter(workContractToVariation.getWorkCenter())
                .withVariationType(variationTypeCode)
                .withStartDate(startDate)
                .withExpectedEndDate(expectedEndDate)
                .withModificationDate(null)
                .withEndDate(null)
                .withContractSchedule(contractSchedule)
                .withLaborCategory(workContractToVariation.getLaborCategory())
                .withQuoteAccountCode(workContractToVariation.getQuoteAccountCode())
                .withIdentificationContractNumberINEM(workContractToVariation.getOfficialIdentificationNumber() + "-1")
                .withPublicNotes(workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtension().getPublicNotes().getText())
                .withPrivateNotes(workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtension().getPrivateNotes().getText())
                .withClientNotification(LocalDateTime.of(notificationDate, notificationTime))
                .build();

        Integer workContractExtensionId = workContractController.createWorkContract(workContractCreationRequest);

        Date modificationDate = Date.valueOf(workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtension().getDateFrom().getValue().minusDays(1));
        workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtension().updateModificationDateInAllWorkContractVariation(workContractNumber, modificationDate);

        return workContractExtensionId;
    }


    public Integer persistWorkContractHoursWorkWeekChange(WorkContractVariationMainController workContractVariationMainController){
        this.workContractVariationMainController = workContractVariationMainController;

        Integer variationTypeCode = 230;

        LocalDate notificationDate = workContractVariationMainController.getDateHourNotification().getNotificationDate().getValue();
        LocalTime notificationTime = Utilities.convertStringToLocalTime(workContractVariationMainController.getDateHourNotification().getNotificationHour().getText());
        LocalDate startDate = workContractVariationMainController.getWorkContractVariationVariation().getWorkContractVariationSchedule().getStartDate().getValue();
        LocalDate expectedEndDate = workContractVariationMainController.getWorkContractVariationVariation().getWorkContractVariationSchedule().getExpectedEndDate().getValue();

        WorkContract workContractToVariation = retrieveWorkContractToVariation();
        Integer workContractNumber = workContractVariationMainController.getWorkContractVariationSelector().getWorkContractSelector().getSelectionModel().getSelectedItem().getContractNumber();

        WorkContractScheduleRetrieve workContractScheduleRetrieve = new WorkContractScheduleRetrieve();
        ContractSchedule completeSchedule = workContractScheduleRetrieve.retrieveWorkContractVariationSchedule(workContractVariationMainController.getWorkContractVariationVariation()
        .getWorkContractVariationSchedule());

        WorkContractCreationRequest workContractCreationRequest = WorkContractCreationRequest.create()
                .withContractNumber(workContractNumber)
                .withEmployer(workContractVariationMainController.getWorkContractVariationSelector().getEmployerSelector().getSelectionModel().getSelectedItem())
                .withEmployee(workContractVariationMainController.getWorkContractVariationSelector().getEmployeeSelector().getSelectionModel().getSelectedItem())
                .withContractType(workContractToVariation.getContractType())
                .withWorkCenter(workContractToVariation.getWorkCenter())
                .withVariationType(variationTypeCode)
                .withStartDate(startDate)
                .withExpectedEndDate(expectedEndDate)
                .withModificationDate(null)
                .withEndDate(null)
                .withContractSchedule(completeSchedule)
                .withLaborCategory(workContractToVariation.getLaborCategory())
                .withQuoteAccountCode(workContractToVariation.getQuoteAccountCode())
                .withIdentificationContractNumberINEM(workContractToVariation.getOfficialIdentificationNumber())
                .withPublicNotes(null)
                .withPrivateNotes(null)
                .withClientNotification(LocalDateTime.of(notificationDate, notificationTime))
                .build();

        Integer workContractHoursWorkWeekId = workContractController.createWorkContract(workContractCreationRequest);

        Date modificationDate = Date.valueOf(workContractVariationMainController.getWorkContractVariationVariation().getWorkContractVariationSchedule().getStartDate().getValue().minusDays(1));
        workContractVariationMainController.getWorkContractVariationVariation().getWorkContractVariationSchedule().updateModificationDateInAllWorkContractVariation(workContractNumber, modificationDate);

        return workContractHoursWorkWeekId;
    }

    //TODO
    public Integer persistWorkContractSpecial(WorkContractVariationMainController workContractVariationMainController) {
        this.workContractVariationMainController = workContractVariationMainController;

        VariationType variationType = workContractVariationMainController.getWorkContractVariationVariation().getWorkContractSpecial().getSpecialVariationSelector()
                .getSelectionModel().getSelectedItem();
        Integer variationTypeCode = variationType.getVariationCode();

        WorkContract workContractToVariation = retrieveWorkContractToVariation();

        LocalDate notificationDate = workContractVariationMainController.getDateHourNotification().getNotificationDate().getValue();
        LocalTime notificationTime = Utilities.convertStringToLocalTime(workContractVariationMainController.getDateHourNotification().getNotificationHour().getText());

        LocalDate startDate = null;
        LocalDate expectedEndDate = retrieveWorkContractToVariation().getExpectedEndDate();

        if(variationType.getSpecialInitial() && !variationType.getReincorporation()){
            startDate = workContractVariationMainController.getWorkContractVariationVariation().getWorkContractSpecial().getStartDate().getValue();
            if( expectedEndDate == null){
                expectedEndDate = workContractVariationMainController.getWorkContractVariationVariation().getWorkContractSpecial().getExpectedEndDate().getValue();
            }
        }else if(variationType.getReincorporation()){
            startDate = workContractVariationMainController.getWorkContractVariationVariation().getWorkContractSpecial().getRestartDate().getValue();
            if(expectedEndDate == null) {
                expectedEndDate = workContractVariationMainController.getWorkContractVariationVariation().getWorkContractSpecial().getExpectedEndDate().getValue();
            }
        }else if(variationType.getSpecialFinal()){
            startDate = workContractVariationMainController.getWorkContractVariationVariation().getWorkContractSpecial().getEndDate().getValue();
            if(expectedEndDate == null) {
                expectedEndDate = workContractVariationMainController.getWorkContractVariationVariation().getWorkContractSpecial().getExpectedEndDate().getValue();
            }
        }

        ContractSchedule contractSchedule = workContractToVariation.getContractSchedule();

        Integer contractNumber = workContractVariationMainController.getWorkContractVariationSelector().getWorkContractSelector().getSelectionModel().getSelectedItem().getContractNumber();

        WorkContractCreationRequest workContractCreationRequest = WorkContractCreationRequest.create()
                .withContractNumber(contractNumber)
                .withEmployer(workContractVariationMainController.getWorkContractVariationSelector().getEmployerSelector().getSelectionModel().getSelectedItem())
                .withEmployee(workContractVariationMainController.getWorkContractVariationSelector().getEmployeeSelector().getSelectionModel().getSelectedItem())
                .withContractType(workContractToVariation.getContractType())
                .withWorkCenter(workContractToVariation.getWorkCenter())
                .withVariationType(variationTypeCode)
                .withStartDate(startDate)
                .withExpectedEndDate(expectedEndDate)
                .withModificationDate(null)
                .withEndDate(null)
                .withContractSchedule(contractSchedule)
                .withLaborCategory(workContractToVariation.getLaborCategory())
                .withQuoteAccountCode(workContractToVariation.getQuoteAccountCode())
                .withIdentificationContractNumberINEM(workContractToVariation.getOfficialIdentificationNumber())
                .withPublicNotes(workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtension().getPublicNotes().getText())
                .withPrivateNotes(workContractVariationMainController.getWorkContractVariationVariation().getWorkContractExtension().getPrivateNotes().getText())
                .withClientNotification(LocalDateTime.of(notificationDate, notificationTime))
                .build();

        Integer workContractVariationSpecialId = workContractController.createWorkContract(workContractCreationRequest);

        Date modificationDate = Date.valueOf(startDate.minusDays(1));
        workContractVariationMainController.getWorkContractVariationVariation().getWorkContractSpecial().updateModificationDateInAllWorkContractVariation(workContractToVariation, modificationDate);

        return workContractVariationSpecialId;
    }

    private WorkContract retrieveWorkContractToVariation() {
        Integer workContractNumber = workContractVariationMainController.getWorkContractVariationSelector().getWorkContractSelector().getSelectionModel().getSelectedItem().getContractNumber();
        List<WorkContract> workContractVariationList = workContractController.findAllByWorkContractNumber(workContractNumber);

        WorkContract workContractToVariation = null;
        for (WorkContract workContract : workContractVariationList) {
            if (workContract.getModificationDate() == null) {
                workContractToVariation = workContract;
            }
        }

        return workContractToVariation;
    }
}
