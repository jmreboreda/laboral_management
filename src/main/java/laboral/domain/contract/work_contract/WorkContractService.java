package laboral.domain.contract.work_contract;

import laboral.domain.contract.WorkContract;
import laboral.domain.contract.work_contract.controller.WorkContractController;
import laboral.domain.contract.work_contract.persistence.dao.WorkContractDAO;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;

import java.time.LocalDate;
import java.util.List;

public class WorkContractService {


    private final WorkContractController workContractController = new WorkContractController();
    private final WorkContractDAO workContractDAO = WorkContractDAO.ContractDAOFactory.getInstance();

    public Integer create(WorkContractDBO workContractDBO){

        return workContractDAO.create(workContractDBO);
    }


    public Integer update(WorkContractDBO workContractDBO){

        return workContractDAO.update(workContractDBO);
    }

    public WorkContractDBO findInitialWorkContractByWorkContractNumber(Integer workContractNumber){

        return workContractDAO.findInitialWorkContractByWorkContractNumber(workContractNumber);
    }

    public List<WorkContractDBO> findAllWorkContractVariationByContractNumber(Integer workContractNumber){

        return workContractDAO.findAllByWorkContractNumber(workContractNumber);
    }

    public WorkContractDBO findById(Integer id){
        return workContractDAO.findById(id);
    }

    public WorkContractDBO findWorkContractVariationDBOByVariousParameters(Integer workContractNumber, Integer variationTypeCode, LocalDate startDate){

        return workContractDAO.findWorkContractVariationDBOByVariousParameters(workContractNumber, variationTypeCode, startDate);
    }
}
