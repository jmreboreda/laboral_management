package laboral.domain.contract.work_contract.manager;

import laboral.domain.contract.WorkContract;
import laboral.domain.contract.WorkContractCreationRequest;
import laboral.domain.contract.work_contract.mapper.MapperWorkContractCreationRequestToWorkContractDBO;
import laboral.domain.contract.work_contract.mapper.MapperWorkContractDBOToWorkContract;
import laboral.domain.contract.work_contract.persistence.dao.WorkContractDAO;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class WorkContractManager {

    WorkContractDAO workContractDAO = WorkContractDAO.ContractDAOFactory.getInstance();

    public Integer createWorkContract(WorkContractCreationRequest workContractCreationRequest){

        MapperWorkContractCreationRequestToWorkContractDBO mapperWorkContractCreationRequestToWorkContractDBO = new MapperWorkContractCreationRequestToWorkContractDBO();


        return workContractDAO.create(mapperWorkContractCreationRequestToWorkContractDBO.map(workContractCreationRequest));
    }

    public WorkContract findById(Integer workContractId){
        MapperWorkContractDBOToWorkContract mapperWorkContractDBOToWorkContract = new MapperWorkContractDBOToWorkContract();

        WorkContractDBO workContractDBO = workContractDAO.findById(workContractId);

        return mapperWorkContractDBOToWorkContract.map(workContractDBO);
    }

    public WorkContract findInitialWorkContractByWorkContractNumber(Integer workContractNumber){

        MapperWorkContractDBOToWorkContract mapperWorkContractDBOToWorkContract = new MapperWorkContractDBOToWorkContract();

        WorkContractDBO workContractDBO = workContractDAO.findInitialWorkContractByWorkContractNumber(workContractNumber);

        return mapperWorkContractDBOToWorkContract.map(workContractDBO);
    }

    public List<WorkContract> findActiveWorkContractAtDate(LocalDate date){

        List<WorkContractDBO> workContractDBOList = workContractDAO.findActiveWorkContractAtDate(date);

        MapperWorkContractDBOToWorkContract mapperWorkContractDBOToWorkContract = new MapperWorkContractDBOToWorkContract();
        List<WorkContract> workContractList = new ArrayList<>();
        for(WorkContractDBO workContractDBO : workContractDBOList){
            WorkContract workContract = mapperWorkContractDBOToWorkContract.map(workContractDBO);
            workContractList.add(workContract);
        }

        return workContractList;
    }

    public List<WorkContract> findByQuoteAccountCodeId(Integer quoteAccountCodeId){

        List<WorkContract> workContractList = new ArrayList<>();
        MapperWorkContractDBOToWorkContract mapperWorkContractDBOToWorkContract = new MapperWorkContractDBOToWorkContract();

        List<WorkContractDBO> workContractDBOList = workContractDAO.findByQuoteAccountCodeId(quoteAccountCodeId);
        for(WorkContractDBO workContractDBO : workContractDBOList){
            WorkContract workContract = mapperWorkContractDBOToWorkContract.map(workContractDBO);
            workContractList.add(workContract);
        }

        return workContractList;
    }

    public List<WorkContract> findAllByWorkContractNumber(Integer contractNumber){

        MapperWorkContractDBOToWorkContract mapperWorkContractDBOToWorkContract = new MapperWorkContractDBOToWorkContract();
        List<WorkContract> workContractList = new ArrayList<>();

        List<WorkContractDBO> workContractDBOList = workContractDAO.findAllByWorkContractNumber(contractNumber);
        for(WorkContractDBO workContractDBO : workContractDBOList){
            WorkContract workContract = mapperWorkContractDBOToWorkContract.map(workContractDBO);
            workContractList.add(workContract);
        }

        return workContractList;
    }

    public Integer findLastContractNumber(){

        return workContractDAO.findLastContractNumber();
    }
}
