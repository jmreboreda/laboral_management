package laboral.domain.contract.work_contract.mapper;

import laboral.domain.contract.WorkContract;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.contract_type.WorkContractType;
import laboral.domain.contract_type.WorkContractTypeService;
import laboral.domain.contract_type.mapper.MapperWorkContractTypeDBOToWorkContractType;
import laboral.domain.contract_type.persistence.dbo.WorkContractTypeDBO;
import laboral.domain.employee.EmployeeService;
import laboral.domain.employee.persistence.dbo.EmployeeDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.person.Person;
import laboral.domain.person.PersonService;
import laboral.domain.person.mapper.MapperPersonDBOToPerson;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.quote_account_code.mapper.MapperQuoteAccountCodeDBOToQuoteAccountCode;
import laboral.domain.variation_type.VariationType;
import laboral.domain.variation_type.VariationTypeService;
import laboral.domain.variation_type.mapper.MapperVariationTypeDBOToVariationType;
import laboral.domain.variation_type.persistence.dbo.VariationTypeDBO;
import laboral.domain.work_center.WorkCenter;
import laboral.domain.work_center.WorkCenterService;
import laboral.domain.work_center.mapper.MapperWorkCenterDBOToWorkCenter;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;

import java.time.LocalDate;

public class MapperWorkContractDBOToWorkContract implements GenericMapper<WorkContractDBO, WorkContract> {

    @Override
    public WorkContract map(WorkContractDBO workContractDBO) {

        MapperPersonDBOToPerson mapperPersonDBOToPerson = new MapperPersonDBOToPerson();
        PersonService personService = PersonService.PersonServiceFactory.getInstance();
        PersonDBO employerDBO = personService.findPersonById(workContractDBO.getEmployerDBO().getClientDBO().getPersonDBO().getId());
        Person employer = mapperPersonDBOToPerson.map(employerDBO);

        EmployeeService employeeService = new EmployeeService();
        EmployeeDBO employeeDBO = employeeService.findByPersonId(workContractDBO.getEmployee().getPersonDBO().getId());
        PersonDBO personDBO = personService.findPersonById(employeeDBO.getId());
        Person employee = mapperPersonDBOToPerson.map(personDBO);

        MapperWorkContractTypeDBOToWorkContractType mapperWorkContractTypeDBOToWorkContractType = new MapperWorkContractTypeDBOToWorkContractType();
        WorkContractTypeService workContractTypeService = WorkContractTypeService.WorkContractTypeServiceFactory.getInstance();

        WorkContractTypeDBO workContractTypeDBO = workContractTypeService.findWorkContractTypeByContractTypeCode(workContractDBO.getContractTypeCode());
        WorkContractType workContractType = mapperWorkContractTypeDBOToWorkContractType.map(workContractTypeDBO);

        MapperVariationTypeDBOToVariationType mapperVariationTypeDBOToVariationType = new MapperVariationTypeDBOToVariationType();
        VariationTypeService variationTypeService = new VariationTypeService();
        VariationTypeDBO variationTypeDBO = variationTypeService.findByVariationTypeCode(workContractDBO.getVariationTypeCode());
        VariationType variationType = mapperVariationTypeDBOToVariationType.map(variationTypeDBO);

        MapperWorkCenterDBOToWorkCenter mapperWorkCenterDBOToWorkCenter = new MapperWorkCenterDBOToWorkCenter();
        WorkCenterService workCenterService = WorkCenterService.WorkCenterServiceFactory.getInstance();
        WorkCenterDBO workCenterDBO = workContractDBO.getWorkCenter() == null ? null :
                workCenterService.findWorkCenterById(workContractDBO.getWorkCenter().getId());
        WorkCenter workCenter = workContractDBO.getWorkCenter() == null ? null : mapperWorkCenterDBOToWorkCenter.map(workCenterDBO);

        MapperQuoteAccountCodeDBOToQuoteAccountCode mapperQuoteAccountCodeDBOToQuoteAccountCode = new MapperQuoteAccountCodeDBOToQuoteAccountCode();
        QuoteAccountCode quoteAccountCode = mapperQuoteAccountCodeDBOToQuoteAccountCode.map(workContractDBO.getQuoteAccountCode());
        LocalDate startDate = workContractDBO.getStartDate() == null ? null : workContractDBO.getStartDate().toLocalDate();
        LocalDate expectedEndDate = workContractDBO.getExpectedEndDate() == null ? null : workContractDBO.getExpectedEndDate().toLocalDate();
        LocalDate modificationDate = workContractDBO.getModificationDate() == null ? null : workContractDBO.getModificationDate().toLocalDate();
        LocalDate endDate = workContractDBO.getEndingDate() == null ? null : workContractDBO.getEndingDate().toLocalDate();

        return WorkContract.create()
                .withId(workContractDBO.getId())
                .withContractNumber(workContractDBO.getContractNumber())
                .withEmployer(employer)
                .withEmployee(employee)
                .withContractType(workContractType)
                .withIdentificationContractNumberINEM(workContractDBO.getOfficialIdentificationNumber())
                .withDateSign(null)
                .withStartDate(startDate)
                .withExpectedEndDate(expectedEndDate)
                .withModificationDate(modificationDate)
                .withEndDate(endDate)
                .withLaborCategory(workContractDBO.getLaborCategory())
                .withPublicNotes(workContractDBO.getPublicNotes())
                .withPrivateNotes(workContractDBO.getPrivateNotes())
                .withContractSchedule(workContractDBO.getContractSchedule())
                .withVariationType(variationType)
                .withWorkCenter(workCenter)
                .withQuoteAccountCode(quoteAccountCode)
                .withClientNotification(workContractDBO.getClientNotification().toLocalDateTime())
                .build();
    }
}
