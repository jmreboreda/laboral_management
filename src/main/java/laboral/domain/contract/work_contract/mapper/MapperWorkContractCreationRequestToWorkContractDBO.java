package laboral.domain.contract.work_contract.mapper;

import laboral.domain.contract.WorkContractCreationRequest;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.employee.EmployeeService;
import laboral.domain.employee.persistence.dbo.EmployeeDBO;
import laboral.domain.employer.EmployerService;
import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.quote_account_code.QuoteAccountCodeService;
import laboral.domain.quote_account_code.persistence.dbo.QuoteAccountCodeDBO;
import laboral.domain.work_center.WorkCenterService;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;

import java.sql.Date;
import java.sql.Timestamp;

public class MapperWorkContractCreationRequestToWorkContractDBO implements GenericMapper<WorkContractCreationRequest, WorkContractDBO> {

    @Override
    public WorkContractDBO map(WorkContractCreationRequest workContractCreationRequest) {

        Timestamp registrationTimeStamp = Timestamp.valueOf(workContractCreationRequest.getClientNotification());

        EmployerService employerDBOService = new EmployerService();
        EmployerDBO employerDBO = employerDBOService.findByClientId(workContractCreationRequest.getEmployer().getClientDBO().getId());

        EmployeeService employeeService = new EmployeeService();
        EmployeeDBO employeeDBO = employeeService.findByPersonId(workContractCreationRequest.getEmployee().getPersonId());

        WorkCenterService workCenterService = WorkCenterService.WorkCenterServiceFactory.getInstance();
        WorkCenterDBO workCenterDBO = workContractCreationRequest.getWorkCenter() == null ? null : workCenterService.findWorkCenterById(workContractCreationRequest.getWorkCenter().getId());

        QuoteAccountCodeDBO quoteAccountCodeDBO;
        if(workContractCreationRequest.getQuoteAccountCode() != null) {
            QuoteAccountCodeService quoteAccountCodeService = new QuoteAccountCodeService();
            quoteAccountCodeDBO = quoteAccountCodeService.findById(workContractCreationRequest.getQuoteAccountCode().getId());
        }else{
            quoteAccountCodeDBO = null;
        }

        Date expectedEndDate = workContractCreationRequest.getExpectedEndDate() == null ? null : Date.valueOf(workContractCreationRequest.getExpectedEndDate());
        Date modificationDate = workContractCreationRequest.getModificationDate() == null ? null : Date.valueOf(workContractCreationRequest.getModificationDate());
        Date endDate = workContractCreationRequest.getEndDate() == null ? null : Date.valueOf(workContractCreationRequest.getEndDate());

        WorkContractDBO workContractDBO = new WorkContractDBO();
        workContractDBO.setId(null);
        workContractDBO.setContractNumber(workContractCreationRequest.getContractNumber());
        workContractDBO.setEmployerDBO(employerDBO);
        workContractDBO.setEmployee(employeeDBO);
        workContractDBO.setContractTypeCode(workContractCreationRequest.getContractType().getContractCode());
        workContractDBO.setContractSchedule(workContractCreationRequest.getContractSchedule());
        workContractDBO.setWorkCenter(workCenterDBO);
        workContractDBO.setVariationTypeCode(workContractCreationRequest.getVariationType());
        workContractDBO.setStartDate(Date.valueOf(workContractCreationRequest.getStartDate()));
        workContractDBO.setExpectedEndDate(expectedEndDate);
        workContractDBO.setModificationDate(modificationDate);
        workContractDBO.setEndingDate(endDate);
        workContractDBO.setLaborCategory(workContractCreationRequest.getLaborCategory());
        workContractDBO.setQuoteAccountCode(quoteAccountCodeDBO);
        workContractDBO.setPublicNotes(workContractCreationRequest.getPublicNotes());
        workContractDBO.setPrivateNotes(workContractCreationRequest.getPrivateNotes());
        workContractDBO.setClientNotification(registrationTimeStamp);

        return workContractDBO;
    }
}
