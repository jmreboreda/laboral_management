package laboral.domain.contract.work_contract.controller;

import laboral.domain.contract.WorkContract;
import laboral.domain.contract.WorkContractCreationRequest;
import laboral.domain.contract.work_contract.manager.WorkContractManager;

import java.time.LocalDate;
import java.util.List;

public class WorkContractController {

    WorkContractManager workContractManager = new WorkContractManager();

    public Integer createWorkContract(WorkContractCreationRequest workContractCreationRequest){

        return workContractManager.createWorkContract(workContractCreationRequest);
    }

    public WorkContract findById(Integer workContractId){

        return workContractManager.findById(workContractId);
    }



    public List<WorkContract> findAllByWorkContractNumber(Integer workContractNumber){

        return workContractManager.findAllByWorkContractNumber(workContractNumber);
    }

    public List<WorkContract> findByQuoteAccountCodeId(Integer quoteAccountCodeId){

        return workContractManager.findByQuoteAccountCodeId(quoteAccountCodeId);
    }

    public Integer findLastContractNumber(){

        return workContractManager.findLastContractNumber();

    }

    public List<WorkContract> findActiveWorkContractAtDate(LocalDate date ){

        return workContractManager.findActiveWorkContractAtDate(date);

    }
}
