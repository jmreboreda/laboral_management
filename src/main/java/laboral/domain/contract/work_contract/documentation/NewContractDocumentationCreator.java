package laboral.domain.contract.work_contract.documentation;

import com.lowagie.text.DocumentException;
import laboral.ApplicationConstants;
import laboral.component.work_contract.ContractConstants;
import laboral.domain.contract.WorkContractCreationRequest;
import laboral.domain.utilities.OSUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

public class NewContractDocumentationCreator {


    public NewContractDocumentationCreator() {
    }

    public Path retrievePathToContractDataSubFolderPDF(WorkContractCreationRequest workContractCreationRequest){
        Path pathOut = null;

        final Optional<Path> maybePath = OSUtils.TemporalFolderUtils.tempFolder();
        String temporalDir = maybePath.get().toString();

        Path pathToContractDataSubFolder = Paths.get(ApplicationConstants.USER_HOME.toString(), temporalDir, workContractCreationRequest.toFileName().concat(ApplicationConstants.PDF_EXTENSION.toString()));
        try {
            Files.createDirectories(pathToContractDataSubFolder.getParent());
            pathOut = NewContractDataSubFolderPDFCreator.createContractDataSubFolderPDF(workContractCreationRequest, ContractConstants.STANDARD_NEW_CONTRACT_TEXT, pathToContractDataSubFolder);
        } catch (IOException | DocumentException e) {
            e.printStackTrace();
        }

        return pathOut;
    }





}
