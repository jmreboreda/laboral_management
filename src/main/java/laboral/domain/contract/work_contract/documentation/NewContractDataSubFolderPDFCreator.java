package laboral.domain.contract.work_contract.documentation;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import laboral.ApplicationConstants;
import laboral.component.work_contract.ContractConstants;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.contract.WorkContractCreationRequest;
import laboral.domain.contract_json.ContractDaySchedule;
import laboral.domain.person.PersonService;
import laboral.domain.person.persistence.dbo.PersonDBO;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.time.DayOfWeek;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;

import static java.time.temporal.ChronoUnit.DAYS;

public class NewContractDataSubFolderPDFCreator {

//    private static final String PATH_TO_PDF_TEMPLATE = "/pdf_forms/DGM_003_Datos_Alta_o_Cambio_Contrato_Trabajo.pdf";
    private static final String PATH_TO_PDF_TEMPLATE = "/pdf_forms/DatosParaContratoDeTrabajo_NEW_VERSION.pdf";


    public NewContractDataSubFolderPDFCreator() {
    }

    public static Path createContractDataSubFolderPDF(WorkContractCreationRequest workContractCreationRequest, String notificationType, Path pathOut) throws IOException, DocumentException {

        PdfReader reader = new PdfReader(PATH_TO_PDF_TEMPLATE);
        PdfStamper stamp = new PdfStamper(reader, new FileOutputStream(pathOut.toString()));
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.toString());
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_TIME_FORMAT.toString());

        String startDate = workContractCreationRequest.getStartDate() != null ? workContractCreationRequest.getStartDate().format(dateFormatter) : "";
        String expectedEndDate = workContractCreationRequest.getExpectedEndDate() != null ? workContractCreationRequest.getExpectedEndDate().format(dateFormatter) : "";
        Long durationDays = null;
        if(!expectedEndDate.equals("")){
            durationDays = DAYS.between(workContractCreationRequest.getStartDate(), workContractCreationRequest.getExpectedEndDate()) + 1;
        }

        PersonService personService = PersonService.PersonServiceFactory.getInstance();
        PersonDBO personEmployee = personService.findPersonById(workContractCreationRequest.getEmployee().getPersonId());
        AddressDBO selectedAddress = null;
        for(AddressDBO address : personEmployee.getAddresses()){
            if (address.getDefaultAddress()){
                selectedAddress = address;
            }
        }

        String completeAddress = "";
        if(selectedAddress.getLocation().equals(selectedAddress.getMunicipality())){
            completeAddress = selectedAddress.getStreetType().getStreetTypeDescription() + " " + selectedAddress.getStreetName() + ", " + selectedAddress.getStreetExtended() +
                    "  " + selectedAddress.getPostalCode() + "  " + selectedAddress.getMunicipality();
        }else {
            completeAddress = selectedAddress.getStreetType().getStreetTypeDescription() + " " + selectedAddress.getStreetName() + ", " + selectedAddress.getStreetExtended() +
                    "  " + selectedAddress.getLocation() + "  " + selectedAddress.getPostalCode() + "  " + selectedAddress.getMunicipality();
        }

        String contractDescription = workContractCreationRequest.getContractType().getColloquial() + ", " + workContractCreationRequest.getContractType().getContractDescription();

        AcroFields contractDataSubFolderPDFFields = stamp.getAcroFields();
        contractDataSubFolderPDFFields.setField("notificationType",notificationType);
        contractDataSubFolderPDFFields.setField("officialContractNumber",workContractCreationRequest.getOfficialIdentificationNumber());
        contractDataSubFolderPDFFields.setField("employerFullName",workContractCreationRequest.getEmployer().toAlphabeticalName());
        contractDataSubFolderPDFFields.setField("quoteAccountCode",workContractCreationRequest.getQuoteAccountCode().toCompleteNumber());
        contractDataSubFolderPDFFields.setField("notificationDate",workContractCreationRequest.getClientNotification().format(dateFormatter));
        contractDataSubFolderPDFFields.setField("notificationHour",workContractCreationRequest.getClientNotification().format(timeFormatter));
        contractDataSubFolderPDFFields.setField("employeeFullName",workContractCreationRequest.getEmployee().toAlphabeticalName());
        contractDataSubFolderPDFFields.setField("employeeNIF",workContractCreationRequest.getEmployee().getNieNif().formattedAsNIF());
        contractDataSubFolderPDFFields.setField("employeeNASS",personEmployee.getSocialSecurityAffiliationNumber());
        contractDataSubFolderPDFFields.setField("employeeBirthDate",personEmployee.getBirthDate().toLocalDate().format(dateFormatter));
        contractDataSubFolderPDFFields.setField("employeeCivilStatus",personEmployee.getCivilStatus().getCivilStatusDescription());
        contractDataSubFolderPDFFields.setField("employeeNationality",personEmployee.getNationality());
        contractDataSubFolderPDFFields.setField("employeeFullAddress",completeAddress);
        contractDataSubFolderPDFFields.setField("employeeMaxStudyLevel",personEmployee.getStudy().getStudyLevelDescription());

        if(workContractCreationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.MONDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("monday", "Yes");
        }
        if(workContractCreationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.TUESDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("tuesday", "Yes");
        }
        if(workContractCreationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("wednesday", "Yes");
        }
        if(workContractCreationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.THURSDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("thursday", "Yes");
        }
        if(workContractCreationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.FRIDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("friday", "Yes");
        }
        if(workContractCreationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.SATURDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("saturday", "Yes");
        }
        if(workContractCreationRequest.getContractSchedule().getSchedule().containsKey(DayOfWeek.SUNDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))) {
            contractDataSubFolderPDFFields.setField("sunday", "Yes");
        }
        contractDataSubFolderPDFFields.setField("contractDescription", contractDescription);
        contractDataSubFolderPDFFields.setField("startDate", startDate);
        contractDataSubFolderPDFFields.setField("expectedEndDate", expectedEndDate);

        if(durationDays != null) {
            contractDataSubFolderPDFFields.setField("durationDays", durationDays + " días");
        }else{
            contractDataSubFolderPDFFields.setField("durationDays", ContractConstants.UNDEFINED_DURATION_TEXT);
        }

        contractDataSubFolderPDFFields.setField("workCenter", workContractCreationRequest.getWorkCenter().toString());

        /* Start of the form fill loop*/
        for(ContractDaySchedule workDay : workContractCreationRequest.getContractSchedule().getSchedule().values()){
            if(workDay.getDayOfWeek().equals(DayOfWeek.MONDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))){
                String workDayPDF = "";
                String dateOne = "";
                String A1AMfrom = "";
                String A1AMto = "";
                String A1PMfrom = "";
                String A1PMto = "";
                String durationHours = "";
                if(workDay.getDayOfWeek() != null){
                    workDayPDF = workDay.getDayOfWeek();
                }
                if(workDay.getDate() != null){
                    dateOne = workDay.getDate();
                }
                if(workDay.getAmFrom() != null){
                    A1AMfrom = workDay.getAmFrom();
                }
                if(workDay.getAmTo() != null){
                    A1AMto = workDay.getAmTo();
                }

                if(workDay.getPmFrom() != null){
                    A1PMfrom = workDay.getPmFrom();
                }
                if(workDay.getPmTo() != null){
                    A1PMto = workDay.getPmTo();
                }
                if(workDay.getDurationHours() != null){
                    durationHours = workDay.getDurationHours();
                }
                contractDataSubFolderPDFFields.setField("dayOne", workDayPDF);
                contractDataSubFolderPDFFields.setField("dateOne", dateOne);
                contractDataSubFolderPDFFields.setField("A1AMfrom",A1AMfrom);
                contractDataSubFolderPDFFields.setField("A1AMto", A1AMto);
                contractDataSubFolderPDFFields.setField("A1PMfrom", A1PMfrom);
                contractDataSubFolderPDFFields.setField("A1PMto", A1PMto);
                contractDataSubFolderPDFFields.setField("hoursOne", durationHours);
            }

            if(workDay.getDayOfWeek().equals(DayOfWeek.TUESDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))){
                String workDayPDF = "";
                String dateTwo = "";
                String A2AMfrom = "";
                String A2AMto = "";
                String A2PMfrom = "";
                String A2PMto = "";
                String durationHours = "";
                if(workDay.getDayOfWeek() != null){
                    workDayPDF = workDay.getDayOfWeek();
                }
                if(workDay.getDate() != null){
                    dateTwo = workDay.getDate();
                }
                if(workDay.getAmFrom() != null){
                    A2AMfrom = workDay.getAmFrom();
                }
                if(workDay.getAmTo() != null){
                    A2AMto = workDay.getAmTo();
                }

                if(workDay.getPmFrom() != null){
                    A2PMfrom = workDay.getPmFrom();
                }
                if(workDay.getPmTo() != null){
                    A2PMto = workDay.getPmTo();
                }
                if(workDay.getDurationHours() != null){
                    durationHours = workDay.getDurationHours();
                }
                contractDataSubFolderPDFFields.setField("dayTwo", workDayPDF);
                contractDataSubFolderPDFFields.setField("dateTwo", dateTwo);
                contractDataSubFolderPDFFields.setField("A2AMfrom",A2AMfrom );
                contractDataSubFolderPDFFields.setField("A2AMto", A2AMto);
                contractDataSubFolderPDFFields.setField("A2PMfrom", A2PMfrom);
                contractDataSubFolderPDFFields.setField("A2PMto", A2PMto);
                contractDataSubFolderPDFFields.setField("hoursTwo", durationHours);
            }

            if(workDay.getDayOfWeek().equals(DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))){
                String workDayPDF = "";
                String dateThree = "";
                String A3AMfrom = "";
                String A3AMto = "";
                String A3PMfrom = "";
                String A3PMto = "";
                String durationHours = "";
                if(workDay.getDayOfWeek() != null){
                    workDayPDF = workDay.getDayOfWeek();
                }
                if(workDay.getDate() != null){
                    dateThree = workDay.getDate();
                }
                if(workDay.getAmFrom() != null){
                    A3AMfrom = workDay.getAmFrom();
                }
                if(workDay.getAmTo() != null){
                    A3AMto = workDay.getAmTo();
                }

                if(workDay.getPmFrom() != null){
                    A3PMfrom = workDay.getPmFrom();
                }
                if(workDay.getPmTo() != null){
                    A3PMto = workDay.getPmTo();
                }
                if(workDay.getDurationHours() != null){
                    durationHours = workDay.getDurationHours();
                }
                contractDataSubFolderPDFFields.setField("dayThree", workDayPDF);
                contractDataSubFolderPDFFields.setField("dateThree", dateThree);
                contractDataSubFolderPDFFields.setField("A3AMfrom",A3AMfrom );
                contractDataSubFolderPDFFields.setField("A3AMto", A3AMto);
                contractDataSubFolderPDFFields.setField("A3PMfrom", A3PMfrom);
                contractDataSubFolderPDFFields.setField("A3PMto", A3PMto);
                contractDataSubFolderPDFFields.setField("hoursThree", durationHours);
            }

            if(workDay.getDayOfWeek().equals(DayOfWeek.THURSDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))){
                String workDayPDF = "";
                String dateFour = "";
                String A4AMfrom = "";
                String A4AMto = "";
                String A4PMfrom = "";
                String A4PMto = "";
                String durationHours = "";
                if(workDay.getDayOfWeek() != null){
                    workDayPDF = workDay.getDayOfWeek();
                }
                if(workDay.getDate() != null){
                    dateFour = workDay.getDate();
                }
                if(workDay.getAmFrom() != null){
                    A4AMfrom = workDay.getAmFrom();
                }
                if(workDay.getAmTo() != null){
                    A4AMto = workDay.getAmTo();
                }

                if(workDay.getPmFrom() != null){
                    A4PMfrom = workDay.getPmFrom();
                }
                if(workDay.getPmTo() != null){
                    A4PMto = workDay.getPmTo();
                }
                if(workDay.getDurationHours() != null){
                    durationHours = workDay.getDurationHours();
                }
                contractDataSubFolderPDFFields.setField("dayFour", workDayPDF);
                contractDataSubFolderPDFFields.setField("dateFour", dateFour);
                contractDataSubFolderPDFFields.setField("A4AMfrom",A4AMfrom );
                contractDataSubFolderPDFFields.setField("A4AMto", A4AMto);
                contractDataSubFolderPDFFields.setField("A4PMfrom", A4PMfrom);
                contractDataSubFolderPDFFields.setField("A4PMto", A4PMto);
                contractDataSubFolderPDFFields.setField("hoursFour", durationHours);
            }

            if(workDay.getDayOfWeek().equals(DayOfWeek.FRIDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))){
                String workDayPDF = "";
                String dateFive = "";
                String A5AMfrom = "";
                String A5AMto = "";
                String A5PMfrom = "";
                String A5PMto = "";
                String durationHours = "";
                if(workDay.getDayOfWeek() != null){
                    workDayPDF = workDay.getDayOfWeek();
                }
                if(workDay.getDate() != null){
                    dateFive = workDay.getDate();
                }
                if(workDay.getAmFrom() != null){
                    A5AMfrom = workDay.getAmFrom();
                }
                if(workDay.getAmTo() != null){
                    A5AMto = workDay.getAmTo();
                }

                if(workDay.getPmFrom() != null){
                    A5PMfrom = workDay.getPmFrom();
                }
                if(workDay.getPmTo() != null){
                    A5PMto = workDay.getPmTo();
                }
                if(workDay.getDurationHours() != null){
                    durationHours = workDay.getDurationHours();
                }
                contractDataSubFolderPDFFields.setField("dayFive", workDayPDF);
                contractDataSubFolderPDFFields.setField("dateFive", dateFive);
                contractDataSubFolderPDFFields.setField("A5AMfrom",A5AMfrom );
                contractDataSubFolderPDFFields.setField("A5AMto", A5AMto);
                contractDataSubFolderPDFFields.setField("A5PMfrom", A5PMfrom);
                contractDataSubFolderPDFFields.setField("A5PMto", A5PMto);
                contractDataSubFolderPDFFields.setField("hoursFive", durationHours);
            }

            if(workDay.getDayOfWeek().equals(DayOfWeek.SATURDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))){
                String workDayPDF = "";
                String dateSix = "";
                String A6AMfrom = "";
                String A6AMto = "";
                String A6PMfrom = "";
                String A6PMto = "";
                String durationHours = "";
                if(workDay.getDayOfWeek() != null){
                    workDayPDF = workDay.getDayOfWeek();
                }
                if(workDay.getDate() != null){
                    dateSix = workDay.getDate();
                }
                if(workDay.getAmFrom() != null){
                    A6AMfrom = workDay.getAmFrom();
                }
                if(workDay.getAmTo() != null){
                    A6AMto = workDay.getAmTo();
                }

                if(workDay.getPmFrom() != null){
                    A6PMfrom = workDay.getPmFrom();
                }
                if(workDay.getPmTo() != null){
                    A6PMto = workDay.getPmTo();
                }
                if(workDay.getDurationHours() != null){
                    durationHours = workDay.getDurationHours();
                }
                contractDataSubFolderPDFFields.setField("daySix", workDayPDF);
                contractDataSubFolderPDFFields.setField("dateSix", dateSix);
                contractDataSubFolderPDFFields.setField("A6AMfrom",A6AMfrom );
                contractDataSubFolderPDFFields.setField("A6AMto", A6AMto);
                contractDataSubFolderPDFFields.setField("A6PMfrom", A6PMfrom);
                contractDataSubFolderPDFFields.setField("A6PMto", A6PMto);
                contractDataSubFolderPDFFields.setField("hoursSix", durationHours);
            }

            if(workDay.getDayOfWeek().equals(DayOfWeek.SUNDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()))){
                String workDayPDF = "";
                String dateSeven = "";
                String A7AMfrom = "";
                String A7AMto = "";
                String A7PMfrom = "";
                String A7PMto = "";
                String durationHours = "";
                if(workDay.getDayOfWeek() != null){
                    workDayPDF = workDay.getDayOfWeek();
                }
                if(workDay.getDate() != null){
                    dateSeven = workDay.getDate();
                }
                if(workDay.getAmFrom() != null){
                    A7AMfrom = workDay.getAmFrom();
                }
                if(workDay.getAmTo() != null){
                    A7AMto = workDay.getAmTo();
                }

                if(workDay.getPmFrom() != null){
                    A7PMfrom = workDay.getPmFrom();
                }
                if(workDay.getPmTo() != null){
                    A7PMto = workDay.getPmTo();
                }
                if(workDay.getDurationHours() != null){
                    durationHours = workDay.getDurationHours();
                }
                contractDataSubFolderPDFFields.setField("daySeven", workDayPDF);
                contractDataSubFolderPDFFields.setField("dateSeven", dateSeven);
                contractDataSubFolderPDFFields.setField("A7AMfrom",A7AMfrom );
                contractDataSubFolderPDFFields.setField("A7AMto", A7AMto);
                contractDataSubFolderPDFFields.setField("A7PMfrom", A7PMfrom);
                contractDataSubFolderPDFFields.setField("A7PMto", A7PMto);
                contractDataSubFolderPDFFields.setField("hoursSeven", durationHours);
            }
        }

        contractDataSubFolderPDFFields.setField("additionalData",workContractCreationRequest.getPublicNotes());
        contractDataSubFolderPDFFields.setField("laborCategory",workContractCreationRequest.getLaborCategory());
        contractDataSubFolderPDFFields.setField("contractNumber",workContractCreationRequest.getContractNumber().toString());

        stamp.setFormFlattening(true);
        stamp.close();

        return pathOut;
    }
}
