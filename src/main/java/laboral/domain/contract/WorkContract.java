package laboral.domain.contract;

import laboral.domain.contract_json.ContractSchedule;
import laboral.domain.contract_type.WorkContractType;
import laboral.domain.interface_pattern.Contract;
import laboral.domain.person.Person;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.traceability_contract_documentation.TraceabilityContractDocumentation;
import laboral.domain.variation_type.VariationType;
import laboral.domain.work_center.WorkCenter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

public class WorkContract implements Contract {

    private Integer id;
    private LocalDate dateSign;
    private Person employer;
    private Person employee;
    private WorkContractType workContractType;
    private Integer contractNumber;
    private VariationType variationType;
    private LocalDate startDate;
    private LocalDate expectedEndDate;
    private LocalDate modificationDate;
    private LocalDate endDate;
    private ContractSchedule contractSchedule;
    private String laborCategory;
    private QuoteAccountCode quoteAccountCode;
    private String officialIdentificationNumber;
    private String publicNotes;
    private String privateNotes;
    private LocalDateTime clientNotification;
    private WorkCenter workCenter;
    private Set<WorkContract> contractVariations;
    private Set<TraceabilityContractDocumentation> traceability;

    public WorkContract(Integer id,
                        LocalDate dateSign,
                        Person employer,
                        Person employee,
                        WorkContractType workContractType,
                        Integer contractNumber,
                        VariationType variationType,
                        LocalDate startDate,
                        LocalDate expectedEndDate,
                        LocalDate modificationDate,
                        LocalDate endDate,
                        ContractSchedule contractSchedule,
                        String laborCategory,
                        QuoteAccountCode quoteAccountCode,
                        String officialIdentificationNumber,
                        String publicNotes,
                        String privateNotes,
                        LocalDateTime clientNotification,
                        WorkCenter workCenter,
                        Set<WorkContract> contractVariations,
                        Set<TraceabilityContractDocumentation> traceability) {

        this.id = id;
        this.dateSign = dateSign;
        this.employer = employer;
        this.employee = employee;
        this.workContractType = workContractType;
        this.contractNumber = contractNumber;
        this.variationType = variationType;
        this.startDate = startDate;
        this.expectedEndDate = expectedEndDate;
        this.modificationDate = modificationDate;
        this.endDate = endDate;
        this.contractSchedule = contractSchedule;
        this.laborCategory = laborCategory;
        this.quoteAccountCode = quoteAccountCode;
        this.officialIdentificationNumber = officialIdentificationNumber;
        this.publicNotes = publicNotes;
        this.privateNotes = privateNotes;
        this.clientNotification = clientNotification;
        this.workCenter = workCenter;
        this.contractVariations = contractVariations;
        this.traceability = traceability;
    }

    public WorkContract() {
    }

    @Override
    public LocalDate getDateSign() {
        return dateSign;
    }

    @Override
    public void setDateSign(LocalDate dateSign) {

    }

    @Override
    public LocalDate getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(LocalDate startDate) {

    }

    @Override
    public WorkContractType getContractType() {
        return workContractType;
    }

    @Override
    public void setContractType(WorkContractType workContractType) {

    }

    @Override
    public Set<TraceabilityContractDocumentation> getTraceability() {
        return traceability;
    }

    @Override
    public void setTraceability(Set<TraceabilityContractDocumentation> traceability) {

    }

    public Integer getId() {
        return id;
    }

    public Integer getContractNumber() {
        return contractNumber;
    }

    public Person getEmployer() {
        return employer;
    }

    public Person getEmployee() {
        return employee;
    }

    public WorkContractType getWorkContractType() {
        return workContractType;
    }

    public VariationType getVariationType() {
        return variationType;
    }

    public LocalDate getExpectedEndDate() {
        return expectedEndDate;
    }

    public LocalDate getModificationDate() {
        return modificationDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public ContractSchedule getContractSchedule() {
        return contractSchedule;
    }

    public String getLaborCategory() {
        return laborCategory;
    }

    public QuoteAccountCode getQuoteAccountCode() {
        return quoteAccountCode;
    }

    public String getOfficialIdentificationNumber() {
        return officialIdentificationNumber;
    }

    public String getPublicNotes() {
        return publicNotes;
    }

    public String getPrivateNotes() {
        return privateNotes;
    }

    public LocalDateTime getClientNotification() {
        return clientNotification;
    }

    public WorkCenter getWorkCenter() {
        return workCenter;
    }

    public Set<WorkContract> getContractVariations() {
        return contractVariations;
    }

    public String toString(){

        return getContractNumber().toString();
    }

    public static WorkContractBuilder create() {
        return new WorkContractBuilder();
    }

    public static class WorkContractBuilder {

        private Integer id;
        private LocalDate dateSign;
        private Person employer;
        private Person employee;
        private WorkContractType workContractType;
        private Integer contractNumber;
        private VariationType variationType;
        private LocalDate startDate;
        private LocalDate expectedEndDate;
        private LocalDate modificationDate;
        private LocalDate endDate;
        private ContractSchedule contractSchedule;
        private String laborCategory;
        private QuoteAccountCode quoteAccountCode;
        private String officialIdentificationNumber;
        private String publicNotes;
        private String privateNotes;
        private LocalDateTime clientNotification;
        private WorkCenter workCenter;
        private Set<WorkContract> contractVariations;
        private Set<TraceabilityContractDocumentation> traceability;

        public WorkContractBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public WorkContractBuilder withDateSign(LocalDate dateSign) {
            this.dateSign = dateSign;
            return this;
        }

        public WorkContractBuilder withEmployer(Person employer) {
            this.employer = employer;
            return this;
        }

        public WorkContractBuilder withEmployee(Person employee) {
            this.employee = employee;
            return this;
        }

        public WorkContractBuilder withContractType(WorkContractType workContractType) {
            this.workContractType = workContractType;
            return this;
        }

        public WorkContractBuilder withContractNumber(Integer contractNumber) {
            this.contractNumber = contractNumber;
            return this;
        }

        public WorkContractBuilder withVariationType(VariationType variationType) {
            this.variationType = variationType;
            return this;
        }

        public WorkContractBuilder withStartDate(LocalDate startDate) {
            this.startDate = startDate;
            return this;
        }

        public WorkContractBuilder withExpectedEndDate(LocalDate expectedEndDate) {
            this.expectedEndDate = expectedEndDate;
            return this;
        }

        public WorkContractBuilder withModificationDate(LocalDate modificationDate) {
            this.modificationDate = modificationDate;
            return this;
        }

        public WorkContractBuilder withEndDate(LocalDate endDate) {
            this.endDate = endDate;
            return this;
        }

        public WorkContractBuilder withContractSchedule(ContractSchedule contractSchedule) {
            this.contractSchedule = contractSchedule;
            return this;
        }

        public WorkContractBuilder withLaborCategory(String laborCategory) {
            this.laborCategory = laborCategory;
            return this;
        }

        public WorkContractBuilder withQuoteAccountCode(QuoteAccountCode quoteAccountCode) {
            this.quoteAccountCode = quoteAccountCode;
            return this;
        }

        public WorkContractBuilder withIdentificationContractNumberINEM(String officialIdentificationNumber) {
            this.officialIdentificationNumber = officialIdentificationNumber;
            return this;
        }

        public WorkContractBuilder withPublicNotes(String publicNotes) {
            this.publicNotes = publicNotes;
            return this;
        }

        public WorkContractBuilder withPrivateNotes(String privateNotes) {
            this.privateNotes = privateNotes;
            return this;
        }

        public WorkContractBuilder withClientNotification(LocalDateTime clientNotification) {
            this.clientNotification = clientNotification;
            return this;
        }

        public WorkContractBuilder withWorkCenter(WorkCenter workCenter) {
            this.workCenter = workCenter;
            return this;
        }

        public WorkContractBuilder withContractVariations(Set<WorkContract> contractVariations) {
            this.contractVariations = contractVariations;
            return this;
        }

        public WorkContractBuilder withTraceability(Set<TraceabilityContractDocumentation> traceability) {
            this.traceability = traceability;
            return this;
        }

        public WorkContract build() {
            return new WorkContract(this.id, this.dateSign, this.employer, this.employee, this.workContractType, this.contractNumber, this.variationType, this.startDate,
                    this.expectedEndDate, this.modificationDate, this.endDate, this.contractSchedule, this.laborCategory, this.quoteAccountCode,
                    this.officialIdentificationNumber, this.publicNotes, this.privateNotes, this.clientNotification, this.workCenter, this.contractVariations, this.traceability);
        }
    }
}
