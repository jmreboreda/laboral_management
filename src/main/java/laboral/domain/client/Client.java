package laboral.domain.client;

import laboral.domain.activity_period.ActivityPeriod;
import laboral.domain.address.Address;
import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.interface_pattern.LegalPersonality;
import laboral.domain.interface_pattern.NaturalPersonality;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.Person;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.person_type.PersonType;
import laboral.domain.service_gm.ServiceGM;
import laboral.domain.study.StudyLevelType;
import laboral.domain.work_center.WorkCenter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

public class Client extends Person implements NaturalPersonality, LegalPersonality {

    private Integer clientId;
    private String sg21Code;
    private Set<ActivityPeriod> activityPeriods;
    private Set<ServiceGM> services;
    private Set<WorkCenter> workCenters;
    private PersonDBO person;

    public Client(Integer id,
                  Integer clientId,
                  String sg21Code,
                  PersonType personType,
                  NieNif nieNif,
                  Set<Address> addresses,
                  Set<ActivityPeriod> activityPeriods,
                  Set<ServiceGM> services,
                  Set<WorkCenter> workCenters,
                  PersonDBO person) {
        super(id, personType, nieNif, addresses);
        this.clientId = clientId;
        this.sg21Code = sg21Code;
        this.activityPeriods = activityPeriods;
        this.services = services;
        this.workCenters = workCenters;
        this.person = person;
    }

    public Client(){
        super();
    }

    public Client(Integer id, PersonType personType, NieNif nieNif, Set<Address> addresses) {
        super(id, personType, nieNif, addresses);
    }

    @Override
    public Integer getId() {
        return super.getId();
    }

    @Override
    public void setId(Integer id) {
        super.setId(id);
    }

    @Override
    public PersonType getPersonType() {
        return super.getPersonType();
    }

    @Override
    public void setPersonType(PersonType personType) {
        super.setPersonType(personType);
    }

    @Override
    public NieNif getNieNif() {
        return super.getNieNif();
    }

    @Override
    public void setNieNif(NieNif nieNif) {
        super.setNieNif(nieNif);
    }

    @Override
    public Set<Address> getAddresses() {
        return super.getAddresses();
    }

    @Override
    public void setAddresses(Set<Address> addresses) {
        super.setAddresses(addresses);
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getSg21Code() {
        return sg21Code;
    }

    public void setSg21Code(String sg21Code) {
        this.sg21Code = sg21Code;
    }

    public Set<ActivityPeriod> getActivityPeriods() {
        return activityPeriods;
    }

    public void setActivityPeriods(Set<ActivityPeriod> activityPeriods) {
        this.activityPeriods = activityPeriods;
    }

    public Set<ServiceGM> getServices() {
        return services;
    }

    public void setServices(Set<ServiceGM> services) {
        this.services = services;
    }

    public Set<WorkCenter> getWorkCenters() {
        return workCenters;
    }

    public void setWorkCenters(Set<WorkCenter> workCenters) {
        this.workCenters = workCenters;
    }

    private PersonDBO getPerson() {
        return person;
    }

    public void setPerson(PersonDBO person) {
        this.person = person;
    }

    public List<Address> getAddressesOfWorkCenters(){
        List<Address> addressOfWorkCentersList = new ArrayList<>();
        for(WorkCenter workCenter : getWorkCenters()){
            Address address = workCenter.getAddress();
            addressOfWorkCentersList.add(address);
        }

        Comparator<Address> addressId = Comparator.comparing(Address::getId);
        addressOfWorkCentersList.sort(addressId);

        return addressOfWorkCentersList;
    }

    public String toString(){
        return toAlphabeticalName();
    }

    @Override
    public String getFirstSurName() {
        return getPerson().getFirstSurname();
    }

    @Override
    public String getSecondSurName() {
        return getPerson().getSecondSurname();
    }

    @Override
    public String getName() {
        return getPerson().getName();
    }

    @Override
    public String getSocialSecurityAffiliationNumber() {
        return getSocialSecurityAffiliationNumber();
    }

    @Override
    public LocalDate getBirthDate() {
        return getBirthDate();
    }

    @Override
    public CivilStatusType getCivilStatus() {
        return getCivilStatus();
    }

    @Override
    public StudyLevelType getStudy() {
        return getStudy();
    }

    @Override
    public String getNationality() {
        return getNationality();
    }

    @Override
    public String getLegalName() {
        return getPerson().getLegalName();
    }

    @Override
    public String toAlphabeticalName(){
        return person.toAlphabeticalName();
    }

    @Override
    public String toNaturalName() {
        return person.toNaturalName();
    }
}
