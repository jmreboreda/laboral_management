package laboral.domain.client.mapper;

import laboral.domain.activity_period.ActivityPeriod;
import laboral.domain.activity_period.mapper.MapperActivityPeriodDBOToActivityPeriod;
import laboral.domain.activity_period.persistence.dbo.ActivityPeriodDBO;
import laboral.domain.address.Address;
import laboral.domain.address.mapper.MapperAddressDBOToAddress;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.client.Client;
import laboral.domain.client.persistence.dbo.ClientDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.mapper.MapperPersonDBOToPerson;
import laboral.domain.service_gm.ServiceGM;
import laboral.domain.service_gm.mapper.MapperServiceGMDBOToServiceGM;
import laboral.domain.service_gm.persistence.dbo.ServiceGMDBO;
import laboral.domain.work_center.WorkCenter;
import laboral.domain.work_center.mapper.MapperWorkCenterDBOToWorkCenter;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;

import java.util.HashSet;
import java.util.Set;

public class MapperClientDBOToClient implements GenericMapper<ClientDBO, Client> {

    MapperActivityPeriodDBOToActivityPeriod mapperActivityPeriodDBOToActivityPeriod = new MapperActivityPeriodDBOToActivityPeriod();
    MapperAddressDBOToAddress mapperAddressDBOToAddress = new MapperAddressDBOToAddress();
    MapperServiceGMDBOToServiceGM mapperServiceGMDBOToServiceGM = new MapperServiceGMDBOToServiceGM();
    MapperWorkCenterDBOToWorkCenter mapperWorkCenterDBOToWorkCenter = new MapperWorkCenterDBOToWorkCenter();

    @Override
    public Client map(ClientDBO clientDBO) {

        Set<ActivityPeriod> activityPeriodSet = new HashSet<>();
        for(ActivityPeriodDBO activityPeriodDBO : clientDBO.getActivityPeriods()){
            activityPeriodSet.add(mapperActivityPeriodDBOToActivityPeriod.map(activityPeriodDBO));
        }

        Set<ServiceGM> serviceGMSet = new HashSet<>();
        for(ServiceGMDBO serviceGMDBO : clientDBO.getServices()){
            serviceGMSet.add(mapperServiceGMDBOToServiceGM.map(serviceGMDBO));
        }

        Set<Address> addressSet = new HashSet<>();
        for(AddressDBO addressDBO : clientDBO.getPersonDBO().getAddresses()){
            addressSet.add( mapperAddressDBOToAddress.map(addressDBO));
        }

        Set<WorkCenter> workCenterSet = new HashSet<>();
        for(WorkCenterDBO workCenterDBO : clientDBO.getWorkCenters()){
            workCenterSet.add(mapperWorkCenterDBOToWorkCenter.map(workCenterDBO));
        }

       return new Client(null, clientDBO.getId(),
               clientDBO.getSg21Code(),
               clientDBO.getPersonDBO().getPersonType(),
               new NieNif(clientDBO.getPersonDBO().getNieNif()),
               addressSet,
               activityPeriodSet,
               serviceGMSet,
               workCenterSet, clientDBO.getPersonDBO());
    }
}
