package laboral.domain.client;

import laboral.domain.activity_period.ActivityPeriod;
import laboral.domain.address.Address;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.Person;
import laboral.domain.person_type.PersonType;
import laboral.domain.service_gm.ServiceGM;
import laboral.domain.work_center.WorkCenter;

import java.time.LocalDate;
import java.util.Set;

public class ClientCreationRequest extends Person {

    Integer personId;
    LocalDate dateFrom;
    LocalDate dateTo;
    LocalDate withoutActivity;
    String sg21Code;
    Set<ActivityPeriod> activityPeriods;
    Set<ServiceGM> services;
    Set<WorkCenter> workCenters;

    public ClientCreationRequest(Integer id,
                                Integer personId,
                                PersonType personType,
                                NieNif nieNif,
                                Set<Address> addresses,
                                LocalDate dateFrom,
                                LocalDate dateTo,
                                LocalDate withoutActivity,
                                String sg21Code,
                                Set<ActivityPeriod> activityPeriods,
                                Set<ServiceGM> services,
                                Set<WorkCenter> workCenters) {
        super(id, personType, nieNif, addresses);

        this.personId = personId;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.withoutActivity = withoutActivity;
        this.sg21Code = sg21Code;
        this.activityPeriods = activityPeriods;
        this.services = services;
        this.workCenters = workCenters;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public LocalDate getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDate dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDate getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDate dateTo) {
        this.dateTo = dateTo;
    }

    public LocalDate getWithoutActivity() {
        return withoutActivity;
    }

    public void setWithoutActivity(LocalDate withoutActivity) {
        this.withoutActivity = withoutActivity;
    }

    public String getSg21Code() {
        return sg21Code;
    }

    public void setSg21Code(String sg21Code) {
        this.sg21Code = sg21Code;
    }

    public Set<ActivityPeriod> getActivityPeriods() {
        return activityPeriods;
    }

    public void setActivityPeriods(Set<ActivityPeriod> activityPeriods) {
        this.activityPeriods = activityPeriods;
    }

    public Set<ServiceGM> getServices() {
        return services;
    }

    public void setServices(Set<ServiceGM> services) {
        this.services = services;
    }

    public Set<WorkCenter> getWorkCenters() {
        return workCenters;
    }

    public void setWorkCenters(Set<WorkCenter> workCenters) {
        this.workCenters = workCenters;
    }

    public static ClientCreationRequest.ClientCreationRequestBuilder create() {
        return new ClientCreationRequest.ClientCreationRequestBuilder();
    }

    public static class ClientCreationRequestBuilder {

        private Integer personId;
        private PersonType personType;
        private NieNif nieNif;
        private Set<Address> addresses;
        private LocalDate dateFrom;
        private LocalDate dateTo;
        private LocalDate withoutActivity;
        private String sg21Code;
        private Set<ActivityPeriod> activityPeriods;
        private Set<ServiceGM> services;
        private Set<WorkCenter> workCenters;

        public ClientCreationRequest.ClientCreationRequestBuilder withPersonId(Integer personId) {
            this.personId = personId;
            return this;
        }

        public ClientCreationRequest.ClientCreationRequestBuilder withPersonType(PersonType personType) {
            this.personType = personType;
            return this;
        }

        public ClientCreationRequest.ClientCreationRequestBuilder withNieNif(NieNif nieNif) {
            this.nieNif = nieNif;
            return this;
        }

        public ClientCreationRequest.ClientCreationRequestBuilder withAddresses(Set<Address> addresses) {
            this.addresses = addresses;
            return this;
        }

        public ClientCreationRequest.ClientCreationRequestBuilder withDateFrom(LocalDate dateFrom) {
            this.dateFrom = dateFrom;
            return this;
        }

        public ClientCreationRequest.ClientCreationRequestBuilder withDateTo(LocalDate dateTo) {
            this.dateTo = dateTo;
            return this;
        }

        public ClientCreationRequest.ClientCreationRequestBuilder withWithoutActivity(LocalDate withoutActivity) {
            this.withoutActivity = withoutActivity;
            return this;
        }

        public ClientCreationRequest.ClientCreationRequestBuilder withSg21Code(String sg21Code) {
            this.sg21Code = sg21Code;
            return this;
        }

        public ClientCreationRequest.ClientCreationRequestBuilder withActivityPeriods(Set<ActivityPeriod> activityPeriods) {
            this.activityPeriods = activityPeriods;
            return this;
        }

        public ClientCreationRequest.ClientCreationRequestBuilder withServices(Set<ServiceGM> services) {
            this.services = services;
            return this;
        }

        public ClientCreationRequest.ClientCreationRequestBuilder withWorkCenters(Set<WorkCenter> workCenters) {
            this.workCenters = workCenters;
            return this;
        }

        public ClientCreationRequest build() {
            return new ClientCreationRequest(null, this.personId, this.personType, this.nieNif, this.addresses, this.dateFrom, this.dateTo, this.withoutActivity, this.sg21Code, this.activityPeriods, this.services, this.workCenters);
        }
    }
}
