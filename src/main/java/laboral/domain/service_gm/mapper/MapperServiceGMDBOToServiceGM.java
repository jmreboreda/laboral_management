package laboral.domain.service_gm.mapper;

import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.service_gm.ServiceGM;
import laboral.domain.service_gm.persistence.dbo.ServiceGMDBO;

import java.time.LocalDate;

public class MapperServiceGMDBOToServiceGM implements GenericMapper<ServiceGMDBO, ServiceGM> {

    @Override
    public ServiceGM map(ServiceGMDBO serviceGMDBO) {

        LocalDate dateFrom = serviceGMDBO.getDateFrom() == null ? null : serviceGMDBO.getDateFrom().toLocalDate();
        LocalDate dateTo = serviceGMDBO.getDateTo() == null ? null : serviceGMDBO.getDateTo().toLocalDate();

        ServiceGM serviceGM = new ServiceGM();
        serviceGM.setId(serviceGMDBO.getId());
        serviceGM.setClient(serviceGMDBO.getClientDBO());
        serviceGM.setServiceGM(serviceGMDBO.getService());
        serviceGM.setDateFrom(dateFrom);
        serviceGM.setDateTo(dateTo);
        serviceGM.setClaimInvoices(serviceGMDBO.getClaimInvoices());

        return serviceGM;
    }
}
