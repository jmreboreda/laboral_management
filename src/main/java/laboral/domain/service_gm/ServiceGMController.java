package laboral.domain.service_gm;

import java.time.LocalDate;
import java.util.List;

public class ServiceGMController {

    public ServiceGMController() {
    }

    ServiceGMManager serviceGMManager = new ServiceGMManager();

    public List<ServiceGM> findAllClientGMWithInvoicesToClaimInPeriod(LocalDate periodInitialDate, LocalDate periodFinalDate){

        return serviceGMManager.findAllClientGMWithInvoicesToClaimInPeriod(periodInitialDate, periodFinalDate);
    }

    public List<ServiceGM> findServiceGMByClientId(Integer clientId){
        return serviceGMManager.findServiceGMByClientId(clientId);
    }
}
