package laboral.domain.service_gm;

import laboral.domain.service_gm.mapper.MapperServiceGMDBOToServiceGM;
import laboral.domain.service_gm.persistence.dao.ServiceGMDAO;
import laboral.domain.service_gm.persistence.dbo.ServiceGMDBO;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ServiceGMManager {

    public ServiceGMManager() {
    }

    ServiceGMDAO serviceGMDAO = ServiceGMDAO.ServiceGMDAOFactory.getInstance();

    public List<ServiceGM> findAllClientGMWithInvoicesToClaimInPeriod(LocalDate periodInitialDate, LocalDate periodFinalDate){

        List<ServiceGM> serviceGMList = new ArrayList<>();
        MapperServiceGMDBOToServiceGM mapperServiceGMDBOToServiceGM = new MapperServiceGMDBOToServiceGM();

        List<ServiceGMDBO> serviceGMDBOList = serviceGMDAO.findAllClientGMWithInvoicesToClaimInPeriod(periodInitialDate, periodFinalDate);
        for(ServiceGMDBO serviceGMDBO : serviceGMDBOList){
            ServiceGM serviceGM = mapperServiceGMDBOToServiceGM.map(serviceGMDBO);
            serviceGMList.add(serviceGM);
        }

        return serviceGMList;
    }

    public List<ServiceGM> findServiceGMByClientId(Integer clientId){

        List<ServiceGM> serviceGMList = new ArrayList<>();
        MapperServiceGMDBOToServiceGM  mapperServiceGMDBOToServiceGM = new MapperServiceGMDBOToServiceGM();

        List<ServiceGMDBO> serviceGMDBOList = serviceGMDAO.findServiceGMByClientId(clientId);
        for(ServiceGMDBO serviceGMDBO : serviceGMDBOList){
            ServiceGM serviceGM = mapperServiceGMDBOToServiceGM.map(serviceGMDBO);
            serviceGMList.add(serviceGM);
        }
        return serviceGMList;

    }
}
