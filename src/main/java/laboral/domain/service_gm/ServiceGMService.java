package laboral.domain.service_gm;

import java.time.LocalDate;
import java.util.List;

public class ServiceGMService {

    public ServiceGMService() {
    }

    public static List<ServiceGM> findAllClientGMWithInvoicesToClaimInPeriod(LocalDate periodInitialDate, LocalDate periodFinalDate){

        ServiceGMController serviceGMController = new ServiceGMController();

        return serviceGMController.findAllClientGMWithInvoicesToClaimInPeriod(periodInitialDate, periodFinalDate);
    }
}
