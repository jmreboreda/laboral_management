package laboral.domain.interface_pattern;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import laboral.component.person_management.events.NamePatternChangedEvent;
import laboral.component.person_management.events.PersonToModifyEvent;

import java.util.List;

public interface GenericPersonFinderByName <V>{

    EventHandler<NamePatternChangedEvent> namePatternEventEventHandler = null;
    EventHandler<PersonToModifyEvent> persistedPersonToModifyEventEventHandler = null;

    Parent parent = null;

    @FXML
    void initialize();

    void initializeView();

    TitledPane getHeaderPane();

    void setHeaderPane(TitledPane entity);

    TextField getPersonName();

    void setPersonName(TextField entity);

    ListView<V> getPersonsNames();

    void setPersonsNames(ListView<V> entity);

    void onPersonNameChanged(Event event);

    void onSelectedPersonToModification(MouseEvent event);

    void refreshPersonNames(List<V> entity);

    void setOnNamePatternChanged(EventHandler<NamePatternChangedEvent> entity);

    void setOnSelectPersonToModification(EventHandler<PersonToModifyEvent> entity);
}
