package laboral.domain.interface_pattern;

import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.nienif.NieNif;
import laboral.domain.study.StudyLevelType;

import java.time.LocalDate;

public interface NaturalPersonality {

    String getFirstSurName();
    String getSecondSurName();
    String getName();
    String getSocialSecurityAffiliationNumber();
    LocalDate getBirthDate();
    CivilStatusType getCivilStatus();
    StudyLevelType getStudy();
    String getNationality();

    NieNif getNieNif();

    String toAlphabeticalName();

    String toNaturalName();

}
