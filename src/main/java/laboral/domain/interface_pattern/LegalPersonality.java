package laboral.domain.interface_pattern;

import laboral.domain.nienif.NieNif;

public interface LegalPersonality {

    String getLegalName();

    String toAlphabeticalName();

    NieNif getNieNif();
}
