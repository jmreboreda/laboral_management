package laboral.domain.interface_pattern;

public interface GenericMapper<T, V> {

    V map(T entity);
}
