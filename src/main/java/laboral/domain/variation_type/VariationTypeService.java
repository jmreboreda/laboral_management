package laboral.domain.variation_type;

import laboral.domain.variation_type.persistence.dao.VariationTypeDAO;
import laboral.domain.variation_type.persistence.dbo.VariationTypeDBO;

public class VariationTypeService {

    VariationTypeDAO variationTypeDAO = VariationTypeDAO.VariationTypeDAOFactory.getInstance();

    public VariationTypeDBO findByVariationTypeCode(Integer variationTypeCode){

        return variationTypeDAO.findByVariationTypeCode(variationTypeCode);
    }
}
