package laboral.domain.variation_type;

public class VariationType {

    private Integer id;
    private Integer variationCode;
    private String variationDescription;
    private Boolean extinction;
    private Boolean conversion;
    private Boolean special;
    private Boolean specialInitial;
    private Boolean specialFinal;
    private Boolean extension;
    private Boolean category;
    private Boolean initial;
    private Boolean reincorporation;
    private Boolean workingDay;
    private Boolean idcRequired;

    public VariationType() {
    }

    public VariationType(Integer id,
                         Integer variationCode,
                         String variationDescription,
                         Boolean extinction,
                         Boolean conversion,
                         Boolean special,
                         Boolean specialInitial,
                         Boolean specialFinal,
                         Boolean extension,
                         Boolean category,
                         Boolean initial,
                         Boolean reincorporation,
                         Boolean workingDay,
                         Boolean idcRequired) {
        this.id = id;
        this.variationCode = variationCode;
        this.variationDescription = variationDescription;
        this.extinction = extinction;
        this.conversion = conversion;
        this.special = special;
        this.specialInitial = specialInitial;
        this.specialFinal = specialFinal;
        this.extension = extension;
        this.category = category;
        this.initial = initial;
        this.reincorporation = reincorporation;
        this.workingDay= workingDay;
        this.idcRequired = idcRequired;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVariationCode() {
        return variationCode;
    }

    public void setVariationCode(Integer variationCode) {
        this.variationCode = variationCode;
    }

    public String getVariationDescription() {
        return variationDescription;
    }

    public void setVariationDescription(String variationDescription) {
        this.variationDescription = variationDescription;
    }

    public Boolean getExtinction() {
        return extinction;
    }

    public void setExtinction(Boolean extinction) {
        this.extinction = extinction;
    }

    public Boolean getConversion() {
        return conversion;
    }

    public void setConversion(Boolean conversion) {
        this.conversion = conversion;
    }

    public Boolean getSpecial() {
        return special;
    }

    public void setSpecial(Boolean special) {
        this.special = special;
    }

    public Boolean getSpecialInitial() {
        return specialInitial;
    }

    public void setSpecialInitial(Boolean specialInitial) {
        this.specialInitial = specialInitial;
    }

    public Boolean getSpecialFinal() {
        return specialFinal;
    }

    public void setSpecialFinal(Boolean specialFinal) {
        this.specialFinal = specialFinal;
    }

    public Boolean getExtension() {
        return extension;
    }

    public void setExtension(Boolean extension) {
        this.extension = extension;
    }

    public Boolean getCategory() {
        return category;
    }

    public void setCategory(Boolean category) {
        this.category = category;
    }

    public Boolean getInitial() {
        return initial;
    }

    public void setInitial(Boolean initial) {
        this.initial = initial;
    }

    public Boolean getReincorporation() {
        return reincorporation;
    }

    public void setReincorporation(Boolean reincorporation) {
        this.reincorporation = reincorporation;
    }

    public Boolean getWorkingDay() {
        return workingDay;
    }

    public void setWorkingDay(Boolean workingDay) {
        this.workingDay = workingDay;
    }

    public Boolean getIdcRequired() {
        return idcRequired;
    }

    public void setIdcRequired(Boolean idcRequired) {
        this.idcRequired = idcRequired;
    }

    public String toString(){
        return this.variationDescription;
    }

    public static VariationType.VariationTypeBuilder create() {
        return new VariationType.VariationTypeBuilder();
    }

    public static class VariationTypeBuilder {

        private Integer id;
        private Integer variationCode;
        private String variationDescription;
        private Boolean extinction;
        private Boolean conversion;
        private Boolean special;
        private Boolean specialInitial;
        private Boolean specialFinal;
        private Boolean extension;
        private Boolean category;
        private Boolean initial;
        private Boolean reincorporation;
        private Boolean workingDay;
        private Boolean idcRequired;

        public VariationType.VariationTypeBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public VariationType.VariationTypeBuilder withVariationCode(Integer variationCode) {
            this.variationCode = variationCode;
            return this;
        }

        public VariationType.VariationTypeBuilder withVariationDescription(String variationDescription) {
            this.variationDescription = variationDescription;
            return this;
        }

        public VariationType.VariationTypeBuilder withExtinction(Boolean extinction) {
            this.extinction = extinction;
            return this;
        }

        public VariationType.VariationTypeBuilder withConversion(Boolean conversion) {
            this.conversion = conversion;
            return this;
        }

        public VariationType.VariationTypeBuilder withSpecial(Boolean special) {
            this.special = special;
            return this;
        }

        public VariationType.VariationTypeBuilder withSpecialInitial(Boolean specialInitial) {
            this.specialInitial = specialInitial;
            return this;
        }

        public VariationType.VariationTypeBuilder withSpecialFinal(Boolean specialFinal) {
            this.specialFinal = specialFinal;
            return this;
        }

        public VariationType.VariationTypeBuilder withExtension(Boolean extension) {
            this.extension = extension;
            return this;
        }

        public VariationType.VariationTypeBuilder withCategory(Boolean category) {
            this.category = category;
            return this;
        }

        public VariationType.VariationTypeBuilder withInitial(Boolean initial) {
            this.initial = initial;
            return this;
        }

        public VariationType.VariationTypeBuilder withReincorporation(Boolean reincorporation) {
            this.reincorporation = reincorporation;
            return this;
        }

        public VariationType.VariationTypeBuilder withWorkingDay(Boolean workingDay) {
            this.workingDay = workingDay;
            return this;
        }

        public VariationType.VariationTypeBuilder withIdcRequired(Boolean idcRequired) {
            this.idcRequired = idcRequired;
            return this;
        }

        public VariationType build() {
            return new VariationType(this.id, this.variationCode, this.variationDescription, this.extinction, this.conversion, this.special, this.specialInitial, this.specialFinal,
                    this.extension, this.category, this.initial, this.reincorporation, this.workingDay, this.idcRequired);
        }
    }
}
