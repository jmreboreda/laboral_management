package laboral.domain.variation_type.persistence.dbo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "variation_type")
@NamedQueries(value = {
        @NamedQuery(
                name = VariationTypeDBO.FIND_ALL,
                query = "select p from VariationTypeDBO p order by id"
        ),
        @NamedQuery(
                name = VariationTypeDBO.FIND_ALL_VARIATION_TYPES_SELECTABLES,
                query = "select p from VariationTypeDBO p where isMenuSelectable = true and isInitialContract = true order by variationDescription"
        ),
        @NamedQuery(
                name = VariationTypeDBO.FIND_VARIATION_TYPE_BY_ID,
                query = "select p from VariationTypeDBO p where p.id = :variationTypeId"
        ),
        @NamedQuery(
                name = VariationTypeDBO.FIND_VARIATION_TYPE_BY_VARIATION_CODE,
                query = "select p from VariationTypeDBO p where p.variationCode = :variationCode"
        )
})

public class VariationTypeDBO implements Serializable{

    public static final String FIND_ALL_VARIATION_TYPES_SELECTABLES = "VariationTypeDBO.FIND_ALL_CONTRACT_TYPES_SELECTABLES";
    public static final String FIND_VARIATION_TYPE_BY_ID = "VariationTypeDBO.FIND_VARIATION_TYPE_BY_ID";
    public static final String FIND_VARIATION_TYPE_BY_VARIATION_CODE = "VariationTypeDBO.FIND_VARIATION_TYPE_BY_VARIATION_CODE";
    public static final String FIND_ALL = "VariationTypeDBO.FIND_ALL";

    @Id
    @SequenceGenerator(name = "variation_type_id_seq", sequenceName = "variation_type_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "variation_type_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;
    private Integer variationCode;
    private String variationDescription;
    private Boolean extinction;
    private Boolean conversion;
    private Boolean special;
    private Boolean specialInitial;
    private Boolean specialFinal;
    private Boolean extension;
    private Boolean category;
    private Boolean initial;
    private Boolean reincorporation;
    private Boolean workingDay;
    private Boolean idcRequired;

    public VariationTypeDBO() {
    }

    public VariationTypeDBO(Integer id,
                            Integer variationCode,
                            String variationDescription,
                            Boolean extinction,
                            Boolean conversion,
                            Boolean special,
                            Boolean specialInitial,
                            Boolean specialFinal,
                            Boolean extension,
                            Boolean category,
                            Boolean initial,
                            Boolean reincorporation,
                            Boolean workingDay,
                            Boolean idcRequired) {
        this.id = id;
        this.variationCode = variationCode;
        this.variationDescription = variationDescription;
        this.extinction = extinction;
        this.conversion = conversion;
        this.special = special;
        this.specialInitial = specialInitial;
        this.specialFinal = specialFinal;
        this.extension = extension;
        this.category = category;
        this.initial = initial;
        this.reincorporation = reincorporation;
        this.workingDay = workingDay;
        this.idcRequired = idcRequired;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVariationCode() {
        return variationCode;
    }

    public void setVariationCode(Integer variationCode) {
        this.variationCode = variationCode;
    }

    public String getVariationDescription() {
        return variationDescription;
    }

    public void setVariationDescription(String variationDescription) {
        this.variationDescription = variationDescription;
    }

    public Boolean getExtinction() {
        return extinction;
    }

    public void setExtinction(Boolean extinction) {
        this.extinction = extinction;
    }

    public Boolean getConversion() {
        return conversion;
    }

    public void setConversion(Boolean conversion) {
        this.conversion = conversion;
    }

    public Boolean getSpecial() {
        return special;
    }

    public void setSpecial(Boolean special) {
        this.special = special;
    }

    public Boolean getSpecialInitial() {
        return specialInitial;
    }

    public void setSpecialInitial(Boolean specialInitial) {
        this.specialInitial = specialInitial;
    }

    public Boolean getSpecialFinal() {
        return specialFinal;
    }

    public void setSpecialFinal(Boolean specialFinal) {
        this.specialFinal = specialFinal;
    }

    public Boolean getExtension() {
        return extension;
    }

    public void setExtension(Boolean extension) {
        this.extension = extension;
    }

    public Boolean getCategory() {
        return category;
    }

    public void setCategory(Boolean category) {
        this.category = category;
    }

    public Boolean getInitial() {
        return initial;
    }

    public void setInitial(Boolean initial) {
        this.initial = initial;
    }

    public Boolean getReincorporation() {
        return reincorporation;
    }

    public void setReincorporation(Boolean reincorporation) {
        this.reincorporation = reincorporation;
    }

    public Boolean getWorkingDay() {
        return workingDay;
    }

    public void setWorkingDay(Boolean workingDay) {
        this.workingDay = workingDay;
    }

    public Boolean getIdcRequired() {
        return idcRequired;
    }

    public void setIdcRequired(Boolean idcRequired) {
        this.idcRequired = idcRequired;
    }
}
