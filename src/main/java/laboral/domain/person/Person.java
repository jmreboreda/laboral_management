package laboral.domain.person;

import laboral.domain.address.Address;
import laboral.domain.nienif.NieNif;
import laboral.domain.person_type.PersonType;

import java.util.Set;

public abstract class Person {

    private Integer id;
    private PersonType personType;
    private NieNif nieNif;
    private Set<Address> addresses;

    public Person(Integer id, PersonType personType, NieNif nieNif, Set<Address> addresses) {
        this.id = id;
        this.personType = personType;
        this.nieNif = nieNif;
        this.addresses = addresses;
    }

    public Person() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PersonType getPersonType() {
        return personType;
    }

    public void setPersonType(PersonType personType) {
        this.personType = personType;
    }

    public NieNif getNieNif() {
        return nieNif;
    }

    public void setNieNif(NieNif nieNif) {
        this.nieNif = nieNif;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    public String toString(){
            return personType.getPersonTypeDescription();
    }
}
