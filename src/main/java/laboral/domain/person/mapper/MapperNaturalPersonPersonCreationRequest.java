package laboral.domain.person.mapper;

import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.person.NaturalPerson;
import laboral.domain.person.PersonCreationRequest;

public class MapperNaturalPersonPersonCreationRequest implements GenericMapper<NaturalPerson, PersonCreationRequest> {

    @Override
    public PersonCreationRequest map(NaturalPerson naturalPerson) {

        return PersonCreationRequest.PersonCreationRequestBuilder.create()
                .withPersonType(naturalPerson.getPersonType())
                .withFirstSurname(naturalPerson.getFirstSurName())
                .withSecondSurname(naturalPerson.getSecondSurName())
                .withName(naturalPerson.getName())
                .withNieNif(naturalPerson.getNieNif())
                .withBirthDate(naturalPerson.getBirthDate())
                .withSocialSecurityAffiliationNumber(naturalPerson.getSocialSecurityAffiliationNumber())
                .withStudy(naturalPerson.getStudy())
                .withNationality(naturalPerson.getNationality())
                .withCivilStatus(naturalPerson.getCivilStatus())
                .withAddresses(naturalPerson.getAddresses())
                .build();
    }
}

