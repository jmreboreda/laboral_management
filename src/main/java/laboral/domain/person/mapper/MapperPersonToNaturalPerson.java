package laboral.domain.person.mapper;

import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.person.NaturalPerson;
import laboral.domain.person.Person;

public class MapperPersonToNaturalPerson implements GenericMapper<Person, NaturalPerson> {
    @Override
    public NaturalPerson map(Person person) {
        NaturalPerson naturalPerson = (NaturalPerson) person;

        return NaturalPerson.create()
                .withId(naturalPerson.getId())
                .withFirstSurname(naturalPerson.getFirstSurName())
                .withSecondSurname(naturalPerson.getSecondSurName())
                .withName(naturalPerson.getName())
                .withNieNif(naturalPerson.getNieNif())
                .withSocialSecurityAffiliationNumber(naturalPerson.getSocialSecurityAffiliationNumber())
                .withBirthDate(naturalPerson.getBirthDate())
                .withCivilStatus(naturalPerson.getCivilStatus())
                .withNationality(naturalPerson.getNationality())
                .withStudy(naturalPerson.getStudy())
                .withAddresses(naturalPerson.getAddresses())
                .build();
    }
}
