package laboral.domain.person.controller;

import laboral.domain.person.Person;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person.manager.PersonManager;
import laboral.domain.person.mapper.MapperPersonDBOToPerson;

import java.util.List;

public class PersonController {

    private PersonManager personManager = new PersonManager();

    public PersonController() {
    }

    public Integer personCreator(PersonCreationRequest personCreationRequest){

        return personManager.personCreate(personCreationRequest);
    }

    public Integer personUpdater(Person persistedPerson){

        return personManager.personUpdate(persistedPerson);
    }

    public Person findPersonById(Integer id){

        MapperPersonDBOToPerson mapperPersonDBOToPerson = new MapperPersonDBOToPerson();

        return mapperPersonDBOToPerson.map(personManager.findPersonById(id));

    }

    public List<Person> findPersonByNieNif(String nieNif){

        return personManager.findPersonByNieNif(nieNif);

    }

    public List<Person> findPersonByPattern(String pattern){

        return personManager.findPersonByPattern(pattern);
    }

    public List<Person> findPersonBySSAN(String socialSecurityAffiliationNumber){

        return personManager.findPersonBySSAN(socialSecurityAffiliationNumber);
    }

    public List<Person> findAllNaturalPerson(){

        return personManager.findAllNaturalPerson();
    }
}
