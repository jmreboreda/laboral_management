package laboral.domain.person;

import laboral.domain.person.controller.PersonController;
import laboral.domain.person.manager.PersonManager;
import laboral.domain.person.persistence.dbo.PersonDBO;

import java.util.List;

public class PersonService {

    private PersonService() {
    }

    public static class PersonServiceFactory {

        private static PersonService personService;

        public static PersonService getInstance() {
            if(personService == null) {
                personService = new PersonService();
            }
            return personService;
        }
    }

    private final PersonManager personManager = new PersonManager();

    public PersonDBO findPersonById(Integer id){

        return personManager.findPersonById(id);
    }
}
