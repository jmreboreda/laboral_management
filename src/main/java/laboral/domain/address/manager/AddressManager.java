package laboral.domain.address.manager;

import laboral.domain.address.Address;
import laboral.domain.address.mapper.MapperAddressDBOToAddress;
import laboral.domain.address.mapper.MapperAddressToAddressDBO;
import laboral.domain.address.persistence.dao.AddressDAO;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.person.manager.PersonManager;
import laboral.domain.person.persistence.dbo.PersonDBO;

public class AddressManager {

    AddressDAO addressDAO = AddressDAO.AddressDAOFactory.getInstance();

    public Integer createAddress(Address address, Integer personId){

        MapperAddressToAddressDBO mapperAddressToAddressDBO = new MapperAddressToAddressDBO();

        PersonManager personManager = new PersonManager();
        PersonDBO personDBO = personManager.findPersonById(personId);

        AddressDBO addressDBO = mapperAddressToAddressDBO.map(address);
        addressDBO.setPersonDBO(personDBO);

        return addressDAO.create(addressDBO);
    }

    public Boolean deleteAddress(Address address){
        AddressDBO addressDBO = addressDAO.getSession().load(AddressDBO.class, address.getId());

        return addressDAO.delete(addressDBO);
    }

    public Address findAddressById(Integer id){
        MapperAddressDBOToAddress mapperAddressDBOToAddress = new MapperAddressDBOToAddress();
        return mapperAddressDBOToAddress.map(addressDAO.findById(id));
    }
}
