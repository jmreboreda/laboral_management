package laboral.domain.address;

public class Address {

    private Integer id;
    private StreetType streetType;
    private String streetName;
    private String streetExtended;
    private String location;
    private String municipality;
    private Province province;
    private String postalCode;
    private Boolean defaultAddress;

    public Address() {
    }

    public Address(Integer id, StreetType streetType, String streetName, String streetExtended, String municipality, String location, Province province, String postalCode, Boolean defaultAddress) {
        this.id = id;
        this.streetType = streetType;
        this.streetName = streetName;
        this.streetExtended = streetExtended;
        this.location = location;
        this.municipality = municipality;
        this.province = province;
        this.postalCode = postalCode;
        this.defaultAddress = defaultAddress;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StreetType getStreetType() {
        return streetType;
    }

    public void setStreetType(StreetType streetType) {
        this.streetType = streetType;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetExtended() {
        return streetExtended;
    }

    public void setStreetExtended(String streetExtended) {
        this.streetExtended = streetExtended;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Province getProvince() {
        return province;
    }

    public void setProvince(Province province) {
        this.province = province;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Boolean getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(Boolean defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public String toString(){
        return getStreetType().getStreetTypeDescription().concat(" ").concat(getStreetName()).concat(" ").concat(getStreetExtended()
                .concat("\n\t").concat(getPostalCode()).concat(" ").concat(getLocation())
                .concat("  ").concat(getProvince().getName()).concat(" defaultAddress: ").concat(getDefaultAddress().toString()));
    }

    public String formattedAddress(){
        String space = " ";

        if(getStreetType() == null || getStreetType() == StreetType.UNKN){

            return "";
        }

        return getStreetType().getStreetTypeDescription().concat(" ").concat(getStreetName()).concat(", ").concat(getStreetExtended()
                .concat(space.repeat(4)).concat(getPostalCode()).concat(space.repeat(4)).concat(getLocation())
                .concat(space.repeat(4)).concat(getProvince().getName()));
    }


    public static final class AddressBuilder {
        private Integer id;
        private StreetType streetType;
        private String streetName;
        private String streetExtended;
        private String location;
        private String municipality;
        private Province province;
        private String postalCode;
        private Boolean defaultAddress;

        private AddressBuilder() {
        }

        public static AddressBuilder create() {
            return new AddressBuilder();
        }

        public AddressBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public AddressBuilder withStreetType(StreetType streetType) {
            this.streetType = streetType;
            return this;
        }

        public AddressBuilder withStreetName(String streetName) {
            this.streetName = streetName;
            return this;
        }

        public AddressBuilder withStreetExtended(String streetExtended) {
            this.streetExtended = streetExtended;
            return this;
        }

        public AddressBuilder withLocation(String location) {
            this.location = location;
            return this;
        }

        public AddressBuilder withMunicipality(String municipality) {
            this.municipality = municipality;
            return this;
        }

        public AddressBuilder withProvince(Province province) {
            this.province = province;
            return this;
        }

        public AddressBuilder withPostalCode(String postalCode) {
            this.postalCode = postalCode;
            return this;
        }

        public AddressBuilder withDefaultAddress(Boolean defaultAddress) {
            this.defaultAddress = defaultAddress;
            return this;
        }

        public Address build() {
            Address address = new Address();
            address.setId(id);
            address.setStreetType(streetType);
            address.setStreetName(streetName);
            address.setStreetExtended(streetExtended);
            address.setLocation(location);
            address.setMunicipality(municipality);
            address.setProvince(province);
            address.setPostalCode(postalCode);
            address.setDefaultAddress(defaultAddress);
            return address;
        }
    }
}
