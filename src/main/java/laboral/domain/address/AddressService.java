package laboral.domain.address;

import laboral.domain.address.controller.AddressController;
import laboral.domain.address.persistence.dao.AddressDAO;
import laboral.domain.address.persistence.dbo.AddressDBO;

public class AddressService {

    private AddressService() {
    }

    public static class AddressServiceFactory {

        private static AddressService addressService;

        public static AddressService getInstance() {
            if(addressService == null) {
                addressService = new AddressService();
            }
            return addressService;
        }
    }

    private final AddressController addressController = new AddressController();
    private final AddressDAO addressDAO = AddressDAO.AddressDAOFactory.getInstance();

    public Address findAddressById(Integer clientId){

        return addressController.findAddressById(clientId);
    }

    public AddressDBO findAddressDBOById(Integer id){

        return addressDAO.findById(id);
    }
}
