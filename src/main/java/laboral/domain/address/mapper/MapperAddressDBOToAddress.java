package laboral.domain.address.mapper;

import laboral.domain.address.Address;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.interface_pattern.GenericMapper;

public class MapperAddressDBOToAddress implements GenericMapper<AddressDBO, Address> {

    @Override
    public Address map(AddressDBO addressDBO) {

            Address address = new Address();

            address.setId(addressDBO.getId());
            address.setDefaultAddress(addressDBO.getDefaultAddress());
            address.setStreetType(addressDBO.getStreetType());
            address.setStreetName(addressDBO.getStreetName());
            address.setStreetExtended(addressDBO.getStreetExtended());
            address.setLocation(addressDBO.getLocation());
            address.setMunicipality(addressDBO.getMunicipality());
            address.setProvince(addressDBO.getProvince());
            address.setPostalCode(addressDBO.getPostalCode());

        return address;
    }
}
