package laboral.domain.address.mapper;

import laboral.domain.address.Address;
import laboral.domain.address.persistence.dbo.AddressDBO;
import laboral.domain.interface_pattern.GenericMapper;

public class MapperAddressToAddressDBO implements GenericMapper<Address, AddressDBO> {

    @Override
    public AddressDBO map(Address address) {

        return AddressDBO.AddressDBOBuilder.create()
                .withId(address.getId())
                .withMunicipality(address.getMunicipality())
                .withLocation(address.getLocation())
                .withPostalCode(address.getPostalCode())
                .withDefaultAddress(address.getDefaultAddress())
                .withProvince(address.getProvince())
                .withStreetName(address.getStreetName())
                .withStreetExtended(address.getStreetExtended())
                .withStreetType(address.getStreetType())
                .build();
    }
}
