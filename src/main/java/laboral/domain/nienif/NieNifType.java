package laboral.domain.nienif;

public enum NieNifType {

    DOCUMENT_ERROR("Es un NIE_NIF erróneo."),
    DOCUMENT_NIF_DNI("Es un DNI_NIF."),
    DOCUMENT_NIF_NIE("Es un NIE."),
    DOCUMENT_NIF_CIF("Es un NIF de persona jurídica"),
    DOCUMENT_NIF_EWLP("Es una entidad sin personalidad jurídica."),
    DOCUMENT_OTHER("Es otro tipo de documento");


    private String documentType;

    NieNifType(String documentType) {
        this.documentType = documentType;
    }
}
