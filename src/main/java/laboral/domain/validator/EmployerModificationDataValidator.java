package laboral.domain.validator;

import javafx.stage.Stage;
import laboral.component.employer_management.EmployerManagementConstants;
import laboral.component.employer_management.employer_modification.controller.EmployerModificationMainController;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import java.util.List;
import java.util.regex.Pattern;

public class EmployerModificationDataValidator {

    EmployerModificationMainController employerModificationMainController;

    public EmployerModificationDataValidator(EmployerModificationMainController employerModificationMainController) {
        this.employerModificationMainController = employerModificationMainController;
    }

    public Boolean validateQuoteAccountCodeDataEntry(){
        List<QuoteAccountCode> quoteAccountCodeList = employerModificationMainController.getEmployerQuoteAccountCodeTableView().getQuoteAccountCodeTableView().getItems();
        if(!validateNoMissingQuoteAccountCodeData(quoteAccountCodeList) ||
        !validateQuoteAccountCode(quoteAccountCodeList)){

            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

    private Boolean validateNoMissingQuoteAccountCodeData(List<QuoteAccountCode> quoteAccountCodeList) {

        Pattern provinceCodePattern = Pattern.compile("\\d{2}");
        Pattern numberPattern = Pattern.compile("\\d{7}");
        Pattern controlDigitPattern = Pattern.compile("\\d{2}");
        Pattern activityEpigraphPattern = Pattern.compile("^[0-9]{3}([.][0-9]{1,2})?$");
        Pattern textPattern = Pattern.compile("^[a-záéíóúA-ZÁÉÍÓÚÑñ0-9\\s/_.,-]*$");


        for (QuoteAccountCode quoteAccountCode : quoteAccountCodeList) {
            if (!provinceCodePattern.matcher(quoteAccountCode.getProvince()).matches()){
                String message = "Error en C. C. C. " + quoteAccountCode.getProvince() + "-" + quoteAccountCode.getQuoteAccountCodeNumber() + quoteAccountCode.getControlDigit() + "\nNo hay dos números en la provincia.";
                Message.errorMessage((Stage) this.employerModificationMainController.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, message);
                return Boolean.FALSE;
            }

            if(!numberPattern.matcher(quoteAccountCode.getQuoteAccountCodeNumber()).matches()){
                String message = "Error en C. C. C. " + quoteAccountCode.getProvince() + "-" + quoteAccountCode.getQuoteAccountCodeNumber() + quoteAccountCode.getControlDigit() + "\nNo hay siete números en el número del C. C. C..";
                Message.errorMessage((Stage) this.employerModificationMainController.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, message);
                return Boolean.FALSE;
            }

            if(!controlDigitPattern.matcher(quoteAccountCode.getControlDigit()).matches()){
                String message = "Error en C. C. C. " + quoteAccountCode.getProvince() + "-" + quoteAccountCode.getQuoteAccountCodeNumber() + quoteAccountCode.getControlDigit() + "\nNo hay dos números en el D. C.";
                Message.errorMessage((Stage) this.employerModificationMainController.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, message);
                return Boolean.FALSE;
            }

            if( !activityEpigraphPattern.matcher(quoteAccountCode.getActivityEpigraphNumber()).matches()){
                String message = "Error en C. C. C. " + quoteAccountCode.getProvince() + "-" + quoteAccountCode.getQuoteAccountCodeNumber() + quoteAccountCode.getControlDigit() + "\nEpígrafe del IAE mal construido.";
                Message.errorMessage((Stage) this.employerModificationMainController.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, message);
                return Boolean.FALSE;
            }

            if(!textPattern.matcher(quoteAccountCode.getActivityDescription()).matches()){
                String message = "Error en C. C. C. " + quoteAccountCode.getProvince() + "-" + quoteAccountCode.getQuoteAccountCodeNumber() + quoteAccountCode.getControlDigit() + "\nCaracteres extraños en la actividad del epígrafe.";
                Message.errorMessage((Stage) this.employerModificationMainController.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, message);
                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    private Boolean validateQuoteAccountCode(List<QuoteAccountCode> quoteAccountCodeList){

        for(QuoteAccountCode quoteAccountCode : quoteAccountCodeList){
            if(Integer.parseInt(quoteAccountCode.getProvince()) < 1 ||
                    (Integer.parseInt(quoteAccountCode.getProvince()) > 53 && Integer.parseInt(quoteAccountCode.getProvince()) != 66 )){
                String message = "Error en C. C. C. " + quoteAccountCode.getProvince() + "-" + quoteAccountCode.getQuoteAccountCodeNumber() + quoteAccountCode.getControlDigit() + "\nCódigo de provincia erróneo.";
                Message.errorMessage((Stage) this.employerModificationMainController.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, message);
                return Boolean.FALSE;
            }

            if(quoteAccountCode.getQuoteAccountCodeNumber().length() != 7){
                String message = "Error en C. C. C. " + quoteAccountCode.getProvince() + "-" + quoteAccountCode.getQuoteAccountCodeNumber() + quoteAccountCode.getControlDigit() + "\nNúmero del C. C. C. erróneo.";
                Message.errorMessage((Stage) this.employerModificationMainController.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, message);
                return Boolean.FALSE;

            }

            String qacWithoutControlDigit = quoteAccountCode.getProvince().concat(quoteAccountCode.getQuoteAccountCodeNumber());
            String controlDigit = quoteAccountCode.getControlDigit();
            Integer calculatedControlDigitNumber = (Integer.parseInt(qacWithoutControlDigit)%97);
            if(!calculatedControlDigitNumber.equals(Integer.parseInt(controlDigit))){
                String message = "Error en C. C. C. " + quoteAccountCode.getProvince() + "-" + quoteAccountCode.getQuoteAccountCodeNumber() + quoteAccountCode.getControlDigit() + "\nNúmero del D. C. erróneo.";
                Message.errorMessage((Stage) this.employerModificationMainController.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, message);
                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }
}
