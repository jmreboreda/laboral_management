package laboral.domain.utilities.utilities;

public class UtilitiesErrors {

    /** Date and time errors */
    public static final String FORMAT_TEXT_TO_CONVERT_INCORRECT = "El formato del texto a convertir es incorrecto.";
    public static final String TEXT_TO_CONVERT_IS_NULL = "El texto a convertir es nulo.";


}
