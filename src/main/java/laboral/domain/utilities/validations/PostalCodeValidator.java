package laboral.domain.utilities.validations;

import java.util.regex.Pattern;

public class PostalCodeValidator {

    public Boolean validatePostalCode(String postalCode) {

        final Pattern postalCodePattern = Pattern.compile("\\d{5}");

        if (!postalCodePattern.matcher(postalCode).matches() ||
                Integer.parseInt(postalCode) > 52999 ||
                Integer.parseInt(postalCode) < 1000){

            return false;
        }

        return true;
    }
}
