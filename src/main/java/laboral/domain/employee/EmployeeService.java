package laboral.domain.employee;

import laboral.domain.employee.persistence.dao.EmployeeDAO;
import laboral.domain.employee.persistence.dbo.EmployeeDBO;
import laboral.domain.employer.persistence.dao.EmployerDAO;
import laboral.domain.employer.persistence.dbo.EmployerDBO;

public class EmployeeService {

    EmployeeDAO employeeDAO = EmployeeDAO.EmployeeDAOFactory.getInstance();

    public EmployeeDBO findById(Integer employeeId){

        return employeeDAO.findById(employeeId);
    }


    public EmployeeDBO findByPersonId(Integer personId){

        return employeeDAO.findByPersonId(personId);
    }
}
