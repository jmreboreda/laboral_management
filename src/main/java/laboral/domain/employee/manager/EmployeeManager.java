package laboral.domain.employee.manager;

import laboral.domain.employee.Employee;
import laboral.domain.employee.EmployeeCreationRequest;
import laboral.domain.employee.mapper.MapperEmployeeDBOToEmployee;
import laboral.domain.employee.persistence.dao.EmployeeDAO;
import laboral.domain.employee.persistence.dbo.EmployeeDBO;

import java.util.ArrayList;
import java.util.List;

public class EmployeeManager {

    public EmployeeManager() {
    }

    EmployeeDAO employeeDAO = EmployeeDAO.EmployeeDAOFactory.getInstance();


    public Integer createEmployee(EmployeeCreationRequest employeeCreationRequest){

        EmployeeDBO employeeDBO = new EmployeeDBO();
        employeeDBO.setId(employeeCreationRequest.getId());
        employeeDBO.setPersonDBO(employeeCreationRequest.getPersonDBO());

        return employeeDAO.create(employeeDBO);

    }
    public Employee findEmployeeByPersonId(Integer personId){

        EmployeeDBO employeeDBO = employeeDAO.findByPersonId(personId);
        if(employeeDBO == null){
            return null;
        }

        MapperEmployeeDBOToEmployee mapperEmployeeDBOToEmployee = new MapperEmployeeDBOToEmployee();
        return mapperEmployeeDBOToEmployee.map(employeeDBO);
    }

    public List<Employee> findEmployeeByNamePattern(String pattern){

        List<Employee> employeeList = new ArrayList<>();
        MapperEmployeeDBOToEmployee mapperEmployeeDBOToEmployee = new MapperEmployeeDBOToEmployee();

        List<EmployeeDBO> employeeDBOList = employeeDAO.findEmployeeByNamePattern(pattern);
        for(EmployeeDBO employeeDBO : employeeDBOList){
            Employee employee = mapperEmployeeDBOToEmployee.map(employeeDBO);
            employeeList.add(employee);
        }

        return employeeList;
    }

    public Employee findById(Integer id){
        MapperEmployeeDBOToEmployee mapperEmployeeDBOToEmployee = new MapperEmployeeDBOToEmployee();

        EmployeeDBO employeeDBO = employeeDAO.findById(id);

        return mapperEmployeeDBOToEmployee.map(employeeDBO);

    }

    public List<Employee> findAll(){

        MapperEmployeeDBOToEmployee mapperEmployeeDBOToEmployee = new MapperEmployeeDBOToEmployee();

        List<Employee> employeeList = new ArrayList<>();
        List<EmployeeDBO> employeeDBOList = employeeDAO.findAll();
        for(EmployeeDBO employeeDBO : employeeDBOList){
            Employee employee = mapperEmployeeDBOToEmployee.map(employeeDBO);
            employeeList.add(employee);
        }

        return employeeList;
    }
}
