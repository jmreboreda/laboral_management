package laboral.domain.employee.mapper;

import laboral.domain.employee.Employee;
import laboral.domain.employee.persistence.dbo.EmployeeDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.person.persistence.dbo.PersonDBO;

public class MapperEmployeeDBOToEmployee implements GenericMapper<EmployeeDBO, Employee> {

    @Override
    public Employee map(EmployeeDBO employeeDBO) {

        Employee employee = new Employee();
        employee.setId(employeeDBO.getId());

        PersonDBO personDBO = PersonDBO.create()
                .withId(employeeDBO.getPersonDBO().getId())
                .withPersonType(employeeDBO.getPersonDBO().getPersonType())
                .withFirstSurname(employeeDBO.getPersonDBO().getFirstSurname())
                .withSecondSurname(employeeDBO.getPersonDBO().getSecondSurname())
                .withName(employeeDBO.getPersonDBO().getName())
                .withNieNif(employeeDBO.getPersonDBO().getNieNif())
                .withSocialSecurityAffiliationNumber(employeeDBO.getPersonDBO().getSocialSecurityAffiliationNumber())
                .withBirthDate(employeeDBO.getPersonDBO().getBirthDate())
                .withCivilStatus(employeeDBO.getPersonDBO().getCivilStatus())
                .withStudy(employeeDBO.getPersonDBO().getStudy())
                .withAddresses(employeeDBO.getPersonDBO().getAddresses())
                .withNationality(employeeDBO.getPersonDBO().getNationality())
                .build();

        employee.setPerson(personDBO);

        return employee;
    }
}
