package laboral.domain.employee;

import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.interface_pattern.NaturalPersonality;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.study.StudyLevelType;

import java.time.LocalDate;
import java.util.Objects;

public class Employee implements NaturalPersonality {

    private Integer id;
    private PersonDBO person;

    public Employee() {
    }

    public Employee(Integer id, PersonDBO person) {
        this.id = id;
        this.person = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Employee)) return false;
        Employee employee = (Employee) o;
        return Objects.equals(id, employee.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, person.toAlphabeticalName());
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    private PersonDBO getPerson() {
        return person;
    }

    public void setPerson(PersonDBO person) {
        this.person = person;
    }

    public String toString(){
        String firstSurname = person.getFirstSurname() == null ? person.getFirstSurname() + ", " : person.getFirstSurname() + " ";
        String secondSurname = person.getSecondSurname() == null ? "" : person.getSecondSurname() + ", ";

        return firstSurname
                .concat(secondSurname)
                .concat(person.getName());
    }

    public Integer getPersonId(){
        return getPerson().getId();
    }

    @Override
    public String getFirstSurName() {
        return getPerson().getFirstSurname();
    }

    @Override
    public String getSecondSurName() {
        return getPerson().getSecondSurname();
    }

    @Override
    public String getName() {
        return getPerson().getName();
    }

    @Override
    public String getSocialSecurityAffiliationNumber() {
        return getSocialSecurityAffiliationNumber();
    }

    @Override
    public LocalDate getBirthDate() {
        return getBirthDate();
    }

    @Override
    public CivilStatusType getCivilStatus() {
        return getCivilStatus();
    }

    @Override
    public StudyLevelType getStudy() {
        return getStudy();
    }

    @Override
    public String getNationality() {
        return getNationality();
    }

    @Override
    public String toAlphabeticalName() {
        return getPerson().toAlphabeticalName();
    }

    @Override
    public String toNaturalName() {
        return getPerson().toNaturalName();
    }

    @Override
    public NieNif getNieNif() {
        return new NieNif(getPerson().getNieNif());
    }
}
