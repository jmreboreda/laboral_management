package laboral.domain.employee.persistence.dao;


import laboral.domain.employee.persistence.dbo.EmployeeDBO;
import laboral.domain.employer.persistence.dao.EmployerDAO;
import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.logging.Logger;

public class EmployeeDAO implements GenericDAO<EmployeeDBO, Integer> {

    private static final Logger logger = Logger.getLogger(EmployeeDAO.class.getSimpleName());

    private SessionFactory sessionFactory;
    private Session session;

    public EmployeeDAO() {
    }

    public static class EmployeeDAOFactory {

        private static EmployeeDAO employeeDAO;

        public static EmployeeDAO getInstance() {
            if(employeeDAO == null) {
                employeeDAO = new EmployeeDAO(HibernateUtil.retrieveGlobalSession());
            }
            return employeeDAO;
        }

    }

    private EmployeeDAO(Session session) {
        this.session = session;
    }


    @Override
    public Integer create(EmployeeDBO employeeDBO) {
        try {
            session.beginTransaction();
            session.saveOrUpdate(employeeDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }
        return employeeDBO.getId();
    }

    @Override
    public EmployeeDBO findById(Integer employeeId) {
        TypedQuery<EmployeeDBO> query = session.createNamedQuery(EmployeeDBO.FIND_EMPLOYEE_BY_ID, EmployeeDBO.class);
        query.setParameter("employeeId", employeeId);

        try {
            return query.getSingleResult();
        }catch(NoResultException e){
            return null;
        }
    }

    public EmployeeDBO findByPersonId(Integer personId){

        EmployeeDBO employeeDBO = null;

        TypedQuery<EmployeeDBO> query = session.createNamedQuery(EmployeeDBO.FIND_EMPLOYEE_BY_PERSON_ID, EmployeeDBO.class);
        query.setParameter("personId", personId);

        try{
            employeeDBO = query.getSingleResult();
//            logger.info("The person " + employeeDBO.toAlphabeticalName() + " IS ALREADY an employee.");

        }catch(NoResultException e){
//            logger.info("The selected person is NOT an employee.");
        }

        return employeeDBO;
    }

    public List<EmployeeDBO> findEmployeeByNamePattern(String pattern){
        TypedQuery<EmployeeDBO> query = session.createNamedQuery(EmployeeDBO.FIND_EMPLOYEE_BY_NAME_PATTERN, EmployeeDBO.class);
        query.setParameter("pattern", "%" + pattern.toLowerCase() + "%");

        return query.getResultList();
    }

    @Override
    public Integer update(EmployeeDBO entity) {

        return null;
    }

    @Override
    public Boolean delete(EmployeeDBO employeeDBO) {

        return null;
    }

    @Override
    public List<EmployeeDBO> findAll() {
        TypedQuery<EmployeeDBO> query = session.createNamedQuery(EmployeeDBO.FIND_ALL_EMPLOYEE, EmployeeDBO.class);

        return query.getResultList();
    }

}
