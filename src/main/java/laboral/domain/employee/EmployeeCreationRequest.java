package laboral.domain.employee;

import laboral.domain.person.persistence.dbo.PersonDBO;

public class EmployeeCreationRequest {

    private Integer id;
    private PersonDBO personDBO;

    public EmployeeCreationRequest() {
    }

    public EmployeeCreationRequest(Integer id, PersonDBO personDBO) {
        this.id = id;
        this.personDBO = personDBO;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public PersonDBO getPersonDBO() {
        return personDBO;
    }

    public void setPersonDBO(PersonDBO personDBO) {
        this.personDBO = personDBO;
    }

}
