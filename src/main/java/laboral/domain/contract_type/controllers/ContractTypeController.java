package laboral.domain.contract_type.controllers;

import laboral.domain.contract_type.WorkContractType;
import laboral.domain.contract_type.manager.ContractTypeManager;

import java.util.List;

public class ContractTypeController {

    public ContractTypeController() {
    }

    ContractTypeManager contractTypeManager = new ContractTypeManager();

    public List<WorkContractType> findAll(){

        return contractTypeManager.findAll();
    }
}
