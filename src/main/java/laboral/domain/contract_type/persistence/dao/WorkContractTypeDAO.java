package laboral.domain.contract_type.persistence.dao;


import laboral.domain.contract_type.persistence.dbo.WorkContractTypeDBO;
import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class WorkContractTypeDAO implements GenericDAO<WorkContractTypeDBO, Integer> {

    private SessionFactory sessionFactory;
    private Session session;

    public WorkContractTypeDAO() {
    }

    public static class WorkContractTypeDAOFactory {

        private static WorkContractTypeDAO workContractTypeDAO;

        public static WorkContractTypeDAO getInstance() {
            if(workContractTypeDAO == null) {
                workContractTypeDAO = new WorkContractTypeDAO(HibernateUtil.retrieveGlobalSession());
            }
            return workContractTypeDAO;
        }
    }

    public WorkContractTypeDAO(Session session) {
        this.session = session;
    }

    @Override
    public Integer create(WorkContractTypeDBO workContractTypeDBO) {
        try {
            session.beginTransaction();
            session.saveOrUpdate(workContractTypeDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }
        return workContractTypeDBO.getId();
    }

    @Override
    public WorkContractTypeDBO findById(Integer contractTypeId) {
        TypedQuery<WorkContractTypeDBO> query = session.createNamedQuery(WorkContractTypeDBO.FIND_CONTRACT_TYPE_BY_ID, WorkContractTypeDBO.class);
        query.setParameter("contractTypeId", contractTypeId);

        return query.getSingleResult();    }

    @Override
    public Integer update(WorkContractTypeDBO workContractTypeDBO) {

        return null;
    }

    public WorkContractTypeDBO findByWorkContractTypeCode(Integer workContractTypeCode){
        TypedQuery<WorkContractTypeDBO> query = session.createNamedQuery(WorkContractTypeDBO.FIND_WORK_CONTRACT_TYPE_BY_WORK_CONTRACT_TYPE_CODE, WorkContractTypeDBO.class);
        query.setParameter("workContractTypeParameter", workContractTypeCode);

        return query.getSingleResult();
    }

    @Override
    public Boolean delete(WorkContractTypeDBO workContractTypeDBO) {

        return null;
    }

    @Override
    public List<WorkContractTypeDBO> findAll() {
        Query query = session.createNamedQuery(WorkContractTypeDBO.FIND_ALL_SELECTABLE_CONTRACT_TYPES, WorkContractTypeDBO.class);

        return query.getResultList();
    }
}
