package laboral.domain.contract_type;

import laboral.domain.contract_type.persistence.dao.WorkContractTypeDAO;
import laboral.domain.contract_type.persistence.dbo.WorkContractTypeDBO;

public class WorkContractTypeService {

    private WorkContractTypeService() {
    }

    public static class WorkContractTypeServiceFactory {

        private static WorkContractTypeService workContractTypeService;

        public static WorkContractTypeService getInstance() {
            if(workContractTypeService == null) {
                workContractTypeService = new WorkContractTypeService();
            }
            return workContractTypeService;
        }
    }


    private WorkContractTypeDAO workContractTypeDAO = WorkContractTypeDAO.WorkContractTypeDAOFactory.getInstance();

    public WorkContractTypeDBO findWorkContractTypeByContractTypeCode(Integer workContractTypeCode){

        return workContractTypeDAO.findByWorkContractTypeCode(workContractTypeCode);
    }
}
