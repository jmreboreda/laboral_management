package laboral.domain.activity_period.mapper;

import laboral.domain.activity_period.ActivityPeriod;
import laboral.domain.activity_period.PersistedActivityPeriod;
import laboral.domain.activity_period.persistence.dbo.ActivityPeriodDBO;
import laboral.domain.interface_pattern.GenericMapper;

import java.sql.Date;

public class MapperActivityPeriodToActivityPeriodDBO implements GenericMapper<ActivityPeriod, ActivityPeriodDBO> {

    @Override
    public ActivityPeriodDBO map(ActivityPeriod activityPeriod) {

        Date dateTo = activityPeriod.getDateTo() == null ? null : Date.valueOf(activityPeriod.getDateTo());
        Date withOutActivityDate = activityPeriod.getWithoutActivityDate() == null ? null : Date.valueOf(activityPeriod.getWithoutActivityDate());

        ActivityPeriodDBO activityPeriodDBO = new ActivityPeriodDBO();
        activityPeriodDBO.setId(activityPeriod.getId());
        activityPeriodDBO.setDateFrom(Date.valueOf(activityPeriod.getDateFrom()));
        activityPeriodDBO.setDateTo(dateTo);
        activityPeriodDBO.setWithoutActivityDate(withOutActivityDate);

        return activityPeriodDBO;
    }
}
