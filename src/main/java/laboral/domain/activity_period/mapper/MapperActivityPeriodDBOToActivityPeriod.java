package laboral.domain.activity_period.mapper;

import laboral.domain.activity_period.ActivityPeriod;
import laboral.domain.activity_period.PersistedActivityPeriod;
import laboral.domain.activity_period.persistence.dbo.ActivityPeriodDBO;
import laboral.domain.client.mapper.MapperClientDBOToClient;
import laboral.domain.interface_pattern.GenericMapper;

import java.time.LocalDate;

public class MapperActivityPeriodDBOToActivityPeriod implements GenericMapper<ActivityPeriodDBO, ActivityPeriod> {

    @Override
    public ActivityPeriod map(ActivityPeriodDBO activityPeriodDBO) {

        LocalDate dateFrom = activityPeriodDBO.getDateFrom() == null ? null : activityPeriodDBO.getDateFrom().toLocalDate();
        LocalDate dateTo = activityPeriodDBO.getDateTo() == null ? null : activityPeriodDBO.getDateTo().toLocalDate();
        LocalDate withoutActivityDate = activityPeriodDBO.getWithoutActivityDate() == null ? null : activityPeriodDBO.getWithoutActivityDate().toLocalDate();



        ActivityPeriod activityPeriod = new ActivityPeriod();
        activityPeriod.setId(activityPeriodDBO.getId());
        activityPeriod.setClient(activityPeriodDBO.getClientDBO());
        activityPeriod.setDateFrom(dateFrom);
        activityPeriod.setDateTo(dateTo);
        activityPeriod.setWithoutActivityDate(withoutActivityDate);

        return activityPeriod;
    }
}
