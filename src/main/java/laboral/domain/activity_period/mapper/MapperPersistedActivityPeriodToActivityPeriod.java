package laboral.domain.activity_period.mapper;

import laboral.domain.activity_period.ActivityPeriod;
import laboral.domain.activity_period.PersistedActivityPeriod;
import laboral.domain.interface_pattern.GenericMapper;

public class MapperPersistedActivityPeriodToActivityPeriod implements GenericMapper<PersistedActivityPeriod, ActivityPeriod> {

    @Override
    public ActivityPeriod map(PersistedActivityPeriod persistedActivityPeriod) {

        ActivityPeriod activityPeriod = new ActivityPeriod();
        activityPeriod.setDateFrom(persistedActivityPeriod.getDateFrom());
        activityPeriod.setDateTo(persistedActivityPeriod.getDateTo());
        activityPeriod.setWithoutActivityDate(persistedActivityPeriod.getWithoutActivityDate());

        return activityPeriod;
    }
}
