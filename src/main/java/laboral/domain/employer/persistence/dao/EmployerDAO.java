package laboral.domain.employer.persistence.dao;

import laboral.domain.client.persistence.dbo.ClientDBO;
import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.interface_pattern.GenericDAO;
import laboral.domain.utilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.logging.Logger;

public class EmployerDAO implements GenericDAO<EmployerDBO, Integer> {

    private static final Logger logger = Logger.getLogger(EmployerDAO.class.getSimpleName());

    private SessionFactory sessionFactory;
    private Session session;

    public EmployerDAO() {
    }

    public static class EmployerDAOFactory {

        private static EmployerDAO employerDAO;

        public static EmployerDAO getInstance() {
            if(employerDAO == null) {
                employerDAO = new EmployerDAO(HibernateUtil.retrieveGlobalSession());
            }
            return employerDAO;
        }

    }

    private EmployerDAO(Session session) {
        this.session = session;
    }

    public Session getSession() {
        return session;
    }


    @Override
    public Integer create(EmployerDBO employerDBO) {
        try {
            session.beginTransaction();
            session.saveOrUpdate(employerDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }
        return employerDBO.getId();
    }

    @Override
    public EmployerDBO findById(Integer employerId) {
        TypedQuery<EmployerDBO> query = session.createNamedQuery(EmployerDBO.FIND_EMPLOYER_BY_ID, EmployerDBO.class);
        query.setParameter("employerId", employerId);

        return query.getSingleResult();
    }

    public EmployerDBO findByClientId(Integer clientId) {

        EmployerDBO employerDBO = null;

        TypedQuery<EmployerDBO> query = session.createNamedQuery(EmployerDBO.FIND_EMPLOYER_BY_CLIENT_ID, EmployerDBO.class);
        query.setParameter("clientId", clientId);

        try{
            employerDBO = query.getSingleResult();
//            logger.info("The client " + employerDBO.toAlphabeticalName() + " IS ALREADY an employer.");

        }catch(NoResultException e){
//            logger.info("The selected client is NOT an employer.");
        }

        return employerDBO;
    }

//    // With NativeQuery
//    public EmployerDBO findByClientIdWithNativeQuery(Integer clientId) {
//
//        EmployerDBO employerDBO = null;
//
//        try {
//            employerDBO = session.createNativeQuery("SELECT * FROM employer WHERE clientId = " + clientId, EmployerDBO.class).getSingleResult();
//            logger.info("The client " + employerDBO.toAlphabeticalName() + " IS ALREADY an employer.");
//
//        }catch(NoResultException e){
//            logger.info("The selected client is NOT employer.");
//        }
//
//        return employerDBO;
//    }

    @Override
    public Integer update(EmployerDBO employerDBO) {
        try {
            session.beginTransaction();
            session.merge(employerDBO);
            session.getTransaction().commit();
        }
        catch (org.hibernate.exception.ConstraintViolationException cve){

        }

        return employerDBO.getId();
    }

    @Override
    public Boolean delete(EmployerDBO entity) {
        return null;
    }


    @Override
    public List<EmployerDBO> findAll() {
        TypedQuery<EmployerDBO> query = session.createNamedQuery(EmployerDBO.FIND_ALL_EMPLOYER, EmployerDBO.class);

        return query.getResultList();
    }

    public List<EmployerDBO> findActiveEmployerByPattern(String pattern){
        TypedQuery<EmployerDBO> query = session.createNamedQuery(EmployerDBO.FIND_ACTIVE_EMPLOYER_BY_NAME_PATTERN, EmployerDBO.class);
        query.setParameter("pattern", "%" + pattern.toLowerCase() + "%");

        return query.getResultList();
    }

}
