package laboral.domain.employer.persistence.dbo;

import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.client.persistence.dbo.ClientDBO;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.interface_pattern.LegalPersonality;
import laboral.domain.interface_pattern.NaturalPersonality;
import laboral.domain.nienif.NieNif;
import laboral.domain.quote_account_code.persistence.dbo.QuoteAccountCodeDBO;
import laboral.domain.study.StudyLevelType;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "employer")
@NamedQueries(value = {
        @NamedQuery(
                name = EmployerDBO.FIND_ALL_EMPLOYER,
                query = "select p from EmployerDBO as p"
        ),
        @NamedQuery(
                name = EmployerDBO.FIND_EMPLOYER_BY_ID,
                query = "select p from EmployerDBO as p where p.id = :employerId"
        ),
        @NamedQuery(
                name = EmployerDBO.FIND_EMPLOYER_BY_CLIENT_ID,
                query = "select p from EmployerDBO as p where p.clientDBO.id = :clientId"
        ),
        @NamedQuery(
                name = EmployerDBO.FIND_ACTIVE_EMPLOYER_BY_NAME_PATTERN,
                query = "select p from EmployerDBO as p where concat(lower(p.clientDBO.personDBO.firstSurname), ' ', lower(p.clientDBO.personDBO.secondSurname), ', ', lower(p.clientDBO.personDBO.name)) like :pattern or " +
                        "lower(p.clientDBO.personDBO.legalName) like :pattern"
        )
})

public class EmployerDBO implements Serializable, NaturalPersonality, LegalPersonality {

    public static final String FIND_ALL_EMPLOYER = "EmployerDBO.FIND_ALL_EMPLOYER";
    public static final String FIND_EMPLOYER_BY_ID = "EmployerDBO.FIND_EMPLOYER_BY_ID";
    public static final String FIND_EMPLOYER_BY_CLIENT_ID = "EmployerDBO.FIND_EMPLOYER_BY_CLIENT_ID";
    public static final String FIND_ACTIVE_EMPLOYER_BY_NAME_PATTERN = "EmployerDBO.FIND_EMPLOYER_BY_NAME_PATTERN";

    @Id
    @SequenceGenerator(name = "employer_id_seq", sequenceName = "employer_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employer_id_seq")
    @Column(name = "id", updatable = false)
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "clientId")
    private ClientDBO clientDBO;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "employerId")
    private Set<QuoteAccountCodeDBO> quoteAccountCodes;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "employerId")
    private Set<WorkContractDBO> contracts;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClientDBO getClientDBO() {
        return clientDBO;
    }

    public void setClientDBO(ClientDBO clientDBO) {
        this.clientDBO = clientDBO;
    }

    public Set<QuoteAccountCodeDBO> getQuoteAccountCodes() {
        return quoteAccountCodes;
    }

    public void setQuoteAccountCodes(Set<QuoteAccountCodeDBO> quoteAccountCodes) {
        this.quoteAccountCodes = quoteAccountCodes;
    }

    public Set<WorkContractDBO> getContracts() {
        return contracts;
    }

    public void setContracts(Set<WorkContractDBO> contracts) {
        this.contracts = contracts;
    }

    public Boolean isActiveAtDate(LocalDate date){
        if(getClientDBO().isActiveAtDate(date)){

            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    @Override
    public String getFirstSurName() {
        return getClientDBO().getPersonDBO().getFirstSurname();
    }

    @Override
    public String getSecondSurName() {
        return getClientDBO().getPersonDBO().getSecondSurname();
    }

    @Override
    public String getName() {
        return getClientDBO().getPersonDBO().getName();
    }

    @Override
    public String getSocialSecurityAffiliationNumber() {
        return getSocialSecurityAffiliationNumber();
    }

    @Override
    public LocalDate getBirthDate() {
        return getBirthDate();
    }

    @Override
    public CivilStatusType getCivilStatus() {
        return getCivilStatus();
    }

    @Override
    public StudyLevelType getStudy() {
        return getStudy();
    }

    @Override
    public String getNationality() {
        return getNationality();
    }

    @Override
    public String getLegalName() {
        return getClientDBO().getPersonDBO().getLegalName();
    }

    @Override
    public String toAlphabeticalName() {
        return getClientDBO().toAlphabeticalName();
    }

    @Override
    public String toNaturalName() {
        return getClientDBO().toNaturalName();
    }

    @Override
    public NieNif getNieNif() {
        return new NieNif(getClientDBO().getPersonDBO().getNieNif());
    }
}

