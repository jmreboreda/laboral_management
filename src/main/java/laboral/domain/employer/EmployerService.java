package laboral.domain.employer;

import laboral.domain.employer.persistence.dao.EmployerDAO;
import laboral.domain.employer.persistence.dbo.EmployerDBO;

public class EmployerService {

    EmployerDAO employerDAO = EmployerDAO.EmployerDAOFactory.getInstance();

    public EmployerDBO  findByClientId(Integer clientId){

        return employerDAO.findByClientId(clientId);
    }
}
