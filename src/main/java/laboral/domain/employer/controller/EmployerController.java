package laboral.domain.employer.controller;

import laboral.domain.employer.Employer;
import laboral.domain.employer.manager.EmployerManager;

import java.util.List;

public class EmployerController {

    public EmployerController() {
    }

    EmployerManager employerManager = new EmployerManager();

    public Integer createEmployer(Employer employer){
        return employerManager.createEmployer(employer);
    }

    public Integer updateEmployer(Employer employer){

        return employerManager.updateEmployer(employer);
    }

    public Employer findEmployerById(Integer employerId){

        return employerManager.findById(employerId);
    }

    public Employer findEmployerByClientId(Integer clientId){

        return employerManager.findEmployerByClientId(clientId);
    }

    public List<Employer> findAll(){

        return employerManager.findAll();
    }

    public List<Employer> findActiveEmployerByNamePattern(String pattern){

        return employerManager.findActiveEmployerByNamePattern(pattern);
    }
}
