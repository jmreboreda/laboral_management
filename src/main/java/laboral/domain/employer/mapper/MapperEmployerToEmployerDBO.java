package laboral.domain.employer.mapper;

import laboral.domain.employer.Employer;
import laboral.domain.employer.persistence.dbo.EmployerDBO;
import laboral.domain.interface_pattern.GenericMapper;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.quote_account_code.mapper.MapperQuoteAccountCodeToQuoteAccountCodeDBO;
import laboral.domain.quote_account_code.persistence.dbo.QuoteAccountCodeDBO;

import java.util.HashSet;
import java.util.Set;

public class MapperEmployerToEmployerDBO implements GenericMapper<Employer, EmployerDBO> {

    @Override
    public EmployerDBO map(Employer employer) {

        MapperQuoteAccountCodeToQuoteAccountCodeDBO mapperQuoteAccountCodeToQuoteAccountCodeDBO = new MapperQuoteAccountCodeToQuoteAccountCodeDBO();
        Set<QuoteAccountCodeDBO> quoteAccountCodeDBOSet = new HashSet<>();
        for(QuoteAccountCode quoteAccountCode : employer.getQuoteAccountCodes()){
            QuoteAccountCodeDBO quoteAccountCodeDBO = mapperQuoteAccountCodeToQuoteAccountCodeDBO.map(quoteAccountCode);
            quoteAccountCodeDBOSet.add(quoteAccountCodeDBO);
        }

        EmployerDBO employerDBO = new EmployerDBO();
        employerDBO.setClientDBO(employer.getClientDBO());
        employerDBO.setQuoteAccountCodes(quoteAccountCodeDBOSet);

        return employerDBO;
    }
}
