package laboral.domain.employer;

import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.client.persistence.dbo.ClientDBO;
import laboral.domain.contract.WorkContract;
import laboral.domain.interface_pattern.LegalPersonality;
import laboral.domain.interface_pattern.NaturalPersonality;
import laboral.domain.nienif.NieNif;
import laboral.domain.person_type.PersonType;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.study.StudyLevelType;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

public class Employer implements NaturalPersonality, LegalPersonality {

    private Integer id;
    private Set<QuoteAccountCode> quoteAccountCodes;
    private ClientDBO clientDBO;
    private Set<WorkContract> workContracts;

    public Employer(Integer id, Set<QuoteAccountCode> quoteAccountCodes, ClientDBO clientDBO, Set<WorkContract> workContracts) {
        this.id = id;
        this.quoteAccountCodes = quoteAccountCodes;
        this.clientDBO = clientDBO;
        this.workContracts = workContracts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employer employer = (Employer) o;
        return id.equals(employer.id) && clientDBO.equals(employer.clientDBO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, clientDBO);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Set<QuoteAccountCode> getQuoteAccountCodes() {
        return quoteAccountCodes;
    }

    public void setQuoteAccountCodes(Set<QuoteAccountCode> quoteAccountCodes) {
        this.quoteAccountCodes = quoteAccountCodes;
    }

    public ClientDBO getClientDBO() {
        return clientDBO;
    }

    public void setClientDBO(ClientDBO clientDBO) {
        this.clientDBO = clientDBO;
    }

    public Set<WorkContract> getWorkContracts() {
        return workContracts;
    }

    public void setWorkContracts(Set<WorkContract> workContracts) {
        this.workContracts = workContracts;
    }

    public String toString() {

        if (clientDBO.getPersonDBO().getPersonType().equals(PersonType.NATURAL_PERSON)) {
            String firstSurname = clientDBO.getPersonDBO().getFirstSurname() == null ? clientDBO.getPersonDBO().getFirstSurname() + ", " : clientDBO.getPersonDBO().getFirstSurname() + " ";
            String secondSurname = clientDBO.getPersonDBO().getSecondSurname() == null ? "" : clientDBO.getPersonDBO().getSecondSurname() + ", ";

            return firstSurname
                    .concat(secondSurname)
                    .concat(clientDBO.getPersonDBO().getName());
        } else

            return clientDBO.getPersonDBO().getLegalName();
    }

    public Boolean isActiveAtDate(LocalDate date){

        return clientDBO.isActiveAtDate(date);
    }

    @Override
    public String getFirstSurName() {
        return getClientDBO().getPersonDBO().getFirstSurname();
    }

    @Override
    public String getSecondSurName() {
        return getClientDBO().getPersonDBO().getSecondSurname();
    }

    @Override
    public String getName() {
        return getClientDBO().getPersonDBO().getName();
    }

    @Override
    public String getSocialSecurityAffiliationNumber() {
        return getSocialSecurityAffiliationNumber();
    }

    @Override
    public LocalDate getBirthDate() {
        return getBirthDate();
    }

    @Override
    public CivilStatusType getCivilStatus() {
        return getCivilStatus();
    }

    @Override
    public StudyLevelType getStudy() {
        return getStudy();
    }

    @Override
    public String getNationality() {
        return getNationality();
    }

    @Override
    public String getLegalName() {
        return getClientDBO().getPersonDBO().getLegalName();
    }

    @Override
    public String toAlphabeticalName() {
        return getClientDBO().toAlphabeticalName();
    }

    @Override
    public String toNaturalName() {
        return getClientDBO().toNaturalName();
    }

    @Override
    public NieNif getNieNif() {
        return new NieNif(clientDBO.getPersonDBO().getNieNif());
    }


    public static final class EmployerBuilder {
        private Integer id;
        private Set<QuoteAccountCode> quoteAccountCodes;
        private ClientDBO clientDBO;
        private Set<WorkContract> workContracts;

        private EmployerBuilder() {
        }

        public static EmployerBuilder create() {
            return new EmployerBuilder();
        }

        public EmployerBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public EmployerBuilder withQuoteAccountCodes(Set<QuoteAccountCode> quoteAccountCodes) {
            this.quoteAccountCodes = quoteAccountCodes;
            return this;
        }

        public EmployerBuilder withClientDBO(ClientDBO clientDBO) {
            this.clientDBO = clientDBO;
            return this;
        }

        public EmployerBuilder withWorkContracts(Set<WorkContract> workContracts) {
            this.workContracts = workContracts;
            return this;
        }

        public Employer build() {
            return new Employer(id, quoteAccountCodes, clientDBO, workContracts);
        }
    }
}
