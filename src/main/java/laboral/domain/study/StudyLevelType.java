package laboral.domain.study;

public enum StudyLevelType {

    UNKNOWN("Desconocido"),
    WITHOUT_STUDIES("Sin estudios"),
    PRIMARY_STUDIES("Estudios primarios"),
    EGB_STUDIES("E.G.B."),
    ESO_STUDIES("E.S.O."),
    BACHELOR("Bachillerato"),
    PROFESSIONAL_CERTIFICATE("Certificado de profesionalidad"),
    PROFESSIONAL_FORMATION_I("Formación profesional I"),
    PROFESSIONAL_FORMATION_II("Formación profesional II"),
    MID_CYCLE("Ciclo medio"),
    SUPERIOR_CYCLE("Ciclo superior"),
    THREE_YEARS_UNIVERSITY_DEGREE("Diplomatura universitaria"),
    FOUR_YEARS_UNIVERSITY_DEGREE("Grado universitario"),
    FIVE_YEARS_UNIVERSITY_DEGREE("Licenciatura universitaria"),
    PHD_STUDIES("Estudios de doctorado"),
    PHD("Doctorado");

    private String studyLevelDescription;

    StudyLevelType(String studyLevelDescription) {
        this.studyLevelDescription = studyLevelDescription;
    }

    public String getStudyLevelDescription() {
        return studyLevelDescription;
    }

    public String toString(){
        return getStudyLevelDescription();
    }
}
