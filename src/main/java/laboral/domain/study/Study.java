package laboral.domain.study;

public class Study {

    private Integer id;
    private StudyLevelType studyLevel;

    public Study() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StudyLevelType getStudyLevelType() {
        return studyLevel;
    }

    public void setStudyLevelType(StudyLevelType studyLevel) {
        this.studyLevel = studyLevel;
    }
}
