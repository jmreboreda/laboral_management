package laboral.domain.contract_json;

import java.util.Map;

public class ContractSchedule {

    private Map<String, ContractDaySchedule> schedule;

    public ContractSchedule() {
    }

    public ContractSchedule(Map<String, ContractDaySchedule> schedule) {
        this.schedule = schedule;
    }


    public Map<String, ContractDaySchedule> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, ContractDaySchedule> schedule) {
        this.schedule = schedule;
    }
}
