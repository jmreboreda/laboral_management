package laboral.domain.contract_json;

public class ContractDaySchedule {

    private String dayOfWeek;
    private String date;
    private String amFrom;
    private String amTo;
    private String pmFrom;
    private String pmTo;
    private String durationHours;

    public ContractDaySchedule(String dayOfWeek, String date, String amFrom, String amTo, String pmFrom, String pmTo, String durationHours) {
        this.dayOfWeek = dayOfWeek;
        this.date = date;
        this.amFrom = amFrom;
        this.amTo = amTo;
        this.pmFrom = pmFrom;
        this.pmTo = pmTo;
        this.durationHours = durationHours;
    }

    public ContractDaySchedule() {
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public String getDate() {
        return date;
    }

    public String getAmFrom() {
        return amFrom;
    }

    public String getAmTo() {
        return amTo;
    }

    public String getPmFrom() {
        return pmFrom;
    }

    public String getPmTo() {
        return pmTo;
    }

    public String getDurationHours() {
        return durationHours;
    }

    public static ContractDaySchedule.WorkDayBuilder create() {
        return new ContractDaySchedule.WorkDayBuilder();
    }

    public static class WorkDayBuilder {

        private String dayOfWeek;
        private String date;
        private String amFrom;
        private String amTo;
        private String pmFrom;
        private String pmTo;
        private String durationHours;

        public ContractDaySchedule.WorkDayBuilder withDayOfWeek(String dayOfWeek) {
            this.dayOfWeek = dayOfWeek;
            return this;
        }

        public ContractDaySchedule.WorkDayBuilder withDate(String date) {
            this.date = date;
            return this;
        }

        public ContractDaySchedule.WorkDayBuilder withAmFrom(String amFrom) {
            this.amFrom = amFrom;
            return this;
        }

        public ContractDaySchedule.WorkDayBuilder withAmTo(String amTo) {
            this.amTo = amTo;
            return this;
        }

        public ContractDaySchedule.WorkDayBuilder withPmFrom(String pmFrom) {
            this.pmFrom = pmFrom;
            return this;
        }

        public ContractDaySchedule.WorkDayBuilder withPmTo(String pmTo) {
            this.pmTo = pmTo;
            return this;
        }

        public ContractDaySchedule.WorkDayBuilder withDurationHours(String durationHours) {
            this.durationHours = durationHours;
            return this;
        }

        public ContractDaySchedule build() {
            return new ContractDaySchedule(this.dayOfWeek, this.date, this.amFrom, this.amTo, this.pmFrom, this.pmTo, this.durationHours);
        }
    }
}
