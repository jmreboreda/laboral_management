package laboral;

public enum ApplicationConstants {

    /** System */
    OPERATING_SYSTEM(System.getProperty("os.name").toLowerCase()),
    USER_HOME(System.getProperty("user.home").toLowerCase()),
    OS_LINUX("linux"),
    OS_WINDOWS("windows"),

    /** Printer */
    DEFAULT_PRINTER("KONICA MINOLTA"),

    /** Images */
    APPLICATION_ICON("/pics/GMapp_PNG_64x64.png"),
    SPLASH_IMAGE("/pics/GMapp_PNG_64x64.png"),

    /** Formats*/
    DEFAULT_DATE_FORMAT("dd-MM-yyyy"),
    DEFAULT_TIME_FORMAT("HH:mm"),
    ENGLISH_DATE_FORMAT("yyyyMMdd"),

    /** Various */
    PDF_EXTENSION(".pdf"),
    DEFAULT_HOURS_FOR_FULL_TIME("40:00");

    private String value;

    ApplicationConstants(String applicationConstantDescription) {
        this.value = applicationConstantDescription;
    }

    public String getValue() {
        return value;
    }

    public String toString(){
        return getValue();
    }
}
