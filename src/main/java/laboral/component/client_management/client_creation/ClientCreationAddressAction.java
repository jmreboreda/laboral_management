package laboral.component.client_management.client_creation;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class ClientCreationAddressAction extends AnchorPane {

    private static final String CLIENT_MANAGEMENT_ADDRESS_ACTION_FXML = "/fxml/client_management/common_components/client_management_address_action.fxml";

    private EventHandler<MouseEvent> onNewAddressEventHandler;
    private EventHandler<MouseEvent> onDeleteAddressEventHandler;
    private EventHandler<MouseEvent> onOkButtonEventHandler;
    private EventHandler<MouseEvent> onSaveButtonEventHandler;
    private EventHandler<MouseEvent> onExitButtonEventEventHandler;


    private Parent parent;

    @FXML
    private Button newWorkCenterButton;
    @FXML
    private Button deleteWorkCenterButton;

    public ClientCreationAddressAction() {
        this.parent = ViewLoader.load(this, CLIENT_MANAGEMENT_ADDRESS_ACTION_FXML);
    }

    @FXML
    public void initialize(){

//        newAddressButton.setOnMouseClicked(this::onNewAddressButton);
//        deleteAddressButton.setOnMouseClicked(this::onDeleteAddressButton);
    }

    public Button getNewWorkCenterButton() {
        return newWorkCenterButton;
    }

    public void setNewWorkCenterButton(Button newWorkCenterButton) {
        this.newWorkCenterButton = newWorkCenterButton;
    }

    public Button getDeleteWorkCenterButton() {
        return deleteWorkCenterButton;
    }

    public void setDeleteWorkCenterButton(Button deleteWorkCenterButton) {
        this.deleteWorkCenterButton = deleteWorkCenterButton;
    }

    private void onNewAddressButton(MouseEvent event){
        this.onNewAddressEventHandler.handle(event);
    }

    private void onDeleteAddressButton(MouseEvent event){
        this.onDeleteAddressEventHandler.handle(event);
    }
}
