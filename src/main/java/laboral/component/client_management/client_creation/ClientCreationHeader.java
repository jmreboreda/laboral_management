package laboral.component.client_management.client_creation;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class ClientCreationHeader extends AnchorPane {

    private static final String NEW_PERSON_HEADER_FXML = "/fxml/client_management/client_creation/client_creation_header.fxml";

    private Parent parent;

    @FXML
    private Label headerText;

    public ClientCreationHeader() {
        this.parent = ViewLoader.load(this, NEW_PERSON_HEADER_FXML);
    }

    public Label getHeaderText() {
        return headerText;
    }
}
