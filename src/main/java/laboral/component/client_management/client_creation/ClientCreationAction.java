package laboral.component.client_management.client_creation;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class ClientCreationAction extends AnchorPane {

    private static final String NEW_PERSON_ACTION_FXML = "/fxml/person_management/generic/person_management_action_components.fxml";

    private Parent parent;


    @FXML
    private Button okButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button exitButton;

    public ClientCreationAction() {
        this.parent = ViewLoader.load(this, NEW_PERSON_ACTION_FXML);
    }

    @FXML
    public void initialize(){
    }

     public Button getOkButton() {
        return okButton;
    }

    public Button getSaveButton() {
        return saveButton;
    }

    public Button getExitButton() {
        return exitButton;
    }

    public void setOkButton(Button okButton) {
        this.okButton = okButton;
    }

    public void setSaveButton(Button saveButton) {
        this.saveButton = saveButton;
    }

    public void setExitButton(Button exitButton) {
        this.exitButton = exitButton;
    }
}
