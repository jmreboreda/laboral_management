package laboral.component.client_management.client_modification.generic_components.event;

import javafx.event.Event;
import javafx.event.EventType;

public class NaturalPersonDataChangedEvent extends Event {

    public static final EventType<NaturalPersonDataChangedEvent> NATURAL_PERSON_DATA_CHANGED_EVENT_TYPE = new EventType<>("PERSON_NAME_TO_MODIFICATION_EVENT_TYPE");
    private final String newValue;

    public NaturalPersonDataChangedEvent(String newValue) {
        super(NATURAL_PERSON_DATA_CHANGED_EVENT_TYPE);
        this.newValue = newValue;

    }

    public String getNaturalPersonToModify() {
        return newValue;
    }
}
