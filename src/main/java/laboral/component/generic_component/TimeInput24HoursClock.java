package laboral.component.generic_component;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import laboral.component.ViewLoader;
import laboral.domain.utilities.Parameters;
import java.util.regex.Pattern;


public class TimeInput24HoursClock extends HBox {

    private static final String TEXT_INPUT = "/fxml/generic/text_input.fxml";

    private Parent parent;

    @FXML
    private Label textLabel;
    @FXML
    private TextField textField;

    public TimeInput24HoursClock() {

        this.parent = ViewLoader.load(this, TEXT_INPUT);
    }

    @FXML
    private void initialize(){

        this.textLabel.setMaxWidth(60);
        this.textField.setPrefWidth(75);
        this.textLabel.setText("Hora: ");
        textField.setAlignment(Pos.CENTER);
        setMargin(textLabel, new Insets(0, 5, 0, 0));
        setMargin(textField, new Insets(0, 5, 0, 0));
        textField.promptTextProperty().setValue("hh:mm");
        textField.setText(null);

        textField.focusedProperty().addListener((arg0, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue)
            {
                onAction(new ActionEvent());
            }
        });

        textField.setOnAction(this::onAction);
    }

    public String getText(){
        return this.textField.getText();
    }

    public void setText(String text){
        this.textField.setText(text);
    }

    public void setTextLabel (String text){
        this.textLabel.setText(text);
    }

    public void setLabelPreferredWidth(Double width){
        this.textLabel.setPrefWidth(width);
    }

    public void setInputPreferredWidth(Double width){
        this.textField.setPrefWidth(width);
    }

    public void setEditable(Boolean bol){
        this.textField.setEditable(bol);
    }

    public void setInputMinWidth(Double width){
        this.textField.setMinWidth(width);
    }

    public void inputRequestFocus(){
        this.textField.requestFocus();
    }

    private void onAction(ActionEvent event) {

        String timeAsString = textField.getText();

        if(timeAsString == null){
            return;
        }

        String hours = "";
        String minutes = "";

        Pattern timeCompletePattern = Pattern.compile("\\d{2}:\\d{2}");
        Pattern timeFourDigitPattern = Pattern.compile("\\d{4}");
        Pattern timeOnlyOneDigitPattern = Pattern.compile("\\d");
        Pattern timeOnlyTwoDigitPattern = Pattern.compile("\\d{2}");
        Pattern timeOnlyMinutesPattern = Pattern.compile(":\\d{2}");

        if(timeCompletePattern.matcher(timeAsString).matches()){
            hours = timeAsString.substring(0,2);
            minutes = timeAsString.substring(3,5);
        } else

        if(timeFourDigitPattern.matcher(timeAsString).matches()){
            hours = timeAsString.substring(0,2);
            minutes = timeAsString.substring(2,4);
        } else

        if(timeOnlyOneDigitPattern.matcher(timeAsString).matches()){
            hours = "0".concat(timeAsString);
            minutes = "00";
        } else

        if(timeOnlyTwoDigitPattern.matcher(timeAsString).matches()){
            hours = timeAsString;
            minutes = "00";
        } else

        if(timeOnlyMinutesPattern.matcher(timeAsString).matches()){
            hours = "00";
            minutes = timeAsString.substring(1, 3);
        }
        else {
            textField.setText("");
        }

        if(hours.equals("24")){
            hours = "00";
        }

        if(!textField.getText().equals("") && (Integer.parseInt(hours) > Parameters.INTEGER_MAXIMUM_HOURS_IN_A_DAY ||
                Integer.parseInt(minutes) > Parameters.INTEGER_MAXIMUM_VALUE_MINUTES_IN_HOUR)) {

            textField.setText("");
        }else {
            String twoDot = !hours.equals("") ? ":" : "";
            textField.setText(hours + twoDot + minutes);
        }
    }
}
