package laboral.component.person_management.controllers;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import laboral.component.ViewLoader;
import laboral.component.client_management.client_modification.generic_components.PersonFinderGenericByName;
import laboral.component.person_management.person_modification.ewlp.EntityWithoutLegalPersonalityModificationView;
import laboral.component.person_management.person_modification.legal_person.LegalPersonModificationView;
import laboral.component.person_management.person_modification.natural_person.NaturalPersonModificationView;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person_type.PersonType;
import laboral.domain.utilities.Parameters;
import laboral.domain.validator.PersonDataValidator;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class PersonModificationController extends AnchorPane {

    private static final Logger logger = Logger.getLogger(PersonModificationController.class.getSimpleName());
    private static final String HEADER_PANEL_TEXT = "Selección de la persona a modificar";

    private static final String PERSON_MODIFICATION_CONTROLLER_FXML = "/fxml/person_management/person_modification/person_modification_controller.fxml";

    private final Map<PersonType, PersonModificationView> personModificationViews = new EnumMap<>(PersonType.class);

    private Parent parent;
    @FXML
    private PersonFinderGenericByName personFinderGenericByName;
    @FXML
    private NaturalPersonModificationView naturalPersonModificationView;
    @FXML
    private LegalPersonModificationView legalPersonModificationView;
    @FXML
    private EntityWithoutLegalPersonalityModificationView entityWithoutLegalPersonalityModificationView;

    @FXML
    public void initialize() {
        personFinderGenericByName.getHeaderPane().setText(HEADER_PANEL_TEXT);
        personFinderGenericByName.getHeaderPane().setStyle(Parameters.FOREGROUND_TEXT_BLUE_COLOR);
        personFinderGenericByName.toBack();
        personFinderGenericByName.setVisible(false);
        naturalPersonModificationView.toBack();
        naturalPersonModificationView.setVisible(false);
        legalPersonModificationView.toBack();
        legalPersonModificationView.setVisible(false);
    }

    public PersonModificationController() {
        logger.info("Initializing person modification controller ...");
        this.parent = ViewLoader.load(this, PERSON_MODIFICATION_CONTROLLER_FXML);
        personModificationViews.put(PersonType.LEGAL_PERSON, legalPersonModificationView);
        personModificationViews.put(PersonType.NATURAL_PERSON, naturalPersonModificationView);
        personModificationViews.put(PersonType.ENTITY_WITHOUT_LEGAL_PERSONALITY, entityWithoutLegalPersonalityModificationView);
    }

    public PersonFinderGenericByName getPersonModificationNameFinder() {
        return personFinderGenericByName;
    }

    public void setPersonModificationNameFinder(PersonFinderGenericByName personFinderGenericByName) {
        this.personFinderGenericByName = personFinderGenericByName;
    }

    public NaturalPersonModificationView getNaturalPersonModificationView() {
        return naturalPersonModificationView;
    }

    public void setNaturalPersonModificationView(NaturalPersonModificationView naturalPersonModificationView) {
        this.naturalPersonModificationView = naturalPersonModificationView;
    }

    public LegalPersonModificationView getLegalPersonModificationView() {
        return legalPersonModificationView;
    }

    public void setLegalPersonModificationView(LegalPersonModificationView legalPersonModificationView) {
        this.legalPersonModificationView = legalPersonModificationView;
    }

    public EntityWithoutLegalPersonalityModificationView getEntityWithoutLegalPersonalityModificationView() {
        return entityWithoutLegalPersonalityModificationView;
    }


    public PersonModificationView getPersonModificationView(PersonType personType) {
        return personModificationViews.get(personType);
    }

    public void setEntityWithoutLegalPersonalityModificationView(EntityWithoutLegalPersonalityModificationView entityWithoutLegalPersonalityModificationView) {
        this.entityWithoutLegalPersonalityModificationView = entityWithoutLegalPersonalityModificationView;
    }

    public void initializeViews(){
        personFinderGenericByName.initializeView();
        naturalPersonModificationView.setVisible(false);
        legalPersonModificationView.setVisible(false);
        entityWithoutLegalPersonalityModificationView.setVisible(false);
        personFinderGenericByName.setVisible(false);
    }

    public Boolean validateCorrectDataEnteredFromPerson(PersonCreationRequest personCreationRequest) {

        PersonDataValidator personDataValidator = new PersonDataValidator((Stage) this.getScene().getWindow());

        if (personCreationRequest.getPersonType().equals(PersonType.NATURAL_PERSON)) {
            if (!personDataValidator.naturalPersonDataValidator(personCreationRequest)) {
                return Boolean.FALSE;
            }
        }

        if (personCreationRequest.getPersonType().equals(PersonType.LEGAL_PERSON)) {
            if (!personDataValidator.legalPersonDataValidator(personCreationRequest)) {
                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }

    public List<PersonModificationView> getPersonModificationViews() {
        return Arrays.asList(naturalPersonModificationView, legalPersonModificationView, entityWithoutLegalPersonalityModificationView);
    }
}
