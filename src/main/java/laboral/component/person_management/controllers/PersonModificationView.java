package laboral.component.person_management.controllers;

import javafx.scene.layout.AnchorPane;
import laboral.component.client_management.client_modification.generic_components.AddressesGenericTableView;

public interface PersonModificationView {

    PersonDataEntryView getPersonDataEntryView();

    AddressesGenericTableView getAddress();

    AnchorPane getAnchorPane();

    String getHeaderTitleText();
}
