package laboral.component.person_management.controllers;

import javafx.event.EventHandler;
import laboral.component.person_management.events.PersonDataChangedEvent;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.Person;
import laboral.domain.person.persistence.dbo.PersonDBO;

import java.util.List;

public interface PersonDataEntryView {

    List<PersonDBO> finPersonByNieNif(NieNif nieNif);

    void refreshDataToModifyPerson(Person person);

    void onChanged(EventHandler<PersonDataChangedEvent> personDataEventHandler);

}
