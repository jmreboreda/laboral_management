package laboral.component.person_management.common_components;

import laboral.component.ViewLoader;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

public class PersonManagementAction extends AnchorPane {

    private static final String NEW_PERSON_ACTION_FXML = "/fxml/person_management/generic/person_management_action_components.fxml";

    private EventHandler<MouseEvent> onNewClientEventHandler;
    private EventHandler<MouseEvent> onNewEmployeeEventHandler;
    private EventHandler<MouseEvent> onNewAddressEventHandler;
    private EventHandler<MouseEvent> onDeleteAddressEventHandler;
    private EventHandler<MouseEvent> onOkButtonEventHandler;
    private EventHandler<MouseEvent> onSaveButtonEventHandler;
    private EventHandler<MouseEvent> onExitButtonEventEventHandler;


    private Parent parent;

    @FXML
    private Button newClientButton;
    @FXML
    private Button newEmployeeButton;
    @FXML
    private Button okButton;
    @FXML
    private Button saveButton;
    @FXML
    private Button exitButton;

    public PersonManagementAction() {
        this.parent = ViewLoader.load(this, NEW_PERSON_ACTION_FXML);
    }

    @FXML
    public void initialize(){

        newClientButton.setOnMouseClicked(this::onNewClientButton);
        newEmployeeButton.setOnMouseClicked(this::onNewEmployeeButton);
        okButton.setOnMouseClicked(this::onOkButton);
        saveButton.setOnMouseClicked(this::onSaveButton);
        exitButton.setOnMouseClicked(this::onExitButton);
    }

    public Button getNewClientButton() {
        return newClientButton;
    }

    public Button getNewEmployeeButton() {
        return newEmployeeButton;
    }

    public Button getOkButton() {
        return okButton;
    }

    public Button getSaveButton() {
        return saveButton;
    }

    public Button getExitButton() {
        return exitButton;
    }

    private void onOkButton(MouseEvent event){
        this.onOkButtonEventHandler.handle(event);
    }

    private void onNewClientButton(MouseEvent event){
        onNewClientEventHandler.handle(event);
    }

    private void onNewEmployeeButton(MouseEvent event){
        onNewEmployeeEventHandler.handle(event);
    }

    private void onSaveButton(MouseEvent event){
        this.onSaveButtonEventHandler.handle(event);
    }

    private void onExitButton(MouseEvent event){
        this.onExitButtonEventEventHandler.handle(event);
    }

    public void setOnNewClientButton(EventHandler<MouseEvent> onNewClientEventHandler){
        this.onNewClientEventHandler = onNewClientEventHandler;
    }

    public void setOnNewEmployeeButton(EventHandler<MouseEvent> onNewEmployeeEventHandler){
        this.onNewEmployeeEventHandler = onNewEmployeeEventHandler;
    }

    public void setOnNewAddressButton(EventHandler<MouseEvent> onNewAddressEventHandler){
        this.onNewAddressEventHandler = onNewAddressEventHandler;
    }

    public void setOnDeleteAddressButton(EventHandler<MouseEvent> onDeleteAddressEventHandler){
        this.onDeleteAddressEventHandler = onDeleteAddressEventHandler;
    }

    public void setOnOkButton(EventHandler<MouseEvent> mouseEventEventHandlerOnOkButton){
        this.onOkButtonEventHandler = mouseEventEventHandlerOnOkButton;
    }

    public void setOnSaveButton(EventHandler<MouseEvent> onSaveButtonEventHandler){
        this.onSaveButtonEventHandler = onSaveButtonEventHandler;
    }

    public void setOnExitButton(EventHandler<MouseEvent> mouseEventEventHandlerOnExitButton){
       this.onExitButtonEventEventHandler = mouseEventEventHandlerOnExitButton;
    }
}
