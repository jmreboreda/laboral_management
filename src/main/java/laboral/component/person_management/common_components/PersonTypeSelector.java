package laboral.component.person_management.common_components;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class PersonTypeSelector extends AnchorPane {

    private static final String NEW_PERSON_MANAGEMENT_SELECTOR_FXML = "/fxml/person_management/person_type_selector.fxml";

    private Parent parent;

    @FXML
    private ToggleGroup personTypeGroup;
    @FXML
    private RadioButton naturalPerson;
    @FXML
    private RadioButton legalPerson;
    @FXML
    private RadioButton entityWithoutLegalPersonality;

    public PersonTypeSelector() {
        this.parent = ViewLoader.load(this, NEW_PERSON_MANAGEMENT_SELECTOR_FXML);
    }

    @FXML
    public void initialize() {

    }

    public ToggleGroup getPersonTypeGroup() {
        return personTypeGroup;
    }

    public RadioButton getNaturalPerson() {
        return naturalPerson;
    }

    public RadioButton getLegalPerson() {
        return legalPerson;
    }

    public RadioButton getEntityWithoutLegalPersonality() {
        return entityWithoutLegalPersonality;
    }
}
