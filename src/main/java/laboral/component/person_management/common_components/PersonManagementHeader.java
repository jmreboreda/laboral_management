package laboral.component.person_management.common_components;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;
import javafx.scene.Parent;

public class PersonManagementHeader extends AnchorPane {

    private static final String NEW_PERSON_HEADER_FXML = "/fxml/person_management/person_management_header.fxml";

    private Parent parent;

    @FXML
    private Label headerText;

    public PersonManagementHeader() {
        this.parent = ViewLoader.load(this, NEW_PERSON_HEADER_FXML);
    }

    public Label getHeaderText() {
        return headerText;
    }
}
