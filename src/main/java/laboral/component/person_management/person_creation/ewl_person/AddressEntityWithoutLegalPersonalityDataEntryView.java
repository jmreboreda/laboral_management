package laboral.component.person_management.person_creation.ewl_person;

import laboral.component.client_management.client_modification.generic_components.AddressGenericEntryDataView;

public class AddressEntityWithoutLegalPersonalityDataEntryView extends AddressGenericEntryDataView {

    public AddressEntityWithoutLegalPersonalityDataEntryView() {
    }
}
