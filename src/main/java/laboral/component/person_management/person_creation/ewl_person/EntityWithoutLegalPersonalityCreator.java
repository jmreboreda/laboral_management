package laboral.component.person_management.person_creation.ewl_person;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;
import laboral.domain.address.Address;
import laboral.domain.address.Province;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person_type.PersonType;

import java.util.HashSet;
import java.util.Set;

public class EntityWithoutLegalPersonalityCreator extends AnchorPane {

    private static final String NEW_EWL_PERSONALITY_FXML = "/fxml/person_management/person_creation/ewl_person/new_entity_without_legal_personality_creator.fxml";

    private Parent parent;

    @FXML
    private EntityWithoutLegalPersonalityDataEntryView entityWithoutLegalPersonalityDataEntryView;
    @FXML
    private AddressEntityWithoutLegalPersonalityDataEntryView addressEntityWithoutLegalPersonalityDataEntryView;

    public EntityWithoutLegalPersonalityCreator() {

        this.parent = ViewLoader.load(this, NEW_EWL_PERSONALITY_FXML);
        entityWithoutLegalPersonalityDataEntryView.getHeaderPane().setText("Datos de la entidad");
    }

    @FXML
    public void initialize(){
    }

    public EntityWithoutLegalPersonalityDataEntryView getEntityWithoutLegalPersonalityDataEntryView() {
        return entityWithoutLegalPersonalityDataEntryView;
    }

    public void setEntityWithoutLegalPersonalityDataEntryView(EntityWithoutLegalPersonalityDataEntryView entityWithoutLegalPersonalityDataEntryView) {
        this.entityWithoutLegalPersonalityDataEntryView = entityWithoutLegalPersonalityDataEntryView;
    }

    public AddressEntityWithoutLegalPersonalityDataEntryView getAddressEntityWithoutLegalPersonalityDataEntryView() {
        return addressEntityWithoutLegalPersonalityDataEntryView;
    }

    public void setAddressEntityWithoutLegalPersonalityDataEntryView(AddressEntityWithoutLegalPersonalityDataEntryView addressEntityWithoutLegalPersonalityDataEntryView) {
        this.addressEntityWithoutLegalPersonalityDataEntryView = addressEntityWithoutLegalPersonalityDataEntryView;
    }

    public Boolean validateNoMissingData(){
        return entityWithoutLegalPersonalityDataEntryView.validateNoMissingData();

    }

    public PersonCreationRequest getEntityWithoutLegalPersonalityCreationRequest(){

        Set<Address> addresses = new HashSet<>();

        String postalCode =  addressEntityWithoutLegalPersonalityDataEntryView.getPostalCode().getText();
        Province province = null;
        if(!postalCode.isEmpty()) {
            String provinceInPostalCode = addressEntityWithoutLegalPersonalityDataEntryView.getPostalCode().getText().substring(0, 2);
            for (Province provinceRead : Province.values()) {
                if (provinceRead.getCode().equals(provinceInPostalCode)) {
                    province = provinceRead;
                    break;
                }
            }
        }

        Address address = new Address();
        address.setStreetType(addressEntityWithoutLegalPersonalityDataEntryView.getStreetType().getSelectionModel().getSelectedItem());
        address.setStreetName(addressEntityWithoutLegalPersonalityDataEntryView.getStreetName().getText());
        address.setStreetExtended(addressEntityWithoutLegalPersonalityDataEntryView.getStreetExtended().getText());
        address.setLocation(addressEntityWithoutLegalPersonalityDataEntryView.getLocality().getText());
        address.setMunicipality(addressEntityWithoutLegalPersonalityDataEntryView.getMunicipality().getText());
        address.setPostalCode(addressEntityWithoutLegalPersonalityDataEntryView.getPostalCode().getText());
        address.setProvince(province);
        address.setDefaultAddress(Boolean.TRUE);

        addresses.add(address);

        return PersonCreationRequest.PersonCreationRequestBuilder.create()
                .withPersonType(PersonType.ENTITY_WITHOUT_LEGAL_PERSONALITY)
                .withLegalName(entityWithoutLegalPersonalityDataEntryView.getLegalName().getText())
                .withNieNif(new NieNif(entityWithoutLegalPersonalityDataEntryView.getNieNif().getText()))
                .withAddresses(addresses)
                .build();
    }
}
