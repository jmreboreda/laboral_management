package laboral.component.person_management.person_creation.natural_person;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;
import laboral.domain.address.Address;
import laboral.domain.address.Province;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person_type.PersonType;

import java.util.HashSet;
import java.util.Set;

public class NaturalPersonCreator extends AnchorPane {

    private static final String NATURAL_PERSON_CREATOR_FXML = "/fxml/person_management/person_creation/natural_person/natural_person_creator.fxml";

    private Parent parent;

    @FXML
    private NaturalPersonDataEntryView naturalPersonDataEntryView;
    @FXML
    private AddressNaturalPersonDataEntryView addressNaturalPersonDataEntryView;

    public NaturalPersonCreator() {
        this.parent = ViewLoader.load(this, NATURAL_PERSON_CREATOR_FXML);
    }

    @FXML
    public void initialize(){

    }

    public NaturalPersonDataEntryView getNaturalPersonDataEntryView() {
        return naturalPersonDataEntryView;
    }

    public void setNaturalPersonDataEntryView(NaturalPersonDataEntryView naturalPersonDataEntryView) {
        this.naturalPersonDataEntryView = naturalPersonDataEntryView;
    }

    public AddressNaturalPersonDataEntryView getAddressNaturalPersonDataEntryView() {
        return addressNaturalPersonDataEntryView;
    }

    public void setAddressNaturalPersonDataEntryView(AddressNaturalPersonDataEntryView addressNaturalPersonDataEntryView) {
        this.addressNaturalPersonDataEntryView = addressNaturalPersonDataEntryView;
    }

    public PersonCreationRequest getNaturalPersonCreationRequest(){

        Set<Address> addresses = new HashSet<>();

        String postalCode =  addressNaturalPersonDataEntryView.getPostalCode().getText();
        Province province = null;
        if(!postalCode.isEmpty()) {
            String provinceInPostalCode = addressNaturalPersonDataEntryView.getPostalCode().getText().substring(0, 2);
            for (Province provinceRead : Province.values()) {
                if (provinceRead.getCode().equals(provinceInPostalCode)) {
                    province = provinceRead;
                    break;
                }
            }
        }

        Address address = new Address();
        address.setStreetType(addressNaturalPersonDataEntryView.getStreetType().getSelectionModel().getSelectedItem());
        address.setStreetName(addressNaturalPersonDataEntryView.getStreetName().getText());
        address.setStreetExtended(addressNaturalPersonDataEntryView.getStreetExtended().getText());
        address.setLocation(addressNaturalPersonDataEntryView.getLocality().getText());
        address.setMunicipality(addressNaturalPersonDataEntryView.getMunicipality().getText());
        address.setPostalCode(addressNaturalPersonDataEntryView.getPostalCode().getText());
        address.setProvince(province);
        address.setDefaultAddress(Boolean.TRUE);

        addresses.add(address);

        return PersonCreationRequest.PersonCreationRequestBuilder.create()
                .withId(naturalPersonDataEntryView.getNaturalPersonId())
                .withPersonType(PersonType.NATURAL_PERSON)
                .withFirstSurname(naturalPersonDataEntryView.getFirstSurname().getText())
                .withSecondSurname(naturalPersonDataEntryView.getSecondSurname().getText())
                .withName(naturalPersonDataEntryView.getName().getText())
                .withBirthDate(naturalPersonDataEntryView.getBirthDate().getValue())
                .withNieNif(new NieNif(naturalPersonDataEntryView.getNieNif().getText()))
                .withSocialSecurityAffiliationNumber((naturalPersonDataEntryView.getSocialSecurityAffiliationNumber().getText()))
                .withCivilStatus(naturalPersonDataEntryView.getCivilStatus().getValue())
                .withStudy(naturalPersonDataEntryView.getStudyLevel().getValue())
                .withAddresses(addresses)
                .withNationality(naturalPersonDataEntryView.getNationality().getText())
                .build();
    }
}
