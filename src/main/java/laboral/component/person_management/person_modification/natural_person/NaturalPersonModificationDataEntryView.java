package laboral.component.person_management.person_modification.natural_person;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import laboral.component.client_management.client_modification.generic_components.AddressesGenericTableView;
import laboral.component.client_management.client_modification.generic_components.NaturalPersonGenericDataEntryView;
import laboral.component.person_management.controllers.PersonDataEntryView;
import laboral.component.person_management.controllers.PersonModificationView;
import laboral.component.person_management.events.PersonDataChangedEvent;
import laboral.domain.civil_status.CivilStatusType;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.Person;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.study.StudyLevelType;

import java.time.format.DateTimeFormatter;
import java.util.List;

public class NaturalPersonModificationDataEntryView extends NaturalPersonGenericDataEntryView implements PersonModificationView, PersonDataEntryView {

    public NaturalPersonModificationDataEntryView() {
        super();
    }

    @Override
    public void initialize() {
        super.initialize();
    }

    @Override
    public Integer getNaturalPersonId() {
        return super.getNaturalPersonId();
    }

    @Override
    public void setNaturalPersonId(Integer naturalPersonId) {
        super.setNaturalPersonId(naturalPersonId);
    }

    @Override
    public Group getNaturalPersonGroup() {
        return super.getNaturalPersonGroup();
    }

    @Override
    public TitledPane getHeaderPanel() {
        return super.getHeaderPanel();
    }

    @Override
    public TextField getFirstSurname() {
        return super.getFirstSurname();
    }

    @Override
    public TextField getSecondSurname() {
        return super.getSecondSurname();
    }

    @Override
    public TextField getName() {
        return super.getName();
    }

    @Override
    public CheckBox getNormalizeText() {
        return super.getNormalizeText();
    }

    @Override
    public TextField getNieNif() {
        return super.getNieNif();
    }

    @Override
    public TextField getSocialSecurityAffiliationNumber() {
        return super.getSocialSecurityAffiliationNumber();
    }

    @Override
    public DatePicker getBirthDate() {
        return super.getBirthDate();
    }

    @Override
    public ChoiceBox<CivilStatusType> getCivilStatus() {
        return super.getCivilStatus();
    }

    @Override
    public TextField getNationality() {
        return super.getNationality();
    }

    @Override
    public ChoiceBox<StudyLevelType> getStudyLevel() {
        return super.getStudyLevel();
    }

    @Override
    public void setFirstSurname(TextField firstSurname) {
        super.setFirstSurname(firstSurname);
    }

    @Override
    public void setSecondSurname(TextField secondSurname) {
        super.setSecondSurname(secondSurname);
    }

    @Override
    public void setName(TextField name) {
        super.setName(name);
    }

    @Override
    public void setNormalizeText(CheckBox normalizeText) {
        super.setNormalizeText(normalizeText);
    }

    @Override
    public void setNieNif(TextField nieNif) {
        super.setNieNif(nieNif);
    }

    @Override
    public void setSocialSecurityAffiliationNumber(TextField socialSecurityAffiliationNumber) {
        super.setSocialSecurityAffiliationNumber(socialSecurityAffiliationNumber);
    }

    @Override
    public void setBirthDate(DatePicker birthDate) {
        super.setBirthDate(birthDate);
    }

    @Override
    public void setCivilStatus(ChoiceBox<CivilStatusType> civilStatus) {
        super.setCivilStatus(civilStatus);
    }

    @Override
    public void setNationality(TextField nationality) {
        super.setNationality(nationality);
    }

    @Override
    public void setStudyLevel(ChoiceBox<StudyLevelType> studyLevel) {
        super.setStudyLevel(studyLevel);
    }

    @Override
    public void setDateFormatter(DateTimeFormatter dateFormatter) {
        super.setDateFormatter(dateFormatter);
    }

    @Override
    public void initializeView() {
        super.initializeView();
    }

    @Override
    public void refreshDataToModifyPerson(Person person) {
        super.refreshDataToModifyPerson(person);
    }

    @Override
    public List<PersonDBO> finPersonByNieNif(NieNif nieNif) {
        return null;
    }

    @Override
    public void onChanged(EventHandler<PersonDataChangedEvent> personDataEventHandler) {
        super.onChanged(personDataEventHandler);
    }

    @Override
    public PersonDataEntryView getPersonDataEntryView() {
        return null;
    }

    @Override
    public AddressesGenericTableView getAddress() {
        return null;
    }

    @Override
    public AnchorPane getAnchorPane() {
        return null;
    }

    @Override
    public String getHeaderTitleText() {
        return "Modificación de persona física";
    }
}
