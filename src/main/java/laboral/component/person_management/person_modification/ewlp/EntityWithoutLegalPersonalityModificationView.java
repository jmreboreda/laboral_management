package laboral.component.person_management.person_modification.ewlp;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;
import laboral.component.client_management.client_modification.generic_components.AddressesGenericTableView;
import laboral.component.person_management.controllers.PersonDataEntryView;
import laboral.component.person_management.controllers.PersonModificationView;
import laboral.component.person_management.events.PersonDataChangedEvent;
import laboral.domain.address.Address;
import laboral.domain.address.Province;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.Person;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.person_type.PersonType;
import laboral.domain.utilities.Parameters;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EntityWithoutLegalPersonalityModificationView extends AnchorPane implements PersonModificationView, PersonDataEntryView {

    private static final String ENTITY_WITHOUT_LEGAL_PERSONALITY_MODIFICATION_VIEW_FXML = "/fxml/person_management/person_modification/ewlp/entity_without_legal_personality_modification_view.fxml";
    private static final String ENTITY_WITHOUT_LEGAL_PERSONALITY_MODIFICATION_DATA_ENTRY_VIEW_HEADER_PANE_TEXT = "Datos de la entidad sin personalidad jurídica a modificar";
    private static final String ADDRESSES_ENTITY_WITHOUT_LEGAL_PERSONALITY_MODIFICATION_TABLE_VIEW_HEADER_PANE_TEXT = "Direcciones";

    private Parent parent;

    @FXML
    private EntityWithoutLegalPersonalityModificationDataEntryView entityWithoutLegalPersonalityModificationDataEntryView;
    @FXML
    private AddressesEntityWithoutLegalPersonalityModificationTableView addresses;

    public EntityWithoutLegalPersonalityModificationView() {
        this.parent = ViewLoader.load(this, ENTITY_WITHOUT_LEGAL_PERSONALITY_MODIFICATION_VIEW_FXML);
    }

    @FXML
    public void initialize() {
        entityWithoutLegalPersonalityModificationDataEntryView.getHeaderPane().setText(ENTITY_WITHOUT_LEGAL_PERSONALITY_MODIFICATION_DATA_ENTRY_VIEW_HEADER_PANE_TEXT);
        entityWithoutLegalPersonalityModificationDataEntryView.getHeaderPane().setStyle(Parameters.FOREGROUND_TEXT_RED_COLOR);

        addresses.getHeaderPane().setText(ADDRESSES_ENTITY_WITHOUT_LEGAL_PERSONALITY_MODIFICATION_TABLE_VIEW_HEADER_PANE_TEXT);
        addresses.getHeaderPane().setStyle(Parameters.FOREGROUND_TEXT_RED_COLOR);
    }

    public EntityWithoutLegalPersonalityModificationDataEntryView getEntityWithoutLegalPersonalityDataEntryView() {
        return entityWithoutLegalPersonalityModificationDataEntryView;
    }

    public AddressesEntityWithoutLegalPersonalityModificationTableView getAddresses() {
        return addresses;
    }

    public PersonCreationRequest getLEntityWithoutLegalPersonalityModificationRequest(){

        Set<Address> addressSet = new HashSet<>();
        List<Address> addressList = addresses.getAddresses().getItems();

        for(Address address : addressList){
            String provinceInPostalCode = address.getPostalCode().substring(0,2);
            Province province = null;
            for(Province provinceRead : Province.values()){
                if(provinceRead.getCode().equals(provinceInPostalCode)){
                    province = provinceRead;
                    break;
                }
            }

            Address newAddress = new Address();
            newAddress.setId(address.getId());
            newAddress.setDefaultAddress(address.getDefaultAddress());
            newAddress.setStreetType(address.getStreetType());
            newAddress.setStreetName(address.getStreetName());
            newAddress.setStreetExtended(address.getStreetExtended());
            newAddress.setLocation(address.getLocation());
            newAddress.setMunicipality(address.getMunicipality());
            newAddress.setPostalCode(address.getPostalCode());
            newAddress.setProvince(province);

            addressSet.add(newAddress);
        }

        return PersonCreationRequest.PersonCreationRequestBuilder.create()
                .withId(entityWithoutLegalPersonalityModificationDataEntryView.getEntityWithoutLegalPersonalityId())
                .withPersonType(PersonType.LEGAL_PERSON)
                .withLegalName(entityWithoutLegalPersonalityModificationDataEntryView.getLegalName().getText())
                .withNieNif(new NieNif(entityWithoutLegalPersonalityModificationDataEntryView.getNieNif().getText()))
                .withAddresses(addressSet)
                .build();
    }

    @Override
    public PersonDataEntryView getPersonDataEntryView() {
        return getEntityWithoutLegalPersonalityDataEntryView();
    }

    @Override
    public AnchorPane getAnchorPane() {
        return this;
    }

    @Override
    public AddressesGenericTableView getAddress() {
        return addresses;
    }

    @Override
    public String getHeaderTitleText() {
        return "Modificación de entidad sin personalidad jurídica";
    }

    @Override
    public List<PersonDBO> finPersonByNieNif(NieNif nieNif) {
        return null;
    }

    @Override
    public void refreshDataToModifyPerson(Person person) {

    }

    @Override
    public void onChanged(EventHandler<PersonDataChangedEvent> personDataEventHandler) {

    }
}
