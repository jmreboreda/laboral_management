package laboral.component.person_management.person_modification.ewlp;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import laboral.component.client_management.client_modification.generic_components.AddressesGenericTableView;
import laboral.component.client_management.client_modification.generic_components.EntityWithoutLegalPersonalityGenericDataEntryView;
import laboral.component.person_management.controllers.PersonDataEntryView;
import laboral.component.person_management.controllers.PersonModificationView;
import laboral.component.person_management.events.PersonDataChangedEvent;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.Person;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person.persistence.dbo.PersonDBO;

import java.util.List;

public class EntityWithoutLegalPersonalityModificationDataEntryView extends EntityWithoutLegalPersonalityGenericDataEntryView implements PersonModificationView, PersonDataEntryView {

    public EntityWithoutLegalPersonalityModificationDataEntryView() {
        super();
    }

    @Override
    public TitledPane getHeaderPane() {
        return super.getHeaderPane();
    }

     @Override
    public void initialize() {
        super.initialize();
    }

    @Override
    public Group getLegalPersonGroup() {
        return super.getLegalPersonGroup();
    }

    @Override
    public TextField getLegalName() {
        return super.getLegalName();
    }

    @Override
    public TextField getNieNif() {
        return super.getNieNif();
    }

    @Override
    public CheckBox getNormalizeText() {
        return super.getNormalizeText();
    }

    @Override
    public void initializeView() {
        super.initializeView();
    }

    @Override
    public Boolean validateNoMissingData() {
        return super.validateNoMissingData();
    }

    @Override
    public PersonCreationRequest getPersonCreationRequest() {
        return super.getPersonCreationRequest();
    }

    @Override
    public void refreshDataToModifyPerson(Person person) {
        super.refreshDataToModifyPerson(person);
    }

    @Override
    public List<PersonDBO> finPersonByNieNif(NieNif nieNif) {
        return null;
    }

    @Override
    public void onChanged(EventHandler<PersonDataChangedEvent> personDataEventHandler) {
        super.setOnEntityWithoutLegalPersonalityDataChanged(personDataEventHandler);
    }

    @Override
    public PersonDataEntryView getPersonDataEntryView() {
        return null;
    }

    @Override
    public AddressesGenericTableView getAddress() {
        return null;
    }

    @Override
    public AnchorPane getAnchorPane() {
        return null;
    }

    @Override
    public String getHeaderTitleText() {
        return "Modificación de entidad sin personalidad jurídica";
    }


//    @Deprecated
//    @Override
//    public void setOnEntityWithoutLegalPersonalityDataChanged(EventHandler<PersonDataChangedEvent> personDataEventHandler) {
//        super.setOnEntityWithoutLegalPersonalityDataChanged(personDataEventHandler);
//    }
}
