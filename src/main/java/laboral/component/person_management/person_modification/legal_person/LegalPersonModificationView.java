package laboral.component.person_management.person_modification.legal_person;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;
import laboral.component.client_management.client_modification.generic_components.AddressesGenericTableView;
import laboral.component.person_management.controllers.PersonDataEntryView;
import laboral.component.person_management.controllers.PersonModificationView;
import laboral.domain.address.Address;
import laboral.domain.address.Province;
import laboral.domain.nienif.NieNif;
import laboral.domain.person.PersonCreationRequest;
import laboral.domain.person_type.PersonType;
import laboral.domain.utilities.Parameters;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LegalPersonModificationView extends AnchorPane implements PersonModificationView {

    private static final String LEGAL_PERSON_MODIFICATION_VIEW_FXML = "/fxml/person_management/person_modification/legal_person/legal_person_modification_view.fxml";
    private static final String LEGAL_PERSON_MODIFICATION_DATA_ENTRY_VIEW_HEADER_PANE_TEXT = "Datos de la persona jurídica a modificar";
    private static final String ADDRESSES_LEGAL_PERSON_MODIFICATION_TABLE_VIEW_HEADER_PANE_TEXT = "Direcciones";

    private Parent parent;

    @FXML
    private LegalPersonModificationDataEntryView legalPersonModificationDataEntryView;
    @FXML
    private AddressesLegalPersonModificationTableView addresses;

    public LegalPersonModificationView() {
        this.parent = ViewLoader.load(this, LEGAL_PERSON_MODIFICATION_VIEW_FXML);
    }

    @FXML
    public void initialize() {
        legalPersonModificationDataEntryView.getHeaderPane().setText(LEGAL_PERSON_MODIFICATION_DATA_ENTRY_VIEW_HEADER_PANE_TEXT);
        legalPersonModificationDataEntryView.getHeaderPane().setStyle(Parameters.FOREGROUND_TEXT_RED_COLOR);

        addresses.getHeaderPane().setText(ADDRESSES_LEGAL_PERSON_MODIFICATION_TABLE_VIEW_HEADER_PANE_TEXT);
        addresses.getHeaderPane().setStyle(Parameters.FOREGROUND_TEXT_RED_COLOR);
    }

    public LegalPersonModificationDataEntryView getLegalPersonModificationDataEntryView() {
        return legalPersonModificationDataEntryView;
    }

    public AddressesLegalPersonModificationTableView getAddresses() {
        return addresses;
    }

    public PersonCreationRequest getLegalPersonModificationRequest(){

        Set<Address> addressSet = new HashSet<>();
        List<Address> addressList = addresses.getAddresses().getItems();

        for(Address address : addressList){
            String provinceInPostalCode = address.getPostalCode().substring(0,2);
            Province province = null;
            for(Province provinceRead : Province.values()){
                if(provinceRead.getCode().equals(provinceInPostalCode)){
                    province = provinceRead;
                    break;
                }
            }

            Address newAddress = new Address();
            newAddress.setId(address.getId());
            newAddress.setDefaultAddress(address.getDefaultAddress());
            newAddress.setStreetType(address.getStreetType());
            newAddress.setStreetName(address.getStreetName());
            newAddress.setStreetExtended(address.getStreetExtended());
            newAddress.setLocation(address.getLocation());
            newAddress.setMunicipality(address.getMunicipality());
            newAddress.setPostalCode(address.getPostalCode());
            newAddress.setProvince(province);

            addressSet.add(newAddress);
        }

        return PersonCreationRequest.PersonCreationRequestBuilder.create()
                .withId(legalPersonModificationDataEntryView.getLegalPersonId())
                .withPersonType(PersonType.LEGAL_PERSON)
                .withLegalName(legalPersonModificationDataEntryView.getLegalName().getText())
                .withNieNif(new NieNif(legalPersonModificationDataEntryView.getNieNif().getText()))
                .withAddresses(addressSet)
                .build();
    }

    @Override
    public PersonDataEntryView getPersonDataEntryView() {
        return getLegalPersonModificationDataEntryView();
    }

    @Override
    public AnchorPane getAnchorPane() {
        return this;
    }

    @Override
    public AddressesGenericTableView getAddress() {
        return addresses;
    }

    @Override
    public String getHeaderTitleText() {
        return "Modificación de persona jurídica";
    }
}

