package laboral.component.person_management;

public class PersonManagementConstants {

    public static final String BLUE_COLOR = "-fx-text-inner-color: #000FFF;";
    public static final String RED_COLOR = "-fx-text-inner-color: #640000;";

    public static final String INCOMPLETE_DATA_ENTRY = "Entrada de datos incompleta.";
    public static final String INCOMPLETE_PERSONAL_DATA_ENTRY = "Entrada de datos personales incompleta.";
    public static final String EMPTY_ADDRESSES_IS_NOT_VALID = "Debe haber una dirección establecida al menos.";
    public static final String DEFAULT_ADDRESS_IS_NOT_SET = "No se ha establecido una dirección principal.";
    public static final String SOME_ADDRESS_OR_ADDRESSES_ARE_INCOMPLETE = "Alguna(s) dirección(es) está(n) incompleta(s).";
    public static final String NIE_NIF_IS_NOT_VALID = "El NIE/NIF introducido no es válido.";
    public static final String POSTAL_CODE_IS_NOT_VALID = "El código postal introducido no es válido.";
    public static final String SSAN_IS_NOT_VALID = "El N.A.S.S. introducido no es válido.";
    public static final String CANNOT_DELETE_THE_ONLY_EXISTING_ADDRESS = "No se puede eliminar la única dirección existente.";
    public static final String CANNOT_DELETE_THE_DEFAULT_ADDRESS = "No se puede eliminar la dirección principal.\nEstablezca una nueva dirección principal previamente.";
    public static final String DEFAULT_ADDRESS_INVALID_CHANGE = "No se puede cambiar de dirección principal si es la única dirección existente.";
    public static final String QUESTION_DELETE_ADDRESS_IS_CORRECT = "¿Desea eliminar la dirección seleccionada?";
    public static final String PERSON_SAVED_OK = "Persona guardada correctamente en la base de datos.";
    public static final String PERSON_NOT_SAVED_OK = "ERROR!\n\nNo se ha podido guardar la persona en la base de datos.";
    public static final String PERSON_MODIFICATION_SAVED_OK = "Las modificaciones de la persona se han guardado correctamente en la base de datos.";
    public static final String PERSON_MODIFICATION_NOT_SAVED_OK = "ERROR!\n\nNo se han podido guardar las modificaciones de la persona en la base de datos.";
    public static final String QUESTION_IS_CORRECT_REPEATED_NIE_NIF = "El NIE/NIF introducido ya existe en la tabla de personas para:\n\n";
    public static final String IS_NOT_CORRECT_REPEATED_NASS = "El NASS introducido ya existe en la tabla de personas para:\n\n";
    public static final String TOO_MANY_OPEN_ACTIVITY_PERIODS = "Hay más de un período de actividad abierto.";
    public static final String THERE_ARE_INCONSISTENT_DATES_IN_ANY_PERIOD_OF_ACTIVITY = "Hay fechas inconsistentes en algún(os) período(s) de actividad.";
    public static final String THERE_ARE_ANY_PERIOD_OF_ACTIVITY_WITHOUT_DATES = "Hay períodos de actividad sin fechas de inicio y/o de fin.";
    public static final String TOO_MANY_EQUAL_SERVICES_OPEN = "Hay demasiados servicios abiertos del mismo tipo.";
    public static final String THERE_ARE_INCONSISTENT_DATES_IN_ANY_SERVICE = "Hay fechas inconsistentes en algún(os) servicio(s).";
    public static final String THERE_ARE_SOME_SERVICES_NOT_ESTABLISHED = "Hay algún(os) servicio(s) no establecido(s).";
    public static final String QUESTION_SAVE_NEW_EMPLOYEE = "¿Desea establecer como Empleado a la persona seleccionada?";
    public static final String PERSON_TO_EMPLOYEE_OK = "La persona se ha guardado correctamente como empleado.";
    public static final String PERSON_TO_EMPLOYEE_NOT_OK = "Error: La persona NO se ha guardado como empleado.";

    public static final String OPTION_NOT_IMPLEMENTED_YET = "Opción no implementada todavía.";

    public static final String TOOLTIP_NORMALIZE_TEXT = "Cuando está activado, pone en mayúsculas las letras iniciales de las palabras contenidas en los campos del formulario al" +
            "pulsar el botón \"Aceptar\"";

}
