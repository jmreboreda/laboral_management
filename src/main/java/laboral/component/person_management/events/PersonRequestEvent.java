package laboral.component.person_management.events;

import javafx.event.Event;
import javafx.event.EventType;
import laboral.domain.person.PersonCreationRequest;

public class PersonRequestEvent extends Event {

    public static final EventType<PersonRequestEvent> PERSON_REQUEST_EVENT_EVENT_TYPE = new EventType<>("PERSON_REQUEST_EVENT_EVENT_TYPE");
    private final PersonCreationRequest personCreationRequest;

    public PersonRequestEvent(PersonCreationRequest personCreationRequest) {
        super(PERSON_REQUEST_EVENT_EVENT_TYPE);
        this.personCreationRequest = personCreationRequest;

    }

    public PersonCreationRequest getPersonCreationRequest() {
        return personCreationRequest;
    }
}
