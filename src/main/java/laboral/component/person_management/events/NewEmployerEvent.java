package laboral.component.person_management.events;

import javafx.event.Event;
import javafx.event.EventType;
import laboral.domain.client.Client;

public class NewEmployerEvent extends Event {

    public static final EventType<NewEmployerEvent> NEW_EMPLOYER_EVENT_TYPE = new EventType<>("NEW_EMPLOYER_EVENT_TYPE");
    private final Client newEmployerFromClient;

    public NewEmployerEvent(Client newEmployerFromClient) {
        super(NEW_EMPLOYER_EVENT_TYPE);
        this.newEmployerFromClient = newEmployerFromClient;

    }

    public Client getNewEmployerFromClient() {
        return newEmployerFromClient;
    }
}
