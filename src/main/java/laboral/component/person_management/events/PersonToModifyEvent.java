package laboral.component.person_management.events;

import javafx.event.Event;
import javafx.event.EventType;
import laboral.domain.person.Person;

public class PersonToModifyEvent extends Event {

    public static final EventType<PersonToModifyEvent> PERSON_NAME_TO_MODIFICATION_EVENT_TYPE = new EventType<>("PERSON_NAME_TO_MODIFICATION_EVENT_TYPE");
    private final Person personToModify;

    public PersonToModifyEvent(Person personToModify) {
        super(PERSON_NAME_TO_MODIFICATION_EVENT_TYPE);
        this.personToModify = personToModify;

    }

    public Person getPersonToModify() {
        return personToModify;
    }
}
