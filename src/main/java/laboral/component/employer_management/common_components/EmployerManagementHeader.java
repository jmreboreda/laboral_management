package laboral.component.employer_management.common_components;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import laboral.component.ViewLoader;

public class EmployerManagementHeader extends AnchorPane {

    private static final String NEW_PERSON_HEADER_FXML = "/fxml/employer_management/employer_management_header.fxml";

    private Parent parent;

    @FXML
    private Label headerText;

    public EmployerManagementHeader() {
        this.parent = ViewLoader.load(this, NEW_PERSON_HEADER_FXML);
    }

    @FXML
    public void initialize(){

        getHeaderText().setText("Modificación y baja de empleadores");

    }
    public Label getHeaderText() {
        return headerText;
    }


}
