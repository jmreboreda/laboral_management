package laboral.component.work_contract;

import javafx.scene.Scene;
import javafx.stage.Stage;
import laboral.component.work_contract.creation.components.WorkContractData;
import laboral.component.work_contract.creation.components.WorkContractParts;
import laboral.component.work_contract.creation.components.WorkContractSchedule;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;

import java.time.DayOfWeek;
import java.util.Set;

public class WorkContractDataVerifier {

    public WorkContractDataVerifier() {
    }

    public static Boolean verifyContractParts(WorkContractParts workContractParts, Scene scene){

        if(workContractParts.getEmployerSelector().getSelectionModel().getSelectedItem() == null){
            Message.warningMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.EMPLOYER_IS_NOT_SELECTED);
            return false;
        }

        if(workContractParts.getWorkCenterSelector().getItems().size() > 0 &&
                workContractParts.getWorkCenterSelector().getSelectionModel().getSelectedItem() == null){
            Message.warningMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.WORK_CENTER_IS_NOT_SELECTED);
            return false;
        }

        if(workContractParts.getQuoteAccountCodeSelector().getSelectionModel().getSelectedItem() == null){
            if(!Message.confirmationMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVerifierConstants.QUESTION_NULL_CCC_CODE_IS_CORRECT)){
                return false;
            }
        }

        if(workContractParts.getEmployeeSelector().getSelectionModel().getSelectedItem() == null){
            Message.warningMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.EMPLOYEE_IS_NOT_SELECTED);
            return false;
        }

        return true;
    }

    public static Boolean verifyContractData(WorkContractData workContractData, WorkContractSchedule workContractSchedule, Scene scene){

        if(workContractData.getNotificationHour().getText() == null){
            Message.warningMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.HOUR_NOTIFICATION_IS_NOT_ESTABLISHED);
            return false;
        }

        if(workContractData.getContractTypeSelector().getSelectionModel().getSelectedItem() == null){
            Message.warningMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.CONTRACT_TYPE_NOT_SELECTED);
            return false;
        }

        if(workContractData.getContractTypeSelector().getSelectionModel().getSelectedItem().getTemporal() != workContractData.getTemporalDuration().isSelected()){
            Message.warningMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.DURATION_CONTRACT_SELECTED_DOES_NOT_MATCH);
            return false;
        }

        if(workContractData.getContractTypeSelector().getSelectionModel().getSelectedItem().getFullTime() != workContractData.getFullTime().isSelected()){
            Message.warningMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.WORKDAY_TYPE_SELECTED_DOES_NOT_MATCH);
            return false;
        }

        if(workContractData.getTemporalDuration().isSelected() && workContractData.getDurationDays().getText().isEmpty()) {
                Message.warningMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                        WorkContractVerifierConstants.INVALID_CONTRACT_DURATION);
                return false;
//            }
        }

        if(!workContractData.getDurationDays().getText().isEmpty()) {
            Integer contractDurationDays = Integer.parseInt(workContractData.getDurationDays().getText().replace(".",""));
            if (contractDurationDays <= 0) {
                Message.warningMessage((Stage) scene.getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                        WorkContractVerifierConstants.INVALID_CONTRACT_DURATION);
                return false;
            }
        }

        if(workContractData.getPartialTime().isSelected() && workContractData.getWeeklyWorkHours().getText().equals("00:00")){
            Message.warningMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.PARTIAL_WORK_DAY_WITHOUT_HOURS_WORK_WEEK);
            return false;
        }

        Integer contractDataWeeklyWorkHours = Integer.parseInt(workContractData.getWeeklyWorkHours().getText().replace(":", ""));
        Integer scheduleHoursWorkWeek = Integer.parseInt(workContractSchedule.getHoursWorkWeek().getText().replace(":", ""));
        if(workContractData.getPartialTime().isSelected() &&
                !contractDataWeeklyWorkHours.equals(scheduleHoursWorkWeek)){
            Message.warningMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.DIFFERENT_NUMBER_HOURS_CONTRACT_DATA_AND_CONTRACT_SCHEDULE);
            return false;
        }

        if(workContractData.getDaysOfWeekToWork().size() == 0){
            Message.warningMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.DAYS_TO_WORK_ARE_NOT_SELECTED);
            return false;
        }
        if(workContractData.getLaborCategory().getText().length() == 0){
            Message.warningMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                    WorkContractVerifierConstants.LABOR_CATEGORY_IS_NOT_ESTABLISHED);
            return false;
        }

        return true;
    }

    public static Boolean verifyContractSchedule(WorkContractData workContractData, WorkContractSchedule contractSchedule, Scene scene){

        if(workContractData.getFullTime().isSelected()){
            return true;
        }

        if((contractSchedule.getScheduleOneList().size() > 0 &&
                (contractSchedule.getScheduleTwoList().size() > 0 ||
                        contractSchedule.getScheduleThreeList().size() > 0 ||
                        contractSchedule.getScheduleFourList().size() > 0) ||
                (contractSchedule.getScheduleTwoList().size() > 0 &&
                        (contractSchedule.getScheduleThreeList().size() > 0 ||
                                contractSchedule.getScheduleFourList().size() > 0)))){
            return true;
        }

        Set<DayOfWeek> contractDataDaysOfWeekToWork = workContractData.getDaysOfWeekToWork();
        Set<DayOfWeek> contractScheduleDayOfWeekToWork = contractSchedule.getTableColumnDayOfWeekData();

       if(!contractDataDaysOfWeekToWork.equals(contractScheduleDayOfWeekToWork)){
           Message.warningMessage((Stage) scene.getWindow(),Parameters.SYSTEM_INFORMATION_TEXT,
                   WorkContractVerifierConstants.WORKING_DAYS_ARE_DIFFERENT_IN_CONTRACTDATA_AND_CONTRACTSCHEDULE);
           return false;
       }

        return true;
    }
}
