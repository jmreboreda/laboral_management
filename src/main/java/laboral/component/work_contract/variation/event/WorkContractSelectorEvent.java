package laboral.component.work_contract.variation.event;

import javafx.event.Event;
import javafx.event.EventType;
import laboral.domain.contract.WorkContract;

public class WorkContractSelectorEvent extends Event {

	public static final EventType<WorkContractSelectorEvent> WORK_CONTRACT_SELECTOR_EVENT = new EventType<>("WORK_CONTRACT_SELECTOR_EVENT");
	private final WorkContract workContract;


	public WorkContractSelectorEvent(WorkContract workContract) {
		super(WORK_CONTRACT_SELECTOR_EVENT);
		this.workContract = workContract;
	}

	public WorkContract getWorkContract() {
		return workContract;
	}
}
