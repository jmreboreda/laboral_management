package laboral.component.work_contract.variation.event;

import javafx.event.Event;
import javafx.event.EventType;
import javafx.scene.control.TitledPane;

public class VariationOptionChangeEvent extends Event {

	public static final EventType<VariationOptionChangeEvent> VARIATION_OPTION_CHANGE_EVENT = new EventType<>("VARIATION_OPTION_CHANGE_EVENT");
	private final TitledPane titledPaneSelected;
	private final TitledPane titledPanePrevious;


	public VariationOptionChangeEvent(TitledPane titledPanePrevious, TitledPane titledPaneSelected) {
		super(VARIATION_OPTION_CHANGE_EVENT);
		this.titledPaneSelected = titledPaneSelected;
		this.titledPanePrevious = titledPanePrevious;
	}

	public TitledPane getOptionSelected() {
		return titledPaneSelected;
	}
	public TitledPane getPreviousOption(){
		return titledPanePrevious;
	}
}
