package laboral.component.work_contract.variation.controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.generic_component.DateHourNotification;
import laboral.component.work_contract.ContractConstants;
import laboral.component.work_contract.ContractMainControllerConstants;
import laboral.component.work_contract.TraceabilityCreator;
import laboral.component.work_contract.creation.components.WorkContractCreationActionComponents;
import laboral.component.work_contract.creation.events.ChangeScheduleDurationEvent;
import laboral.component.work_contract.creation.events.EmployeeSelectorChangedEvent;
import laboral.component.work_contract.creation.events.EmployerSelectorChangedEvent;
import laboral.component.work_contract.variation.WorkContractVariationConstants;
import laboral.component.work_contract.variation.component.WorkContractVariationHeader;
import laboral.component.work_contract.variation.component.WorkContractVariationSelector;
import laboral.component.work_contract.variation.component.WorkContractVariationTableView;
import laboral.component.work_contract.variation.component.WorkContractVariationVariation;
import laboral.component.work_contract.variation.event.ChangeDateEvent;
import laboral.component.work_contract.variation.event.WorkContractSelectorEvent;
import laboral.domain.contract.WorkContract;
import laboral.domain.contract.work_contract.WorkContractCreator;
import laboral.domain.contract.work_contract.controller.WorkContractController;
import laboral.domain.contract_json.ContractDaySchedule;
import laboral.domain.contract_variation_for_table_view.WorkContractVariationDataForTableView;
import laboral.domain.employee.Employee;
import laboral.domain.employee.controller.EmployeeController;
import laboral.domain.employer.Employer;
import laboral.domain.employer.controller.EmployerController;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.utilities.utilities.Utilities;
import laboral.domain.variation_type.VariationType;
import laboral.domain.variation_type.controller.VariationTypeController;

import java.text.Collator;
import java.time.Duration;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class WorkContractVariationMainController extends AnchorPane {

    private static final Logger logger = Logger.getLogger(WorkContractVariationMainController.class.getSimpleName());
    private static final String CONTRACT_VARIATION_MAIN_FXML = "/fxml/work_contract/variation/work_contract_variation_main.fxml";

    private final EmployeeController employeeController = new EmployeeController();
    private final WorkContractController workContractController = new WorkContractController();


    private Parent parent;

    @FXML
    private WorkContractVariationHeader workContractVariationHeader;
    @FXML
    private WorkContractVariationSelector workContractVariationSelector;
    @FXML
    private WorkContractVariationTableView workContractVariationTableView;
    @FXML
    private DateHourNotification dateHourNotification;
    @FXML
    private WorkContractVariationVariation workContractVariationVariation;
    @FXML
    private WorkContractCreationActionComponents workContractCreationActionComponents;

    public WorkContractVariationMainController() {
        this.parent = ViewLoader.load(this, CONTRACT_VARIATION_MAIN_FXML);
    }

    @FXML
    public void initialize(){

//        ContextMenu contextMenu = new ContextMenu();
//        MenuItem mi1 = new MenuItem("Ver horario");
//        contextMenu.getItems().add(mi1);
//
//        workContractVariationTableView.addEventHandler(MouseEvent.MOUSE_CLICKED, t -> {
//            if(t.getButton() == MouseButton.SECONDARY) {
//                contextMenu.show(workContractVariationTableView, t.getScreenX(), t.getScreenY());
//            }
//        });

        workContractVariationTableView.disableProperty().bind(this.workContractVariationSelector.getEmployerSelector().getSelectionModel().selectedItemProperty().isNull());
        workContractVariationTableView.disableProperty().bind(this.workContractVariationSelector.getEmployeeSelector().getSelectionModel().selectedItemProperty().isNull());
        workContractVariationTableView.disableProperty().bind(this.workContractVariationSelector.getWorkContractSelector().getSelectionModel().selectedItemProperty().isNull());

        dateHourNotification.disableProperty().bind(this.workContractVariationSelector.getEmployerSelector().getSelectionModel().selectedItemProperty().isNull());
        dateHourNotification.disableProperty().bind(this.workContractVariationSelector.getEmployeeSelector().getSelectionModel().selectedItemProperty().isNull());
        dateHourNotification.disableProperty().bind(this.workContractVariationSelector.getWorkContractSelector().getSelectionModel().selectedItemProperty().isNull());

        workContractVariationVariation.disableProperty().bind(this.workContractVariationSelector.getEmployerSelector().getSelectionModel().selectedItemProperty().isNull());
        workContractVariationVariation.disableProperty().bind(this.workContractVariationSelector.getEmployeeSelector().getSelectionModel().selectedItemProperty().isNull());
        workContractVariationVariation.disableProperty().bind(this.workContractVariationSelector.getWorkContractSelector().getSelectionModel().selectedItemProperty().isNull());

        workContractCreationActionComponents.getOkButton().visibleProperty().bind(this.workContractVariationVariation.getVariationOption().expandedPaneProperty().isNotNull());

        workContractVariationSelector.setOnDateSelectorChange(this::onDateSelectorChange);
        workContractVariationSelector.setOnEmployerSelectorChange(this::onEmployerSelectorChange);
        workContractVariationSelector.setOnEmployeeSelectorChange(this::onEmployeeSelectorChange);
        workContractVariationSelector.setOnWorkContractSelectorChange(this::onWorkContractSelectorChange);
        workContractVariationVariation.getWorkContractVariationSchedule().setOnChangeScheduleDuration(this::onChangeScheduleDuration);


        workContractCreationActionComponents.getOkButton().setOnMouseClicked(this::onOkButton);
        workContractCreationActionComponents.getExitButton().setOnMouseClicked(this::onExitButton);

        loadEmployerSelectorAtDate(LocalDate.now());
        loadExtinctionCauseSelector();
        loadConversionTypeSelector();
        loadSpecialVariationSelector();
        workContractVariationSelector.getDateSelector().requestFocus();
    }

    public WorkContractVariationHeader getWorkContractVariationHeader() {
        return workContractVariationHeader;
    }

    public void setWorkContractVariationHeader(WorkContractVariationHeader workContractVariationHeader) {
        this.workContractVariationHeader = workContractVariationHeader;
    }

    public WorkContractVariationSelector getWorkContractVariationSelector() {
        return workContractVariationSelector;
    }

    public void setWorkContractVariationSelector(WorkContractVariationSelector workContractVariationSelector) {
        this.workContractVariationSelector = workContractVariationSelector;
    }

    public WorkContractVariationTableView getWorkContractVariationTableView() {
        return workContractVariationTableView;
    }

    public void setWorkContractVariationTableView(WorkContractVariationTableView workContractVariationTableView) {
        this.workContractVariationTableView = workContractVariationTableView;
    }

    public DateHourNotification getDateHourNotification() {
        return dateHourNotification;
    }

    public void setDateHourNotification(DateHourNotification dateHourNotification) {
        this.dateHourNotification = dateHourNotification;
    }

    public WorkContractVariationVariation getWorkContractVariationVariation() {
        return workContractVariationVariation;
    }

    public void setWorkContractVariationVariation(WorkContractVariationVariation workContractVariationVariation) {
        this.workContractVariationVariation = workContractVariationVariation;
    }

    public WorkContractCreationActionComponents getWorkContractActionComponents() {
        return workContractCreationActionComponents;
    }

    public void setWorkContractActionComponents(WorkContractCreationActionComponents workContractCreationActionComponents) {
        this.workContractCreationActionComponents = workContractCreationActionComponents;
    }

    private void loadEmployerSelectorAtDate(LocalDate date){

        Set<Employer> employerSet = new HashSet<>();

        EmployerController employerController = new EmployerController();
        List<Employer> allEmployerList = employerController.findAll();
        for(Employer employer : allEmployerList){
            if(employer.isActiveAtDate(date)){
                for(WorkContract workContract : employer.getWorkContracts()){
                    if(workContract.getVariationType().getInitial() &&
                            (workContract.getStartDate().isBefore(date) || workContract.getStartDate().equals(date)) &&
                            (workContract.getEndDate() == null ||
                            (workContract.getEndDate() != null && workContract.getEndDate().isAfter(date)) ||
                            (workContract.getEndDate() != null && workContract.getEndDate().equals(date)))){
                        employerSet.add(employer);
                    }
                }
            }
        }

        List<Employer> employerList = new ArrayList<>(employerSet);
        Collator primaryCollator = Collator.getInstance(new Locale("es", "ES"));
        primaryCollator.setStrength(Collator.PRIMARY);
        List<Employer> employerOrderedList = employerList
                .stream()
                .sorted(Comparator.comparing(Employer::toAlphabeticalName, primaryCollator)).collect(Collectors.toList());

        workContractVariationSelector.refreshEmployerSelector(FXCollections.observableList(employerOrderedList));
    }

    private void loadExtinctionCauseSelector(){

        List<VariationType> variationTypeConversionList = new ArrayList<>();
        VariationTypeController variationTypeController = new VariationTypeController();
        List<VariationType> variationTypeList = variationTypeController.findAllTypesContractVariations();
        for(VariationType variationType : variationTypeList){
            if(variationType.getExtinction()){
                variationTypeConversionList.add(variationType);
            }
        }

        workContractVariationVariation.getWorkContractExtinction().refreshExtinctionCauseSelector(FXCollections.observableArrayList(variationTypeConversionList));
    }

    private void loadConversionTypeSelector(){
        List<VariationType> variationConversionTypeList = new ArrayList<>();
        VariationTypeController variationTypeController = new VariationTypeController();
        List<VariationType> variationTypeList = variationTypeController.findAllTypesContractVariations();
        for(VariationType variationType : variationTypeList){
            if(variationType.getConversion()){
                variationConversionTypeList.add(variationType);
            }
        }

        workContractVariationVariation.getWorkContractConversion().refreshConversionTypeSelector(FXCollections.observableArrayList(variationConversionTypeList));

    }

    private void loadSpecialVariationSelector(){
        List<VariationType> variationConversionTypeList = new ArrayList<>();
        VariationTypeController variationTypeController = new VariationTypeController();
        List<VariationType> variationTypeList = variationTypeController.findAllTypesContractVariations();
        for(VariationType variationType : variationTypeList){
            if(variationType.getReincorporation() || variationType.getSpecial()){
                variationConversionTypeList.add(variationType);
            }
        }

        Collator primaryCollator = Collator.getInstance(new Locale("es", "ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        Comparator<VariationType> compareByVarious = Comparator
                .comparing(VariationType::getSpecial)
                .thenComparing(VariationType::getSpecialInitial).reversed()
                .thenComparing(VariationType::getReincorporation)
                .thenComparing(VariationType::getSpecialFinal)
                .thenComparing(VariationType::getVariationCode)
                .thenComparing(VariationType::getVariationDescription);

        List<VariationType> variationTypeOrderedList= variationConversionTypeList
                .stream()
                .sorted(compareByVarious)
                .collect(Collectors.toList());

        workContractVariationVariation.getWorkContractSpecial().refreshSpecialVariationSelector(FXCollections.observableArrayList(variationTypeOrderedList));
    }

    private void onDateSelectorChange(ChangeDateEvent event){
        interfaceInitialState();
        loadEmployerSelectorAtDate(event.getDate());
    }

    private void onEmployerSelectorChange(EmployerSelectorChangedEvent event) {
        workContractCreationActionComponents.getOkButton().setDisable(false);

        if (event.getSelectedEmployer() != null && event.getSelectedEmployer().getWorkContracts() != null) {

            Set<Employee> employeeSet = new HashSet<>();

            Set<WorkContract> workContractSet = event.getSelectedEmployer().getWorkContracts();
            for (WorkContract workContractVariation : workContractSet) {
                if (!isActiveWorkContractVariationAtDate(workContractVariation, workContractVariationSelector.getDateSelector().getValue())) {
                    continue;
                }

                Employee employee = employeeController.findById(workContractVariation.getEmployee().getId());
                employeeSet.add(employee);
            }

            Collator primaryCollator = Collator.getInstance(new Locale("es", "ES"));
            primaryCollator.setStrength(Collator.PRIMARY);
            List<Employee> employeeOrderedList = employeeSet
                    .stream()
                    .sorted(Comparator.comparing(Employee::toAlphabeticalName, primaryCollator)).collect(Collectors.toList());

            workContractVariationSelector.refreshEmployeeSelector(FXCollections.observableList(employeeOrderedList));
            if(workContractVariationVariation.getVariationOption().getExpandedPane() != null) {
                workContractVariationVariation.getVariationOption().getExpandedPane().setExpanded(false);
            }
        }

        workContractVariationSelector.getDateSelector().requestFocus();
    }

    private void onEmployeeSelectorChange(EmployeeSelectorChangedEvent event){
        workContractCreationActionComponents.getOkButton().setDisable(false);

        if(event.getSelectedEmployee() == null){
            workContractVariationTableView.getWorkContractTableView().getItems().clear();
            return;
        }

        List<WorkContract> workContractList = new ArrayList<>();

        LocalDate date = workContractVariationSelector.getDateSelector().getValue();

        Employee employeeSelected = event.getSelectedEmployee();
        Set<WorkContract> workContractVariationSet = workContractVariationSelector.getEmployerSelector().getSelectionModel().getSelectedItem().getWorkContracts();
        for(WorkContract workContractVariation : workContractVariationSet){
            if(isActiveWorkContractVariationAtDate(workContractVariation, date)){
                Employee employee = employeeController.findById(workContractVariation.getEmployee().getId());
                if (employee.equals(employeeSelected)) {
                    workContractList.add(workContractVariation);
                }
            }
        }

        Collator primaryCollator = Collator.getInstance(new Locale("es", "ES"));
        primaryCollator.setStrength(Collator.PRIMARY);
        List<WorkContract> contractNumberOrderedList = workContractList
                .stream()
                .sorted(Comparator.comparing(WorkContract::toString, primaryCollator)).collect(Collectors.toList());

        workContractVariationSelector.refreshWorkContractSelector(FXCollections.observableList(contractNumberOrderedList));
        if(workContractVariationVariation.getVariationOption().getExpandedPane() != null) {
            workContractVariationVariation.getVariationOption().getExpandedPane().setExpanded(false);
        }

        workContractVariationSelector.getDateSelector().requestFocus();
    }

    private void onWorkContractSelectorChange(WorkContractSelectorEvent event){
        dateHourNotification.setMouseTransparent(false);
        workContractVariationVariation.setWorkContractNotModifiable(Boolean.FALSE);
        workContractVariationVariation.setWorkContractNotConvertible(Boolean.FALSE);
        workContractVariationVariation.setWorkContractNotExtendable(Boolean.FALSE);

        workContractCreationActionComponents.getOkButton().setDisable(false);

        if(event.getWorkContract() == null){
            return;
        }

        Integer extensionCounter = 0;
        VariationTypeController variationTypeController = new VariationTypeController();
        Integer contractNumber = event.getWorkContract().getContractNumber();
        List<WorkContract> contractVariations = workContractController.findAllByWorkContractNumber(contractNumber);
        List<WorkContractVariationDataForTableView> workContractVariationDataForTableViewList = new ArrayList<>();
        for(WorkContract workContractVariationInContract : contractVariations){
            if(workContractVariationInContract.getVariationType().getExtinction()){
                workContractVariationVariation.setWorkContractNotModifiable(Boolean.TRUE);
            }

            if(workContractVariationInContract.getModificationDate() == null && workContractVariationInContract.getExpectedEndDate() == null){
                workContractVariationVariation.setWorkContractNotConvertible(Boolean.TRUE);
            }

            if((workContractVariationInContract.getModificationDate() == null && workContractVariationInContract.getExpectedEndDate() == null)){
                workContractVariationVariation.setWorkContractNotExtendable(Boolean.TRUE);
            }

            if(workContractVariationInContract.getVariationType().getExtension()){
                extensionCounter++;
            }

            VariationType variationType = variationTypeController.findVariationTypeByVariationCode(workContractVariationInContract.getVariationType().getVariationCode());
            String variationDescription = variationType.getInitial() ?
                    variationType.getVariationDescription() + " [" + workContractVariationInContract.getContractType().getColloquial() + "]" :
                    variationType.getVariationDescription();
            Duration hoursWorkWeek = retrieveScheduleHoursWorkWeek(workContractVariationInContract);
            if(hoursWorkWeek == Duration.ZERO){
                if(workContractVariationInContract.getContractType().getFullTime() && !workContractVariationInContract.getVariationType().getExtinction()){
                    hoursWorkWeek = Utilities.converterTimeStringToDuration(ApplicationConstants.DEFAULT_HOURS_FOR_FULL_TIME.toString());
                }
            }
            WorkContractVariationDataForTableView workContractVariationDataForTableView = WorkContractVariationDataForTableView.WorkContractVariationBuilder.create()
                    .withId(workContractVariationInContract.getId())
                    .withVariationTypeCode(workContractVariationInContract.getVariationType().getVariationCode())
                    .withVariationDescription(variationDescription)
                    .withStartDate(workContractVariationInContract.getStartDate())
                    .withExpectedEndDate(workContractVariationInContract.getExpectedEndDate())
                    .withModificationDate(workContractVariationInContract.getModificationDate())
                    .withEndingDate(workContractVariationInContract.getEndDate())
                    .withHoursWorkWeek(hoursWorkWeek)
                    .build();
            workContractVariationDataForTableViewList.add(workContractVariationDataForTableView);
        }

        if(extensionCounter >= ContractConstants.MAXIMUM_LEGAL_NUMBER_OF_EXTENSIONS_FOR_CONTRACT){
            workContractVariationVariation.setWorkContractNotExtendable(Boolean.TRUE);
        }

        Collator primaryCollator = Collator.getInstance(new Locale("es", "ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        Comparator<WorkContractVariationDataForTableView> compareByVarious = Comparator
                .comparing(WorkContractVariationDataForTableView::getStartDate)
                .thenComparing(WorkContractVariationDataForTableView::getVariationTypeCode)
                .thenComparing(WorkContractVariationDataForTableView::getId);

        List<WorkContractVariationDataForTableView> workContractVariationDataForTableViewOrderedList = workContractVariationDataForTableViewList
                .stream()
                .sorted(compareByVarious)
                .collect(Collectors.toList());

        workContractVariationTableView.refreshWorkContractTable(FXCollections.observableList(workContractVariationDataForTableViewOrderedList));
        if(workContractVariationVariation.getVariationOption().getExpandedPane() != null) {
            workContractVariationVariation.getVariationOption().getExpandedPane().setExpanded(false);
        }

        workContractVariationSelector.getDateSelector().requestFocus();
    }

    private Boolean isActiveWorkContractVariationAtDate(WorkContract workContract, LocalDate date){
        if(workContract.getVariationType().getInitial() &&
                (workContract.getStartDate().isBefore(date) || workContract.getStartDate().equals(date)) &&
                (workContract.getEndDate() == null ||
                        (workContract.getEndDate() != null && workContract.getEndDate().isAfter(date)) ||
                        (workContract.getEndDate() != null && workContract.getEndDate().equals(date)))){
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }
    }

    private Duration retrieveScheduleHoursWorkWeek(WorkContract workContract){
        Duration hoursWorkWeek = Duration.ZERO;
        Map<String, ContractDaySchedule> schedule = workContract.getContractSchedule().getSchedule();
        for (Map.Entry<String, ContractDaySchedule> entry : schedule.entrySet()) {
            hoursWorkWeek = hoursWorkWeek.plus(Objects.requireNonNull((Utilities.converterTimeStringToDuration(entry.getValue().getDurationHours()))));
        }

        return hoursWorkWeek;
    }

    private void onChangeScheduleDuration(ChangeScheduleDurationEvent event) {
        if (event.getContractScheduleTotalHoursDuration().compareTo(ContractConstants.LEGAL_MAXIMUM_HOURS_OF_WORK_PER_WEEK) > 0) {
            Message.warningMessage((Stage) getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                    ContractMainControllerConstants.EXCEEDED_MAXIMUM_LEGAL_WEEKLY_HOURS + " [" + Parameters.INTEGER_LEGAL_MAXIMUM_HOURS_OF_WORK_PER_WEEK + "]");
        }
    }

    private void interfaceInitialState(){
        workContractVariationSelector.interfaceInitialState();
        this.dateHourNotification.interfaceInitialState();
        workContractVariationVariation.interfaceInitialState();
    }

    private void onOkButton(MouseEvent event){

        if(dateHourNotification.getNotificationDate().getValue() == null ||
                dateHourNotification.getNotificationDate().getEditor().getText().length() < 10){
            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.DATE_NOTIFICATION_NOT_ESTABLISHED);
            return;
        }

        if(dateHourNotification.getNotificationHour().getText() == null ||
                dateHourNotification.getNotificationHour().getText().isEmpty()){
            Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.HOUR_NOTIFICATION_NOT_ESTABLISHED);
            return;
        }

        Integer contractExtinctionOption = 0;
        Integer extensionContractOption = 1;
        Integer hoursWorkWeekOption = 2;
        Integer conversionContractOption = 3;
        Integer specialContractOption = 4;

        // ContractExtinctionOption
        if(workContractVariationVariation.getVariationOption().getExpandedPane()
                .equals(workContractVariationVariation.getVariationOption().getPanes().get(contractExtinctionOption))){
            List<WorkContract> contractVariationList =
                    workContractController.findAllByWorkContractNumber(workContractVariationSelector.getWorkContractSelector().getSelectionModel().getSelectedItem().getContractNumber());
            verifyAndPersistContractVariationExtinction(contractVariationList);

            return;
        }

        // ExtensionContractOption
        if(workContractVariationVariation.getVariationOption().getExpandedPane()
                .equals(workContractVariationVariation.getVariationOption().getPanes().get(extensionContractOption))){
            List<WorkContract> contractVariationList =
                    workContractController.findAllByWorkContractNumber(workContractVariationSelector.getWorkContractSelector().getSelectionModel().getSelectedItem().getContractNumber());
            verifyAndPersistContractVariationExtension(contractVariationList);

            return;
        }

        // HoursWorkWeekOption
        if(workContractVariationVariation.getVariationOption().getExpandedPane()
                .equals(workContractVariationVariation.getVariationOption().getPanes().get(hoursWorkWeekOption))){
            List<WorkContract> contractVariationList =
                    workContractController.findAllByWorkContractNumber(workContractVariationSelector.getWorkContractSelector().getSelectionModel().getSelectedItem().getContractNumber());
            verifyAndPersistHoursWorkWeekChange(contractVariationList);

            return;
        }

        // SpecialContractOption
        if(workContractVariationVariation.getVariationOption().getExpandedPane()
                .equals(workContractVariationVariation.getVariationOption().getPanes().get(specialContractOption))){
            List<WorkContract> contractVariationList =
                    workContractController.findAllByWorkContractNumber(workContractVariationSelector.getWorkContractSelector().getSelectionModel().getSelectedItem().getContractNumber());
            verifyAndPersistContractVariationSpecial(contractVariationList);

            return;
        }
    }

    private Integer createTraceabilityAndUpdateInWokContractVariation(Integer workContractVariationId){
        TraceabilityCreator traceabilityCreator = new TraceabilityCreator();
        Integer traceabilityId = traceabilityCreator.persistTraceabilityOfContractVariation(workContractVariationId);

        return traceabilityCreator.updateTraceabilityInContractVariation(traceabilityId, workContractVariationId);
    }

    private void onExitButton(MouseEvent event){
        logger.info("Contract management: exiting program.");

        Stage stage = (Stage) this.getScene().getWindow();
        stage.close();
    }

    private void verifyAndPersistContractVariationExtinction(List<WorkContract> contractVariationList){
        if(workContractVariationVariation.getWorkContractExtinction()
                .verifyCorrectExtinctionData(contractVariationList, workContractVariationVariation.getWorkContractExtinction().getDateFrom().getValue())){

            if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.QUESTION_EXTINCT_CONTRACT_IS_CORRECT)){
                return;
            }

            WorkContractCreator workContractCreator = new WorkContractCreator();
            Integer workContractVariationId = workContractCreator.persistWorkContractExtinction(this);

            if(workContractVariationId != null){
                Message.informationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTINCTION_PERSISTENCE_OK);
                Integer traceabilityId = createTraceabilityAndUpdateInWokContractVariation(workContractVariationId);
                if(traceabilityId != null){
                    Message.informationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTINCTION_TRACEABILITY_OK);
                }else{
                    Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTINCTION_TRACEABILITY_NOT_OK);
                }
            }else{
                Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTINCTION_PERSISTENCE_NOT_OK);
            }

            onWorkContractSelectorChange(new WorkContractSelectorEvent(workContractVariationSelector.getWorkContractSelector().getSelectionModel().getSelectedItem()));
            dateHourNotification.getNotificationDate().setValue(null);
            dateHourNotification.getNotificationHour().setText("");
            workContractCreationActionComponents.getOkButton().setDisable(true);
        }
    }

    private void verifyAndPersistContractVariationExtension(List<WorkContract> contractVariationList){
        if(workContractVariationVariation.getWorkContractExtension()
                .verifyCorrectExtensionData(contractVariationList,
                        workContractVariationVariation.getWorkContractExtension().getDateFrom().getValue(),
                        workContractVariationVariation.getWorkContractExtension().getDateTo().getValue())){

            if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.QUESTION_EXTENSION_CONTRACT_IS_CORRECT)){
                return;
            }

            WorkContractCreator workContractCreator = new WorkContractCreator();
            Integer workContractVariationId = workContractCreator.persistWorkContractExtension(this);

            if(workContractVariationId != null){
                Message.informationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTENSION_PERSISTENCE_OK);
                Integer traceabilityId = createTraceabilityAndUpdateInWokContractVariation(workContractVariationId);
                if(traceabilityId != null){
                    Message.informationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTENSION_TRACEABILITY_OK);
                }else{
                    Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTENSION_TRACEABILITY_NOT_OK +
                            WorkContractVariationConstants.REVIEW_DATABASE_REQUIRED);
                }
            }else{
                Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTENSION_PERSISTENCE_NOT_OK +
                        WorkContractVariationConstants.REVIEW_DATABASE_REQUIRED);
            }

            onWorkContractSelectorChange(new WorkContractSelectorEvent(workContractVariationSelector.getWorkContractSelector().getSelectionModel().getSelectedItem()));
            dateHourNotification.getNotificationDate().setValue(null);
            dateHourNotification.getNotificationHour().setText("");
            workContractCreationActionComponents.getOkButton().setDisable(true);
        }
    }

    private void verifyAndPersistHoursWorkWeekChange(List<WorkContract> contractVariationList){
        if(workContractVariationVariation.getWorkContractVariationSchedule()
                .verifyCorrectHoursWorkWeekData(contractVariationList,
                        workContractVariationVariation.getWorkContractVariationSchedule().getStartDate().getValue(),
                        workContractVariationVariation.getWorkContractVariationSchedule().getExpectedEndDate().getValue())){

            if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.QUESTION_EXTENSION_CONTRACT_IS_CORRECT)){
                return;
            }

            WorkContractCreator workContractCreator = new WorkContractCreator();
            Integer workContractVariationId = workContractCreator.persistWorkContractExtension(this);

            if(workContractVariationId != null){
                Message.informationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTENSION_PERSISTENCE_OK);
                Integer traceabilityId = createTraceabilityAndUpdateInWokContractVariation(workContractVariationId);
                if(traceabilityId != null){
                    Message.informationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTENSION_TRACEABILITY_OK);
                }else{
                    Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTENSION_TRACEABILITY_NOT_OK +
                            WorkContractVariationConstants.REVIEW_DATABASE_REQUIRED);
                }
            }else{
                Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_EXTENSION_PERSISTENCE_NOT_OK +
                        WorkContractVariationConstants.REVIEW_DATABASE_REQUIRED);
            }

            onWorkContractSelectorChange(new WorkContractSelectorEvent(workContractVariationSelector.getWorkContractSelector().getSelectionModel().getSelectedItem()));
            dateHourNotification.getNotificationDate().setValue(null);
            dateHourNotification.getNotificationHour().setText("");
            workContractCreationActionComponents.getOkButton().setDisable(true);
        }
    }

    private void verifyAndPersistContractVariationSpecial(List<WorkContract> contractVariationList){
        if(workContractVariationVariation.getWorkContractSpecial()
                .verifyCorrectSpecialData(workContractVariationVariation, contractVariationList)){

            if(!Message.confirmationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.QUESTION_SPECIAL_CONTRACT_VARIATION_IS_CORRECT)){
                return;
            }

            VariationType variationTypeSelected = workContractVariationVariation.getWorkContractSpecial().getSpecialVariationSelector().getSelectionModel().getSelectedItem();

            WorkContractCreator workContractCreator = new WorkContractCreator();
            Integer workContractVariationId = workContractCreator.persistWorkContractSpecial(this);

            if(workContractVariationId != null){
                Message.informationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, variationTypeSelected.getVariationDescription() +
                        WorkContractVariationConstants.CONTRACT_SPECIAL_PERSISTENCE_OK);
                Integer traceabilityId = createTraceabilityAndUpdateInWokContractVariation(workContractVariationId);
                if(traceabilityId != null){
                    Message.informationMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_VARIATION_TRACEABILITY_OK +
                            variationTypeSelected.getVariationDescription());
                }else{
                    Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.CONTRACT_VARIATION_TRACEABILITY_NOT_OK +
                            variationTypeSelected.getVariationDescription() + "\n" + WorkContractVariationConstants.REVIEW_DATABASE_REQUIRED);
                }
            }else{
                Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, variationTypeSelected.getVariationDescription() + WorkContractVariationConstants.CONTRACT_SPECIAL_PERSISTENCE_NOT_OK +
                        WorkContractVariationConstants.REVIEW_DATABASE_REQUIRED);
            }

            onWorkContractSelectorChange(new WorkContractSelectorEvent(workContractVariationSelector.getWorkContractSelector().getSelectionModel().getSelectedItem()));
            dateHourNotification.getNotificationDate().setValue(null);
            dateHourNotification.getNotificationHour().setText("");
            workContractCreationActionComponents.getOkButton().setDisable(true);
        }
    }
}
