package laboral.component.work_contract.variation.component;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.LocalDateStringConverter;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.work_contract.variation.WorkContractVariationConstants;
import laboral.domain.contract.WorkContract;
import laboral.domain.contract.work_contract.WorkContractService;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.variation_type.VariationType;
import laboral.domain.variation_type.VariationTypeService;
import laboral.domain.variation_type.persistence.dbo.VariationTypeDBO;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class WorkContractExtinction extends AnchorPane {

    private static final String CONTRACT_VARIATION_CONTRACT_EXTINCTION_FXML = "/fxml/work_contract/variation/work_contract_extinction.fxml";

    private Parent parent;

    @FXML
    private ComboBox<VariationType> extinctionCauseSelector;
    @FXML
    private DatePicker dateFrom;
    @FXML
    private ToggleGroup holidaysToggleGroup;
    @FXML
    private RadioButton rbHolidaysYes;
    @FXML
    private RadioButton rbHolidaysNo;
    @FXML
    private RadioButton rbHolidaysCalculate;
    @FXML
    private TextArea publicNotes;
    @FXML
    private TextArea privateNotes;

    public WorkContractExtinction() {
        this.parent = ViewLoader.load(this, CONTRACT_VARIATION_CONTRACT_EXTINCTION_FXML);
    }

    @FXML
    private void initialize(){

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());
        dateFrom.setConverter(new LocalDateStringConverter(dateFormatter, null));


        extinctionCauseSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 14));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.CENTER_LEFT);
                    setText(item.toString());
                }
            }

        });

        extinctionCauseSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<VariationType> call(ListView<VariationType> param) {
                        final ListCell<VariationType> cell = new ListCell<VariationType>() {
                            @Override
                            public void updateItem(VariationType item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }
                });

        dateFrom.disableProperty().bind(this.extinctionCauseSelector.valueProperty().isNull());
        rbHolidaysYes.disableProperty().bind(this.extinctionCauseSelector.valueProperty().isNull());
        rbHolidaysNo.disableProperty().bind(this.extinctionCauseSelector.valueProperty().isNull());
        rbHolidaysCalculate.disableProperty().bind(this.extinctionCauseSelector.valueProperty().isNull());
        publicNotes.disableProperty().bind(this.extinctionCauseSelector.valueProperty().isNull());
        privateNotes.disableProperty().bind(this.extinctionCauseSelector.valueProperty().isNull());

        publicNotes.setStyle("-fx-text-fill: #000FFF");
        privateNotes.setStyle("-fx-text-fill: #640000");
    }

    public ToggleGroup getHolidaysToggleGroup() {
        return holidaysToggleGroup;
    }

    public void setHolidaysToggleGroup(ToggleGroup holidaysToggleGroup) {
        this.holidaysToggleGroup = holidaysToggleGroup;
    }

    public ComboBox<VariationType> getExtinctionCauseSelector() {
        return extinctionCauseSelector;
    }

    public void setExtinctionCauseSelector(ComboBox<VariationType> extinctionCauseSelector) {
        this.extinctionCauseSelector = extinctionCauseSelector;
    }

    public DatePicker getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(DatePicker dateFrom) {
        this.dateFrom = dateFrom;
    }

    public RadioButton getRbHolidaysYes() {
        return rbHolidaysYes;
    }

    public void setRbHolidaysYes(RadioButton rbHolidaysYes) {
        this.rbHolidaysYes = rbHolidaysYes;
    }

    public RadioButton getRbHolidaysNo() {
        return rbHolidaysNo;
    }

    public void setRbHolidaysNo(RadioButton rbHolidaysNo) {
        this.rbHolidaysNo = rbHolidaysNo;
    }

    public RadioButton getRbHolidaysCalculate() {
        return rbHolidaysCalculate;
    }

    public void setRbHolidaysCalculate(RadioButton rbHolidaysCalculate) {
        this.rbHolidaysCalculate = rbHolidaysCalculate;
    }

    public TextArea getPublicNotes() {
        return publicNotes;
    }

    public void setPublicNotes(TextArea publicNotes) {
        this.publicNotes = publicNotes;
    }

    public TextArea getPrivateNotes() {
        return privateNotes;
    }

    public void setPrivateNotes(TextArea privateNotes) {
        this.privateNotes = privateNotes;
    }

    public void cleanComponents(){
        extinctionCauseSelector.getSelectionModel().clearSelection();
        dateFrom.setValue(null);
        rbHolidaysYes.setSelected(false);
        rbHolidaysNo.setSelected(false);
        rbHolidaysCalculate.setSelected(false);
        publicNotes.clear();
        privateNotes.clear();
    }

    public void refreshExtinctionCauseSelector(ObservableList<VariationType> variationTypeExtinctionObservableList){
        extinctionCauseSelector.getSelectionModel().clearSelection();
        extinctionCauseSelector.getItems().clear();
        extinctionCauseSelector.setItems(variationTypeExtinctionObservableList);
    }

    public Boolean verifyCorrectExtinctionData(List<WorkContract> workContractToFinalizeList, LocalDate extinctionDate){

        if(extinctionCauseSelector.getSelectionModel().getSelectedItem() == null){
            Message.errorMessage((Stage) this.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.EXTINCTION_CAUSE_NOT_ESTABLISHED);
            return Boolean.FALSE;
        }

        if(extinctionDate == null){
            Message.errorMessage((Stage) this.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.ERROR_IN_EXTINCTION_DATE);
            return Boolean.FALSE;
        }

        for(WorkContract workContract : workContractToFinalizeList){
            if(workContract.getModificationDate() != null && extinctionDate.isBefore(workContract.getModificationDate())){
                Message.errorMessage((Stage) this.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.ERROR_IN_EXTINCTION_DATE);
                return Boolean.FALSE;
            }

            if(workContract.getModificationDate() == null && workContract.getExpectedEndDate() != null && extinctionDate.isAfter(workContract.getExpectedEndDate())){
                Message.errorMessage((Stage) this.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.ERROR_IN_EXTINCTION_DATE);
                return Boolean.FALSE;
            }
        }

        if(holidaysToggleGroup.getSelectedToggle() == null){
            Message.errorMessage((Stage) this.getParent().getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVariationConstants.HOLIDAYS_SITUATION_NOT_ESTABLISHED);
            return Boolean.FALSE;
        }else{
            if(rbHolidaysYes.isSelected()){
                publicNotes.setText("Vacaciones disfrutadas. " + publicNotes.getText());
            }else if(rbHolidaysNo.isSelected()){
                publicNotes.setText("Vacaciones NO disfrutadas. " + publicNotes.getText());
            }else{
                publicNotes.setText("Calcular las vacaciones. " + publicNotes.getText());
            }
        }

        return Boolean.TRUE;
    }

    public void updateEndingDateInInitialWorkContract(Integer contractNumber, Date endingDate){
        WorkContractService workContractService = new WorkContractService();
        WorkContractDBO workContractDBO = workContractService.findInitialWorkContractByWorkContractNumber(contractNumber);
        workContractDBO.setModificationDate(endingDate);
        workContractDBO.setEndingDate(endingDate);
        workContractService.update(workContractDBO);
    }

    public void updateModificationDateInAllWorkContractVariation(Integer contractNumber, Date endingDate){
        WorkContractService workContractService = new WorkContractService();
        VariationTypeService variationTypeService = new VariationTypeService();
        List<WorkContractDBO> workContractDBOList = workContractService.findAllWorkContractVariationByContractNumber(contractNumber);
        for(WorkContractDBO workContractDBO : workContractDBOList){
            VariationTypeDBO variationTypeDBO = variationTypeService.findByVariationTypeCode(workContractDBO.getVariationTypeCode());
            if(workContractDBO.getModificationDate() == null && !variationTypeDBO.getExtinction()){
                workContractDBO.setModificationDate(endingDate);
                workContractService.update(workContractDBO);
            }
        }
    }
}
