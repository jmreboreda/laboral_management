package laboral.component.work_contract.variation.component;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Accordion;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import laboral.component.ViewLoader;
import laboral.domain.contract.work_contract.WorkContractService;
import laboral.domain.contract.work_contract.persistence.dbo.WorkContractDBO;
import laboral.domain.variation_type.VariationTypeService;
import laboral.domain.variation_type.persistence.dbo.VariationTypeDBO;

import java.sql.Date;
import java.util.List;

public class WorkContractVariationVariation extends AnchorPane {

    private static final String WORK_CONTRACT_VARIATION_VARIATION_FXML = "/fxml/work_contract/variation/work_contract_variation_variation.fxml";

    Parent parent;

    private final Font normalFont = Font.font("Noto Sans", FontWeight.NORMAL, FontPosture.REGULAR, 13);
    private final Font boldFont = Font.font("Noto Sans", FontWeight.BOLD, FontPosture.REGULAR, 13);

    private final Integer extinctionWorkContractOption = 0;
    private final Integer extensionWorkContractOption = 1;
    private final Integer variationHoursWorkWeekOption = 2;
    private final Integer conversionWorkContractOption = 3;

    @FXML
    private Accordion variationOption;

    @FXML
    private WorkContractExtinction workContractExtinction;
    @FXML
    private WorkContractExtension workContractExtension;
    @FXML
    private WorkContractVariationSchedule workContractVariationSchedule;
    @FXML
    private WorkContractConversion workContractConversion;
    @FXML
    private WorkContractSpecial workContractSpecial;

    public WorkContractVariationVariation() {
        this.parent = ViewLoader.load(this, WORK_CONTRACT_VARIATION_VARIATION_FXML);
    }

    @FXML
    public void initialize() {

        Double widthMin = workContractVariationSchedule.getAmFrom().getWidth() * 1.25;
        workContractVariationSchedule.getAmFrom().setMinWidth(widthMin);
        workContractVariationSchedule.getAmTo().setMinWidth(widthMin);
        workContractVariationSchedule.getPmFrom().setMinWidth(widthMin);
        workContractVariationSchedule.getPmTo().setMinWidth(widthMin);

        variationOption.expandedPaneProperty().addListener((ov, old_val, new_val) -> {
            for(TitledPane titledPane :  variationOption.getPanes()){
                if (titledPane == variationOption.getExpandedPane()){
                    titledPane.setStyle("-fx-text-fill: #000FFF");
                    titledPane.setFont(boldFont);
                }else{
                    titledPane.setStyle("-fx-text-fill: #000FFF");
                    titledPane.setFont(normalFont);
                }
            }

            initializePanes();

        });
    }

    public void interfaceInitialState(){
        variationOption.setExpandedPane(null);
    }


    public Accordion getVariationOption() {
        return variationOption;
    }

    public void setVariationOption(Accordion variationOption) {
        this.variationOption = variationOption;
    }

    public WorkContractExtinction getWorkContractExtinction() {
        return workContractExtinction;
    }

    public void setWorkContractExtinction(WorkContractExtinction workContractExtinction) {
        this.workContractExtinction = workContractExtinction;
    }

    public WorkContractExtension getWorkContractExtension() {
        return workContractExtension;
    }

    public void setWorkContractExtension(WorkContractExtension workContractExtension) {
        this.workContractExtension = workContractExtension;
    }

    public WorkContractVariationSchedule getWorkContractVariationSchedule() {
        return workContractVariationSchedule;
    }

    public void setWorkContractVariationSchedule(WorkContractVariationSchedule workContractVariationSchedule) {
        this.workContractVariationSchedule = workContractVariationSchedule;
    }

    public WorkContractConversion getWorkContractConversion() {
        return workContractConversion;
    }

    public void setWorkContractConversion(WorkContractConversion workContractConversion) {
        this.workContractConversion = workContractConversion;
    }

    public WorkContractSpecial getWorkContractSpecial() {
        return workContractSpecial;
    }

    public void setWorkContractSpecial(WorkContractSpecial workContractSpecial) {
        this.workContractSpecial = workContractSpecial;
    }

    private void initializePanes(){
        workContractExtinction.cleanComponents();
        workContractExtension.cleanComponents();
        workContractVariationSchedule.cleanComponents();
        workContractConversion.cleanComponents();
        workContractSpecial.cleanComponents();
    }

    public void setWorkContractNotModifiable(Boolean isExtinct){
        variationOption.setDisable(isExtinct);
    }

    public void setWorkContractNotConvertible(Boolean isNotConvertible){
        variationOption.getPanes().get(conversionWorkContractOption).setDisable(isNotConvertible);
    }

    public void setWorkContractNotExtendable(Boolean isNotExtendable){
        variationOption.getPanes().get(extensionWorkContractOption).setDisable(isNotExtendable);
    }
}
