package laboral.component.work_contract.variation.component;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Callback;
import javafx.util.converter.LocalDateStringConverter;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.work_contract.creation.events.EmployeeSelectorChangedEvent;
import laboral.component.work_contract.creation.events.EmployerSelectorChangedEvent;
import laboral.component.work_contract.variation.event.ChangeDateCheckerEvent;
import laboral.component.work_contract.variation.event.ChangeDateEvent;
import laboral.component.work_contract.variation.event.WorkContractSelectorEvent;
import laboral.domain.contract.WorkContract;
import laboral.domain.employee.Employee;
import laboral.domain.employer.Employer;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class WorkContractVariationSelector extends AnchorPane {

    private static final String WORK_CONTRACT_VARIATION_SELECTOR_FXML = "/fxml/work_contract/variation/work_contract_variation_selector.fxml";

    Parent parent;

    private EventHandler<ChangeDateCheckerEvent> changeDateCheckerEventEventHandler;
    private EventHandler<ChangeDateEvent> changeDateEventEventHandler;
    private EventHandler<EmployerSelectorChangedEvent> employerSelectorChangedEventEventHandler;
    private EventHandler<EmployeeSelectorChangedEvent> employeeSelectorChangedEventEventHandler;
    private EventHandler<WorkContractSelectorEvent> workContractSelectorEventEventHandler;
    @FXML
    private CheckBox workContractDateChecker;
    @FXML
    private DatePicker dateSelector;
    @FXML
    private ComboBox<Employer> employerSelector;
    @FXML
    private ComboBox<Employee> employeeSelector;
    @FXML
    private ComboBox<WorkContract> workContractSelector;


    public WorkContractVariationSelector() {
        this.parent = ViewLoader.load(this, WORK_CONTRACT_VARIATION_SELECTOR_FXML);
    }

    @FXML
    public void initialize(){

        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());
        dateSelector.setConverter(new LocalDateStringConverter(dateFormatter, null));
        dateSelector.setEditable(false);
        dateSelector.valueProperty().addListener((ov, oldValue, newValue) -> {
            if(oldValue != null) {
                onDateSelectorChange();
            }
        });

        workContractDateChecker.setOnMouseClicked(this::onDateCheckerChange);

        dateSelector.setValue(LocalDate.now());

        employerSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 14));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.CENTER);
                    setText(item.toString());
                }
            }

        });

        employerSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<Employer> call(ListView<Employer> param) {
                        final ListCell<Employer> cell = new ListCell<Employer>() {
                            @Override
                            public void updateItem(Employer item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }
                });

        employeeSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 14));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.CENTER);
                    setText(item.toString());
                }
            }

        });

        employeeSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<Employee> call(ListView<Employee> param) {
                        final ListCell<Employee> cell = new ListCell<Employee>() {
                            @Override
                            public void updateItem(Employee item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }
                });


        workContractSelector.setButtonCell(new ListCell(){
            @Override
            protected void updateItem(Object item, boolean empty) {
                super.updateItem(item, empty);
                if(empty || item==null){
                    setFont(Font.font ("Noto Sans", 14));
                    setAlignment(Pos.CENTER_LEFT);
                    setTextFill(Color.BLUE);
                }
                else {
                    setFont(Font.font ("Noto Sans", FontWeight.BOLD, 14));
                    setTextFill(Color.BLUE);
                    setAlignment(Pos.CENTER);
                    setText(item.toString());
                }
            }

        });

        workContractSelector.setCellFactory(
                new Callback<>() {
                    @Override
                    public ListCell<WorkContract> call(ListView<WorkContract> param) {
                        final ListCell<WorkContract> cell = new ListCell<WorkContract>() {
                            @Override
                            public void updateItem(WorkContract item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item.toString());
                                    setFont(Font.font("Noto Sans", 14));
                                    setTextFill(Color.BLUE);
                                }
                            }
                        };
                        return cell;
                    }
                });

        employerSelector.setOnAction(this::onEmployerSelectorChange);
        employeeSelector.setOnAction(this::onEmployeeSelectorChange);
        workContractSelector.setOnAction(this::onWorkContractSelectorChange);

    }

    public void interfaceInitialState(){
        employerSelector.getSelectionModel().clearSelection();
        employerSelector.getItems().clear();

        employeeSelector.getSelectionModel().clearSelection();
        employeeSelector.getItems().clear();

        workContractSelector.getSelectionModel().clearSelection();
        workContractSelector.getItems().clear();
    }

    public CheckBox getWorkContractDateChecker() {
        return workContractDateChecker;
    }

    public void setWorkContractDateChecker(CheckBox workContractDateChecker) {
        this.workContractDateChecker = workContractDateChecker;
    }

    public DatePicker getDateSelector() {
        return dateSelector;
    }

    public void setDateSelector(DatePicker dateSelector) {
        this.dateSelector = dateSelector;
    }

    public ComboBox<Employer> getEmployerSelector() {
        return employerSelector;
    }

    public void setEmployerSelector(ComboBox<Employer> employerSelector) {
        this.employerSelector = employerSelector;
    }

    public ComboBox<Employee> getEmployeeSelector() {
        return employeeSelector;
    }

    public void setEmployeeSelector(ComboBox<Employee> employeeSelector) {
        this.employeeSelector = employeeSelector;
    }

    public ComboBox<WorkContract> getWorkContractSelector() {
        return workContractSelector;
    }

    public void setWorkContractSelector(ComboBox<WorkContract> workContractSelector) {
        this.workContractSelector = workContractSelector;
    }

    public void refreshEmployerSelector(ObservableList<Employer> employerObservableList){
        employerSelector.getSelectionModel().clearSelection();
        employerSelector.getItems().clear();
        employerSelector.setItems(employerObservableList);
        employerSelector.setPromptText("Seleccionar cliente-empleador");
    }

    public void refreshEmployeeSelector(ObservableList<Employee> employeeObservableList){
        employeeSelector.getSelectionModel().clearSelection();
        employeeSelector.getItems().clear();
        employeeSelector.setItems(employeeObservableList);
        if(employeeObservableList.size() == 1){
            employeeSelector.getSelectionModel().select(0);
        }else{
            workContractSelector.getSelectionModel().clearSelection();
            workContractSelector.getItems().clear();
        }
    }

    public void refreshWorkContractSelector(ObservableList<WorkContract> workContractObservableList){
        workContractSelector.getSelectionModel().clearSelection();
        workContractSelector.getItems().clear();
        workContractSelector.setItems(workContractObservableList);
        if(workContractObservableList.size() == 1){
            workContractSelector.getSelectionModel().select(0);
        }
    }

    private void  onDateCheckerChange(MouseEvent event){
        if(!workContractDateChecker.isSelected()) {
            workContractDateChecker.setSelected(true);
        }
    }

    private void onEmployerSelectorChange(ActionEvent event){
        Employer employer = employerSelector.getSelectionModel().getSelectedItem();
        employerSelectorChangedEventEventHandler.handle(new EmployerSelectorChangedEvent(employer));
    }

    private void onEmployeeSelectorChange(ActionEvent event){
        employeeSelectorChangedEventEventHandler.handle(new EmployeeSelectorChangedEvent(getEmployeeSelector().getSelectionModel().getSelectedItem()));
    }

    private void onDateSelectorChange(){
        changeDateEventEventHandler.handle(new ChangeDateEvent(dateSelector.getValue()));
    }

    private void onWorkContractSelectorChange(ActionEvent event){
        workContractSelectorEventEventHandler.handle(new WorkContractSelectorEvent(workContractSelector.getSelectionModel().getSelectedItem()));
    }

    public void setOnDateSelectorChange(EventHandler<ChangeDateEvent> changeDateEventEventHandler){
        this.changeDateEventEventHandler = changeDateEventEventHandler;
    }

    public void setOnEmployerSelectorChange(EventHandler<EmployerSelectorChangedEvent> employerSelectorChangedEventEventHandler){
        this.employerSelectorChangedEventEventHandler = employerSelectorChangedEventEventHandler;
    }

    public void setOnEmployeeSelectorChange(EventHandler<EmployeeSelectorChangedEvent> employeeSelectorChangedEventEventHandler){
        this.employeeSelectorChangedEventEventHandler = employeeSelectorChangedEventEventHandler;
    }

    public void setOnWorkContractSelectorChange(EventHandler<WorkContractSelectorEvent> workContractSelectorEventEventHandler){
        this.workContractSelectorEventEventHandler = workContractSelectorEventEventHandler;
    }
}
