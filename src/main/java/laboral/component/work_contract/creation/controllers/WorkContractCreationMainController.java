package laboral.component.work_contract.creation.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import laboral.ApplicationConstants;
import laboral.component.ViewLoader;
import laboral.component.person_management.events.NamePatternChangedEvent;
import laboral.component.work_contract.*;
import laboral.component.work_contract.creation.components.*;
import laboral.component.work_contract.creation.events.*;
import laboral.domain.client.controller.ClientController;
import laboral.domain.contract.WorkContract;
import laboral.domain.contract.WorkContractCreationRequest;
import laboral.domain.contract.work_contract.WorkContractCreator;
import laboral.domain.contract.work_contract.controller.WorkContractController;
import laboral.domain.contract.work_contract.documentation.NewContractDocumentationCreator;
import laboral.domain.contract_type.WorkContractType;
import laboral.domain.contract_type.controllers.ContractTypeController;
import laboral.domain.employee.Employee;
import laboral.domain.employee.controller.EmployeeController;
import laboral.domain.employer.Employer;
import laboral.domain.employer.controller.EmployerController;
import laboral.domain.person.controller.PersonController;
import laboral.domain.person.persistence.dao.PersonDAO;
import laboral.domain.person.persistence.dbo.PersonDBO;
import laboral.domain.quote_account_code.QuoteAccountCode;
import laboral.domain.service_gm.ServiceGMController;
import laboral.domain.utilities.Message;
import laboral.domain.utilities.Parameters;
import laboral.domain.utilities.events.DateChangedEvent;
import laboral.domain.utilities.utilities.Utilities;
import laboral.domain.work_center.WorkCenter;
import laboral.domain.work_center.mapper.MapperWorkCenterDBOToWorkCenter;
import laboral.domain.work_center.persistence.dbo.WorkCenterDBO;

import java.nio.file.Path;
import java.text.Collator;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

public class WorkContractCreationMainController extends AnchorPane {

    private static final Logger logger = Logger.getLogger(WorkContractCreationMainController.class.getSimpleName());

    private static final String WORK_CONTRACT_MAIN_CONTROLLER_FXML = "/fxml/work_contract/creation/work_contract_main.fxml";
    private static final String COLOR_RED = "-fx-text-fill: #640000";
    private static final String COLOR_BLUE = "-fx-text-inner-color: #000FFF;";
    private static final String COLOR_GREEN = "-fx-text-fill: #185E03";
    private static final String EMPLOYEE_OF_NEW_CLIENT = "Empleado de nuevo cliente";
    private static final String ADVISORY_TEXT = "Asesoría";
    private static final String UNDEFINED_TEXT  = "Indefinida";

    Parent parent;

    PersonDAO personDAO = PersonDAO.PersonDAOFactory.getInstance();
    EmployerController employerController = new EmployerController();
    EmployeeController employeeController = new EmployeeController();
    ClientController clientController = new ClientController();
    PersonController personController = new PersonController();
    ServiceGMController serviceGMController = new ServiceGMController();
    WorkContractController workContractController = new WorkContractController();
    ContractTypeController contractTypeController = new ContractTypeController();

    DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(ApplicationConstants.DEFAULT_DATE_FORMAT.getValue());

    @FXML
    private WorkContractHeader workContractHeader;
    @FXML
    private WorkContractParts workContractParts;
    @FXML
    private WorkContractData workContractData;
    @FXML
    private WorkContractSchedule workContractSchedule;
    @FXML
    private WorkContractPublicNotes workContractPublicNotes;
    @FXML
    private WorkContractPrivateNotes workContractPrivateNotes;
    @FXML
    private WorkContractCreationActionComponents workContractCreationActionComponents;
    @FXML
    private WorkContractProvisionalData workContractProvisionalData;

    public WorkContractCreationMainController() {
        this.parent = ViewLoader.load(this, WORK_CONTRACT_MAIN_CONTROLLER_FXML);
    }

    @FXML
    public void initialize(){

        workContractParts.setOnEmployerPatternChanged(this::onEmployerNamePatternChange);
        workContractParts.setOnEmployerSelectorChange(this::onEmployerSelectorChange);
        workContractParts.setOnEmployeePatternChanged(this::onEmployeeNamePatternChange);
        workContractParts.setOnEmployeeSelectorChange(this::onEmployeeSelectorChange);
        workContractParts.setOnWorkCenterSelectorChange(this::onWorkCenterSelectorChange);
        workContractParts.setOnQuoteAccountCodeSelectorChange(this::onQuoteAccountCodeSelectorChange);
        workContractData.setOnContractTypeSelectorChange(this::onContractTypeSelectorChange);
        workContractData.getUndefinedDuration().setOnAction(this::onUndefinedDurationChange);
        workContractData.getTemporalDuration().setOnAction(this::onTemporalDurationChange);
        workContractData.getFullTime().setOnAction(this::onFullTimeChange);
        workContractData.getPartialTime().setOnAction(this::onPartialTimeChange);

        workContractData.setOnDateFromChange(this::onDateFromChange);
        workContractData.setOnDateToChange(this::onDateToChange);

        workContractData.setOnWeeklyWorkHoursChange(this::onWeeklyWorkHoursChange);

        workContractData.getDaysOfWeekSelector().getMondayCheck().setOnAction(this::onMondayChange);
        workContractData.getDaysOfWeekSelector().getTuesdayCheck().setOnAction(this::onTuesdayChange);
        workContractData.getDaysOfWeekSelector().getWednesdayCheck().setOnAction(this::onWednesdayChange);
        workContractData.getDaysOfWeekSelector().getThursdayCheck().setOnAction(this::onThursdayChange);
        workContractData.getDaysOfWeekSelector().getFridayCheck().setOnAction(this::onFridayChange);
        workContractData.getDaysOfWeekSelector().getSaturdayCheck().setOnAction(this::onSaturdayChange);
        workContractData.getDaysOfWeekSelector().getSundayCheck().setOnAction(this::onSundayChange);

        workContractData.setOnLaborCategoryChange(this::onLaborCategoryChange);

        workContractSchedule.setOnChangeScheduleDuration(this::onChangeScheduleDuration);

        workContractCreationActionComponents.setOnOkButton(this::onOkButton);
        workContractCreationActionComponents.setOnExitButton(this::onExitButton);
        
        loadContractTypeSelector();
    }

    public static String getEmployeeOfNewClient() {
        return EMPLOYEE_OF_NEW_CLIENT;
    }

    public WorkContractParts getWorkContractParts() {
        return workContractParts;
    }

    public void setWorkContractParts(WorkContractParts workContractParts) {
        this.workContractParts = workContractParts;
    }

    public WorkContractData getWorkContractData() {
        return workContractData;
    }

    public void setWorkContractData(WorkContractData workContractData) {
        this.workContractData = workContractData;
    }

    public WorkContractProvisionalData getWorkContractProvisionalData() {
        return workContractProvisionalData;
    }

    public void setWorkContractProvisionalData(WorkContractProvisionalData workContractProvisionalData) {
        this.workContractProvisionalData = workContractProvisionalData;
    }

    public WorkContractSchedule getWorkContractSchedule() {
        return workContractSchedule;
    }

    public void setWorkContractSchedule(WorkContractSchedule workContractSchedule) {
        this.workContractSchedule = workContractSchedule;
    }

    public WorkContractPublicNotes getWorkContractPublicNotes() {
        return workContractPublicNotes;
    }

    public void setWorkContractPublicNotes(WorkContractPublicNotes workContractPublicNotes) {
        this.workContractPublicNotes = workContractPublicNotes;
    }

    public WorkContractPrivateNotes getWorkContractPrivateNotes() {
        return workContractPrivateNotes;
    }

    public void setWorkContractPrivateNotes(WorkContractPrivateNotes workContractPrivateNotes) {
        this.workContractPrivateNotes = workContractPrivateNotes;
    }

    public WorkContractCreationActionComponents getWorkContractActionComponents() {
        return workContractCreationActionComponents;
    }

    public void setWorkContractActionComponents(WorkContractCreationActionComponents workContractCreationActionComponents) {
        this.workContractCreationActionComponents = workContractCreationActionComponents;
    }

    private void loadContractTypeSelector(){
        List<WorkContractType> workContractTypeList = contractTypeController.findAll();

        Collator primaryCollator = Collator.getInstance(new Locale("es","ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        List<WorkContractType> orderedAndFilteredWorkContractTypeList = workContractTypeList
                .stream()
                .sorted(Comparator.comparing(WorkContractType::getColloquial, primaryCollator)).collect(Collectors.toList());

        ObservableList<WorkContractType> workContractTypeObservableList = FXCollections.observableList(orderedAndFilteredWorkContractTypeList);
        workContractData.contractTypeSelectorRefresh(workContractTypeObservableList);
    }

    private void onEmployerNamePatternChange(NamePatternChangedEvent namePatternChangedEvent){
        String pattern = namePatternChangedEvent.getPattern();

        workContractParts.getEmployerSelector().getSelectionModel().clearSelection();
        workContractParts.getEmployerSelector().getItems().clear();
        workContractParts.getEmployerSelector().setPromptText("");
        workContractParts.getQuoteAccountCodeSelector().getSelectionModel().clearSelection();
        workContractParts.getQuoteAccountCodeSelector().getItems().clear();

        if(pattern.isEmpty() || pattern.length() < 3){
            return;
        }

        List<Employer> employerList = employerController.findActiveEmployerByNamePattern(pattern);

        if (employerList.size() == 0) {
            return;
        }

        Collator primaryCollator = Collator.getInstance(new Locale("es","ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        List<Employer> orderedEmployerList = employerList
                .stream()
                .sorted(Comparator.comparing(Employer::toString, primaryCollator)).collect(Collectors.toList());

        ObservableList<Employer> employerObservableList = FXCollections.observableArrayList(orderedEmployerList);
        workContractParts.employerSelectorRefresh(employerObservableList);
    }

    private void onEmployerSelectorChange(EmployerSelectorChangedEvent event){

        if(event.getSelectedEmployer() != null) {
            workContractProvisionalData.getEmployerLabel().setText(event.getSelectedEmployer().toAlphabeticalName());
            ObservableList<QuoteAccountCode> quoteAccountCodeObservableList = FXCollections.observableList(new ArrayList<>(event.getSelectedEmployer().getQuoteAccountCodes()));
            workContractParts.quoteAccountCodeSelectorRefresh(quoteAccountCodeObservableList);

            MapperWorkCenterDBOToWorkCenter mapperWorkCenterDBOToWorkCenter = new MapperWorkCenterDBOToWorkCenter();
            List<WorkCenter> workCenterList = new ArrayList<>();
            for (WorkCenterDBO workCenterDBO : event.getSelectedEmployer().getClientDBO().getWorkCenters()) {
                WorkCenter workCenter = mapperWorkCenterDBOToWorkCenter.map(workCenterDBO);
                workCenterList.add(workCenter);
            }

            workContractParts.workCenterSelectorRefresh(FXCollections.observableList(workCenterList));
        }else{
            workContractParts.getWorkCenterSelector().getSelectionModel().clearSelection();
            workContractParts.getWorkCenterSelector().getItems().clear();
            workContractProvisionalData.getEmployerLabel().setText("");
            workContractProvisionalData.getQacLabel().setText("");
            workContractProvisionalData.getWorkCenterLabel().setText("");
        }
    }

    private void onWorkCenterSelectorChange(WorkCenterSelectorChangedEvent event){
        String workCenterAddress = event.getSelectedWorkCenter() == null ? "" : event.getSelectedWorkCenter().toString();
        workContractProvisionalData.getWorkCenterLabel().setText(workCenterAddress);
    }

    private void onQuoteAccountCodeSelectorChange(QuoteAccountCodeSelectorChangedEvent quoteAccountCodeSelectorChangedEvent){
        if(workContractParts.getQuoteAccountCodeSelector().getSelectionModel().getSelectedItem() != null) {
            String qacText = quoteAccountCodeSelectorChangedEvent.getSelectedQuoteAccountCode().toCompleteNumber();
            Tooltip tooltip = new Tooltip(workContractParts.getQuoteAccountCodeSelector().getSelectionModel().getSelectedItem().toString());
            tooltip.setStyle("-fx-background-color: blue; -fx-text-fill: white;");
            tooltip.setFont(Font.font("Noto Sans", 12));
            workContractParts.getQuoteAccountCodeSelector().setTooltip(tooltip);
            workContractProvisionalData.getQacLabel().setText(qacText);
        }
    }

    public void onEmployeeNamePatternChange(NamePatternChangedEvent namePatternChangedEvent){
        String pattern = namePatternChangedEvent.getPattern();

        if(pattern.isEmpty()){
            workContractParts.getEmployeeSelector().getSelectionModel().clearSelection();
            workContractParts.getEmployeeSelector().getItems().clear();
            workContractParts.getEmployeeSelector().setPromptText("");

            return;
        }

        List<Employee> employeeList = employeeController.findEmployeeByNamePattern(pattern);

        if (employeeList.size() == 0) {
            workContractParts.getEmployeeSelector().getSelectionModel().clearSelection();
            workContractParts.getEmployeeSelector().getItems().clear();
            workContractParts.getEmployeeSelector().setPromptText("");

            return;
        }

        Collator primaryCollator = Collator.getInstance(new Locale("es","ES"));
        primaryCollator.setStrength(Collator.PRIMARY);

        List<Employee> orderedEmployeeList = employeeList
                .stream()
                .sorted(Comparator.comparing(Employee::toString, primaryCollator)).collect(Collectors.toList());

        ObservableList<Employee> employeeObservableList = FXCollections.observableArrayList(orderedEmployeeList);
        workContractParts.employeeSelectorRefresh(employeeObservableList);
    }

    private void onEmployeeSelectorChange(EmployeeSelectorChangedEvent event) {
        if (event.getSelectedEmployee() != null) {
            workContractProvisionalData.getEmployeeLabel().setText(event.getSelectedEmployee().toString());
            if (event.getSelectedEmployee().getNieNif() != null) {
                List<PersonDBO> personDBOList = personDAO.findPersonByNieNif(event.getSelectedEmployee().getNieNif().getNif());
                workContractParts.refreshEmployeeData(personDBOList.get(0));
            }
        }else {
            workContractProvisionalData.getEmployeeLabel().setText("");
            workContractParts.clearEmployeeData();
        }
    }

    private void onContractTypeSelectorChange(ContractTypeSelectorChangedEvent event){
        workContractProvisionalData.getContractTypeLabel().setText(event.getSelectedContractType().toString());
        workContractData.getDateFrom().setValue(LocalDate.now().minusDays(10L));
        workContractData.getDateTo().setValue(LocalDate.now().minusDays(10L));

        workContractData.getUndefinedDuration().setSelected(event.getSelectedContractType().getUndefined());
        workContractData.getTemporalDuration().setSelected(event.getSelectedContractType().getTemporal());

        workContractData.getFullTime().setSelected(event.getSelectedContractType().getFullTime());
        workContractData.getPartialTime().setSelected(event.getSelectedContractType().getPartialTime());

        if(event.getSelectedContractType().getUndefined()){
            workContractData.getDateTo().setValue(null);
            workContractData.getDurationDays().setText("");
            workContractProvisionalData.getProvisionalDateTo().setText("");
            workContractProvisionalData.getProvisionalContractDurationDays().setText(UNDEFINED_TEXT);
        }else{
            workContractProvisionalData.getProvisionalContractDurationDays().setText("");
        }

        if(workContractData.getFullTime().isSelected()){
            workContractProvisionalData.getProvisionalFullPartialTime().setText(event.getSelectedContractType().getFullPartialTimeText());
            workContractProvisionalData.getProvisionalWeeklyWorkHours().setText(String.valueOf(ApplicationConstants.DEFAULT_HOURS_FOR_FULL_TIME));
            workContractData.getWeeklyWorkHours().setText(String.valueOf(ApplicationConstants.DEFAULT_HOURS_FOR_FULL_TIME));
        }

        if(workContractData.getPartialTime().isSelected()){
            workContractProvisionalData.getProvisionalFullPartialTime().setText(event.getSelectedContractType().getFullPartialTimeText());
            workContractProvisionalData.getProvisionalWeeklyWorkHours().setText("00:00");
            workContractData.getWeeklyWorkHours().setText("00:00");
        }
    }

    private void onUndefinedDurationChange(ActionEvent event){
        workContractData.getUndefinedDuration().setSelected(workContractData.getContractTypeSelector().getSelectionModel().getSelectedItem().getUndefined());
        workContractData.getTemporalDuration().setSelected(workContractData.getContractTypeSelector().getSelectionModel().getSelectedItem().getTemporal());
    }

    private void onTemporalDurationChange(ActionEvent event) {
        workContractData.getTemporalDuration().setSelected(workContractData.getContractTypeSelector().getSelectionModel().getSelectedItem().getTemporal());
        workContractData.getUndefinedDuration().setSelected(workContractData.getContractTypeSelector().getSelectionModel().getSelectedItem().getUndefined());
    }

    private void onDateFromChange(DateChangedEvent event){
        if(event.getDate() == null){
            workContractData.getDurationDays().setText("");
            workContractProvisionalData.getProvisionalDateFrom().setText("");
            workContractData.getDateFrom().requestFocus();

            return;
        }

//        if(event.getDate().isBefore(LocalDate.now().minusDays(4L))){
//            workContractData.getDateFrom().setStyle(COLOR_RED);
//        }else{
//            workContractData.getDateFrom().setStyle(COLOR_BLUE);
//        }

        workContractProvisionalData.getProvisionalDateFrom().setText(event.getDate().format(dateFormatter));
        calculateContractDurationDays();
    }

    private void onDateToChange(DateChangedEvent event){
        if(event.getDate() == null){
            workContractData.getDurationDays().setText("");
            workContractProvisionalData.getProvisionalDateTo().setText("");

            return;
        }

        if(event.getDate().isBefore(LocalDate.now().minusDays(4L))){
            workContractData.getDateTo().setStyle(COLOR_RED);
        }else{
            workContractData.getDateTo().setStyle(COLOR_BLUE);
        }

        workContractProvisionalData.getProvisionalDateTo().setText(event.getDate().format(dateFormatter));
        calculateContractDurationDays();
    }

    private void onFullTimeChange(ActionEvent event){
        workContractData.getFullTime().setSelected(workContractData.getContractTypeSelector().getSelectionModel().getSelectedItem().getFullTime());
        workContractData.getPartialTime().setSelected(workContractData.getContractTypeSelector().getSelectionModel().getSelectedItem().getPartialTime());
    }

    private void onPartialTimeChange(ActionEvent event){
        workContractData.getPartialTime().setSelected(workContractData.getContractTypeSelector().getSelectionModel().getSelectedItem().getPartialTime());
        workContractData.getFullTime().setSelected(workContractData.getContractTypeSelector().getSelectionModel().getSelectedItem().getFullTime());
    }

    private void onWeeklyWorkHoursChange(WeeklyWorkHoursChangedEvent event){
        Duration duration = Utilities.converterTimeStringToDuration(event.getWeeklyWorkHours());
        if(duration == null ||  duration.compareTo(WorkContractParameters.LEGAL_MAXIMUM_HOURS_OF_WORK_PER_WEEK) >= 0L){
            workContractData.getWeeklyWorkHours().setText("");
            workContractProvisionalData.getProvisionalWeeklyWorkHours().setText("");

            return;
        }

        if(duration != null){
            String weeklyWorkHours = Utilities.converterDurationToTimeString(duration);
            workContractData.getWeeklyWorkHours().setText(weeklyWorkHours);
            workContractProvisionalData.getProvisionalWeeklyWorkHours().setText(weeklyWorkHours);
        }else{
            workContractData.getWeeklyWorkHours().setText("");
            workContractProvisionalData.getProvisionalWeeklyWorkHours().setText("");
        }
    }

    private void onMondayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalMonday().setSelected(workContractData.getDaysOfWeekSelector().getMondayCheck().isSelected());
    }

    private void onTuesdayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalTuesday().setSelected(workContractData.getDaysOfWeekSelector().getTuesdayCheck().isSelected());
    }

    private void onWednesdayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalWednesday().setSelected(workContractData.getDaysOfWeekSelector().getWednesdayCheck().isSelected());
    }

    private void onThursdayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalThursday().setSelected(workContractData.getDaysOfWeekSelector().getThursdayCheck().isSelected());
    }

    private void onFridayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalFriday().setSelected(workContractData.getDaysOfWeekSelector().getFridayCheck().isSelected());
    }

    private void onSaturdayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalSaturday().setSelected(workContractData.getDaysOfWeekSelector().getSaturdayCheck().isSelected());
    }

    private void onSundayChange(ActionEvent event){
        workContractProvisionalData.getProvisionalSunday().setSelected(workContractData.getDaysOfWeekSelector().getSundayCheck().isSelected());
    }

    private void onLaborCategoryChange(LaborCategoryChangedEvent event){
        workContractProvisionalData.getProvisionalLaborCategory().setText(event.getLaborCategory());
    }

    private void calculateContractDurationDays() {
        if (workContractData.getTemporalDuration().isSelected()) {
            if (workContractData.getDateFrom().getValue() != null &&
                    workContractData.getDateTo().getValue() != null) {
                if (workContractData.getDateTo().getValue().isBefore(workContractData.getDateFrom().getValue())) {
                    Message.errorMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, WorkContractParameters.THERE_ARE_INCOHERENT_DATES_IN_CONTRACT);
                    workContractData.getDateFrom().setValue(null);
                    workContractData.getDateTo().setValue(null);
                    workContractData.getDurationDays().setText("");
                    workContractProvisionalData.getProvisionalContractDurationDays().setText("");
                } else {
                    long daysBetween = DAYS.between(workContractData.getDateFrom().getValue(), workContractData.getDateTo().getValue()) + 1L;
                    String durationContractDays = String.format("%,d", daysBetween);
                    workContractData.getDurationDays().setText(durationContractDays);
                    workContractProvisionalData.getProvisionalContractDurationDays().setText(durationContractDays);
                }
            }
        }
    }

    private void onChangeScheduleDuration(ChangeScheduleDurationEvent event) {
        if (event.getContractScheduleTotalHoursDuration().compareTo(ContractConstants.LEGAL_MAXIMUM_HOURS_OF_WORK_PER_WEEK) > 0) {
            Message.warningMessage((Stage) getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                    ContractMainControllerConstants.EXCEEDED_MAXIMUM_LEGAL_WEEKLY_HOURS + " [" + Parameters.INTEGER_LEGAL_MAXIMUM_HOURS_OF_WORK_PER_WEEK + "]");
            return;
        }

        Duration weeklyWorkHoursDuration = Utilities.converterTimeStringToDuration(workContractData.getWeeklyWorkHours().getText()) == null ? Duration.ZERO
                : Utilities.converterTimeStringToDuration(workContractData.getWeeklyWorkHours().getText());

        if (event.getContractScheduleTotalHoursDuration().compareTo(weeklyWorkHoursDuration) > 0) {
            Message.warningMessage((Stage) getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                    ContractMainControllerConstants.SCHEDULE_HOURS_GREATER_THAN_CONTRACT_HOURS);
        }
    }

    private void setStatusRevisionErrors(String statusText) {
        workContractProvisionalData.getStatus().setText(statusText);
        if (statusText.equals(ContractConstants.REVISION_WITHOUT_ERRORS)) {
            if (Message.confirmationMessage((Stage) this.getScene().getWindow(),
                    Parameters.SYSTEM_INFORMATION_TEXT, WorkContractVerifierConstants.QUESTION_SAVE_NEW_CONTRACT)) {

                workContractCreationActionComponents.getSendMailButton().setDisable(true);
                workContractCreationActionComponents.getViewPDFButton().setDisable(true);
                workContractCreationActionComponents.getOkButton().setDisable(true);

                // Persist new work contract
                WorkContractCreator workContractCreator = new WorkContractCreator();
                Integer newWorkContractId = workContractCreator.persistNewWorkContract(this);
                if(newWorkContractId != null){
                    WorkContract workContract = workContractController.findById(newWorkContractId);
                    Integer workContractNumber = workContract.getContractNumber();
                    Message.informationMessage((Stage) getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT,
                            ContractMainControllerConstants.CONTRACT_SAVED_OK + workContractNumber);
                    workContractProvisionalData.getWorkContractSavedOkText().setText(ContractMainControllerConstants.CONTRACT_SAVED_OK + workContractNumber);

                    // Persist traceability of new contract
                    Integer traceabilityId = persistNewWorkContractTraceability(newWorkContractId);
                    if(traceabilityId == null) {
                        Message.warningMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ContractConstants.ERROR_PERSISTING_TRACEABILITY_CONTROL_DATA);
                        return;
                    }

                    // Update traceability in new work contract
                    Integer workContractId = updateTraceabilityInNewWorkContract(traceabilityId, newWorkContractId);
                    if (workContractId == null) {
                        Message.warningMessage((Stage) this.getScene().getWindow(), Parameters.SYSTEM_INFORMATION_TEXT, ContractConstants.ERROR_UPDATING_TRACEABILITY_IN_NEW_WORK_CONTRACT);
                        return;
                    }

                    WorkContractCreationRequest workContractCreationRequest = workContractCreator.newWorkContractCreationRequestGenerator((workContractNumber));
                    NewContractDocumentationCreator newContractDocumentationCreator = new NewContractDocumentationCreator();
                    Path path = newContractDocumentationCreator.retrievePathToContractDataSubFolderPDF(workContractCreationRequest);


                }

                lockInterfaceForChanges();
                workContractCreationActionComponents.getSendMailButton().setDisable(false);
                workContractCreationActionComponents.getViewPDFButton().setDisable(false);
                workContractCreationActionComponents.getOkButton().setDisable(true);
            }
        }
    }

    private Integer persistNewWorkContractTraceability(Integer newWorkContractId){
        TraceabilityCreator traceabilityCreator = new TraceabilityCreator();

        return traceabilityCreator.persistTraceabilityOfContractVariation(newWorkContractId);
    }

    private Integer updateTraceabilityInNewWorkContract(Integer traceabilityId, Integer workContractVariationId){
        TraceabilityCreator traceabilityCreator = new TraceabilityCreator();

        return traceabilityCreator.updateTraceabilityInContractVariation(traceabilityId, workContractVariationId);
    }

    private void lockInterfaceForChanges(){
        workContractParts.setMouseTransparent(true);
        workContractData.setMouseTransparent(true);
        workContractSchedule.setMouseTransparent(true);
        workContractPublicNotes.setMouseTransparent(true);
        workContractPrivateNotes.setMouseTransparent(true);
    }

    private void onOkButton(MouseEvent event){
        workContractProvisionalData.getStatus().setStyle(COLOR_RED);

        if (!WorkContractDataVerifier.verifyContractParts(workContractParts, this.getScene())) {
            setStatusRevisionErrors(ContractConstants.REVISION_WITH_ERRORS);
            return;
        }

        if (!WorkContractDataVerifier.verifyContractData(workContractData, workContractSchedule, this.getScene())) {
            setStatusRevisionErrors(ContractConstants.REVISION_WITH_ERRORS);
            return;
        }

        if (!WorkContractDataVerifier.verifyContractSchedule(workContractData, workContractSchedule, this.getScene())) {
            setStatusRevisionErrors(ContractConstants.REVISION_WITH_ERRORS);
            return;
        }

        workContractProvisionalData.getStatus().setStyle(COLOR_GREEN);
        setStatusRevisionErrors(ContractConstants.REVISION_WITHOUT_ERRORS);
    }

    private void onExitButton(MouseEvent event){
        Stage stage = (Stage) this.getScene().getWindow();
        stage.close();
    }
}
