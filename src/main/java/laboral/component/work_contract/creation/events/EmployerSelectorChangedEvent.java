package laboral.component.work_contract.creation.events;

import javafx.event.Event;
import javafx.event.EventType;
import laboral.domain.employer.Employer;

public class EmployerSelectorChangedEvent extends Event {

    public static final EventType<EmployerSelectorChangedEvent> EMPLOYER_SELECTOR_CHANGE_EVENT_TYPE = new EventType<>("CLIENT_SELECTOR_CHANGE_EVENT_TYPE");
    private final Employer selectedEmployer;

    public EmployerSelectorChangedEvent(Employer selectedEmployer) {
        super(EMPLOYER_SELECTOR_CHANGE_EVENT_TYPE);
        this.selectedEmployer = selectedEmployer;

    }

    public Employer getSelectedEmployer() {
        return selectedEmployer;
    }
}
