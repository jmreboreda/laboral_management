package laboral.component.work_contract.creation.events;

import javafx.event.Event;
import javafx.event.EventType;

public class WeeklyWorkHoursChangedEvent extends Event {

    public static final EventType<WeeklyWorkHoursChangedEvent> WEEKLY_WORK_HOURS_CHANGE_EVENT_TYPE = new EventType<>("WEEKLY_WORK_HOURS_CHANGE_EVENT_TYPE");
    private final String weeklyWorkHours;

    public WeeklyWorkHoursChangedEvent(String weeklyWorkHours) {
        super(WEEKLY_WORK_HOURS_CHANGE_EVENT_TYPE);
        this.weeklyWorkHours = weeklyWorkHours;

    }

    public String getWeeklyWorkHours() {
        return weeklyWorkHours;
    }
}
