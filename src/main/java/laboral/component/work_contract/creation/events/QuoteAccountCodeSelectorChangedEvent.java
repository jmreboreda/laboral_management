package laboral.component.work_contract.creation.events;

import javafx.event.Event;
import javafx.event.EventType;
import laboral.domain.quote_account_code.QuoteAccountCode;

public class QuoteAccountCodeSelectorChangedEvent extends Event {

    public static final EventType<QuoteAccountCodeSelectorChangedEvent> QUOTE_ACCOUNT_CODE_SELECTOR_CHANGE_EVENT_TYPE = new EventType<>("QUOTE_ACCOUNT_CODE_SELECTOR_CHANGE_EVENT_TYPE");
    private final QuoteAccountCode quoteAccountCode;

    public QuoteAccountCodeSelectorChangedEvent(QuoteAccountCode quoteAccountCode) {
        super(QUOTE_ACCOUNT_CODE_SELECTOR_CHANGE_EVENT_TYPE);
        this.quoteAccountCode = quoteAccountCode;

    }

    public QuoteAccountCode getSelectedQuoteAccountCode() {
        return quoteAccountCode;
    }
}
